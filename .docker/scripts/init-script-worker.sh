#!/bin/bash

# Create config
[[ -z $DB_PORT ]] && export DB_PORT=5432
export SQLALCHEMY_DATABASE_URI="${DB_SCHEMA}://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}"

# Create/Clean data base
echo "Waiting for database to initialize"
/root/wait-for-it.sh ${DB_HOST}:${DB_PORT} -t 600  #waiting for DB to initialize

# Starting study governor
echo "Starting study governor worker"
celery -A studygovernor.callbacks.backends.celery_worker.celery worker -Q ${WORKER_QUEUE} -l debug