#!/bin/bash

# Create config
[[ -z $DB_PORT ]] && export DB_PORT=5432
export SQLALCHEMY_DATABASE_URI="${DB_SCHEMA}://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}"

# Wait for database
echo "Waiting for database to initialize"
/root/wait-for-it.sh ${DB_HOST}:${DB_PORT} -t 600  #waiting for DB to initialize

# Clean database 
studygov-db-clean --force

/root/init-script.sh
