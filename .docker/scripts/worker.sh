#!/bin/sh

# Starting worker
echo "Starting celery worker"
#[[ -z $STUDYGOV_CELERY_BROKER  ]] && export STUDYGOV_CELERY_BROKER='amqp://guest@rabbitmq'
celery -A studygovernor.callbacks.celery_worker.celery -b ${STUDYGOV_CELERY_BROKER} worker -l debug