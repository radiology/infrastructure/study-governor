Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <http://keepachangelog.com/>`_
and this project adheres to `Semantic Versioning <http://semver.org/>`_

2024-07-02 - 8.1.0
------------------

Fixed
~~~~~

- Bug in command where the regular expression on the stdout would fail all callbacks

Added
~~~~~

- Admin form to upload data to the StudyGovernor
- Castor callback
- Expected return code argument command and external program callbacks

Removed
~~~~~~~

- Everything related to the PIDB


8.0.0 - 2023-12-15
------------------

New
~~~

- Support for Kubernetes deployments.
- Authentication with the studygovernor from the workers must be defined using k8s secrets
- The source in a `variable_map` now can be a JSON-pointer for using partial return values
- The `command` and `external_program` callbacks now can return values from a subprocess if it has a line in
  the format `__VALUES__ = <some single-line JSON string>` in the stdout. In that case the JSON is parsed and
  returned under the `values` key.
- Support for both Postgres and MySQL/mariadb databases.
- Automatic generation of admin accounts based on environment variables for easy setup in container environments.
- Service routes to load configurations and workflows, enabling setting up the StudyGovernor via the API.


Changed
~~~~~~~

- The deployments of the StudyGovernor using docker-compose or kubernetes now use Postgres by default.
- BREAKING: The `variable_map` in a state now has a different definition, the key is the target and the value the source.
- The `python_function` callback now by default passes the `callback_execution_data` and `config` variable along
  to the target function. This can be disable by setting `pass_callback_data=False`, which reverts to the old behaviour.
- Responses of GET calls on the API are now all ordered by database id.



7.0.0 - 2023-03-10
------------------

New
~~~

- The StudyGovernor now supports Cohorts to manage multiple cohorts of data within the same StudyGovernor
- Concept of callbacks and callback execution is now formalised. They are tracked separately by the system and have
  a status and return values
- Conditions are added to transitions, and the first matching transition used automatically once all callbacks are
  finished
- Added the v2 API for a more consistent interface with a more predictable behaviour

Changed
~~~~~~~

- Docker containers updated to python:3.10.5-alpine3.16
- CI testing is done in parallel and reformatted for easier maintenance
- Updated the container init script for provisioning study governor config from a private git repo

6.5.1 - 2022-03-09
------------------

Fixed
~~~~~

- Fixed bug due to new markupsafe release breaking flask
- Fixed bug in external subject links put route

6.5.0 - 2022-02-01
------------------

Added
~~~~~

- Initial support for upgrading workflows (as long as they do not change anything that would
  alter the history of anything in the database)

Fixed
~~~~~

- Some fixes in the PIDB connection
- Fix a dependencies problem caused WTForms having a major release

Changed
~~~~~~~

- Split the API code in different files for better maintainability


6.4.0 - 2021-08-09
------------------

Added
~~~~~

- Users can now register themselves, confirm their email, and change/recover passwords
- Bulk email form for mailing (groups of) users.
- Make it possible to specify a celery queue in the workflow file to enable very basic task routing

Changed
~~~~~~~

- Login in can be done both via username or email
- Reading configuration files will now insert new records, but not overwrite/update any existing record

Fixed
~~~~~

- Ensure the install scripts mak the /var/<service> folder structure

6.3.0 - 2021-02-01
------------------

Added
~~~~~~~

- Kubernetes manifest files
- Pagenation on the web view
- Added support for project data in private reposotory


Fixed
~~~~~~~

- Lots of small bugs and typos
- Callbacks now only use API and not direct database access


6.2.1 - 2020-07-03
------------------

Changed
~~~~~~~

- The workflow visualization has been refactored and is now able to read from the database



6.2.0 - 2020-07-02
------------------


Added
~~~~~

- Workflow version to state view and state/workflow view


Changed
~~~~~~~


- Revamped the way the external systems work, which fixes a lot of problems.
- The fastr_pipeline_launcher is now using python3 and fastr/3.2.0 to run pipelines.
- Adds set_state to the studygovernor client.


Fixed
~~~~~

- The PIDB callback is working with the newer versions of the PIDB tool.



6.1.0 - 2020-03-27
------------------

Added
~~~~~

- Multiple workflows: Multiple worflows can co-exist next to each other
- Views for monitoring the co-existing workflows


6.0.1 - 2020-01-30
------------------

Fixed
~~~~~

- Python 2to3 bug in the studygov-workflow-visualisation CLI tool.


6.0.0 - 2020-01-27
------------------

Added
~~~~~

- Adds Role Based access and user management, please see the documentation.
- Added support for python 3.6 or greater
- Added health metric end-points
- Added api versioning
- Testing using pytest and tox
- Migrations
- Automated docker build/upload to registry.gitlab.com/radiology/infrastructure/study-governor
- Example docker-compose file (cant be found in docker directory)
- CI on Gitlab


Changed
~~~~~~~

- The api has been moved from /data to /api/v1
- Refactored the api to make use of blueprints for versioning and portability
- Refactored the config to make use of ``python-dotenv`` for configuration via ``.env`` files
- Refactored the config to make use of environment variables prefixed with ``STUDYGOV``, ``FLASK`` or ``SQLALCHEMY``
- Refactored the app creation process by injecting the app into the extensions on create time instead of on import time.
- Changed the way the app is loaded by making use of ``.env`` and ``.flaskenv`` files. Run the app from now on via ``flask run``.
- This package is renamed from syncotron to studygovernor
- jinja2_highlight is replaced by prism
- Moved the repository to Gitlab


Removed
~~~~~~~

- Removed support for python 3.5 or lesser.
- Removed the pontiac data library from this repository. (And moved it to the appropriate project repo).


3.0.0 - 2018-10-24
------------------

Added
~~~~~

- Adds workflow visualization based on GraphViz with colors
- Can add a freetext entry in the ``/data/experiments/<id>/state`` put method
- Adds a send mail callback function
- Added Dockerfile + bibucket-pipeline.yml for automated docker builds

Changed
~~~~~~~

- Changing the state via ``/data/experiments/<id>/state`` now accepts both state
  as ``/data/states/<id-or-label>`` as well as just ``<id-or-label>``
- The error response in ``/data/experiments/<id>/state`` now marshals properly
- The /data/subjects/id api is now accepting a ``filter_field`` argument in a
  query string. You can force to filter on ``id`` or ``label``
- Show external subject id links in subject template
- Added variable $SUBJECT_ID to create_task callback for use in base tasks


Fixed
~~~~~

- Added Flask-Script to the requirements in order for the flask manager to work.
- Added params attribute to the get method of the SyncotronSession class.
- Fixed deprecated imports to flask-script and flask-migrate.




2.0.0 - 2017-06-26
------------------

Added
~~~~~

- Web frontend
- Swagger documentation for REST API
- Sentry.io support

