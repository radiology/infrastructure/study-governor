=============
StudyGovernor
=============

Study Governor is a controller for data in large population imaging studies.

-------------
Documentation
-------------

The Study Governor is documented at https://study-governor.readthedocs.io/

----------
Deployment
----------

The StudyGovernor deployment typically uses either Kubernetes or
docker-compose. Documentation on how to deploy the StudyGovernor
can be found in the deployment section in the documentation at
https://study-governor.readthedocs.io/en/latest/static/howto.html#studygovernor-deployment

-----------
Source Code
-----------

The StudyGovernor is open-source and licensed under an Apache 2.0 License.
The source code found be found at GitLab: https://gitlab.com/radiology/infrastructure/study-governor/