# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import os
import json
from pathlib import Path

from studygovernor import models
from studygovernor import create_app
from studygovernor.api.v2.base import api

# Create an app and set some directories for generation of automatic api description
docs_dir = Path(__file__).parent
spec_dir = docs_dir / '_build' / '_api_spec'
config_file = docs_dir.parent / 'studygovernor' / 'tests' / 'config' / 'test_config.json'
app = create_app({
    'TESTING': True,
    'SQLALCHEMY_DATABASE_URI': 'sqlite:///:memory:',
    'SERVER_NAME': 'studygovernor',
    'SECRET_KEY': 'o8[nc2foeu2foe2ij',
    'SECURITY_PASSWORD_SALT': 'sgfms8-tcfm9de2nv',
    'STUDYGOV_CELERY_BROKER': "pyamqp://guest@localhost",
})

# stylistic changes
html_logo = '../studygovernor/static/img/logo.svg'
html_favicon = '../studygovernor/static/favicon.ico'

html_static_path = ['static/sources']
html_css_files = ['extra.css']

# Read the docs hacks
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
if on_rtd:
    print('[conf.py] On Read the Docs')

# -- Project information -----------------------------------------------------

project = 'StudyGovernor'
copyright = '2015-2023, Hakim Achterberg, Marcel Koek, Adriaan Versteeg'
author = 'Hakim Achterberg, Marcel Koek, Adriaan Versteeg'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx_copybutton',
    'sphinxcontrib.redoc',
    'sphinx.ext.autodoc',
    'sphinx_autodoc_typehints',
    'sphinxcontrib.httpdomain',
    'sphinxcontrib.openapi',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#if not on_rtd:
    #import sphinx_rtd_theme
    #html_theme = 'sphinx_rtd_theme'
    #html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
#else:
    #html_theme = 'default'

html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#html_static_path = ['_static']

with app.app_context():
    os.makedirs(spec_dir, exist_ok=True)
    with open(spec_dir / 'v2.json', 'w') as fout:
            json.dump(api.__schema__, fout)

redoc = [
    {
        'name': 'Study Governor API v2',
        'page': 'api_v2',
        'spec': '_build/_api_spec/v2.json',
        'embed': True,
    },
]
