***************
Study Governor
***************

The Study Governor is an automated data and state manager. This service keeps 
track of the status of all data samples (usually scan sessions) in a study
and automates the process. This is the brain of the imaging infrastructure.
It exposes a REST API and a web interface to monitor and manage the study process.

The Study Governor is licensed with the Apache 2.0 license.

You can find the source code in `this repository <https://gitlab.com/radiology/infrastructure/study-governor>`_


Study Governor documentation
=============================
.. toctree::
    :maxdepth: 3

    static/introduction.rst
    static/howto.rst
    static/deployment.rst
    static/manual.rst
    static/api.rst
    static/reference.rst
    static/changelog.rst
..
    static/tutorial.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
