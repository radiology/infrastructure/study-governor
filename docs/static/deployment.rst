**************
K8s Deployment
**************

This page contains additional information on setting up StudyGovernor when using Kubernetes. For the initial deployment
see section 'Deployment using kubernetes' on the page 'How To's'.

Configuring StudyGovernor deployment
------------------------------------

Copy the values from `values.yaml <https://gitlab.com/radiology/infrastructure/study-governor/-/raw/master/charts/values.yaml?ref_type=heads&inline=false>`_. Important keys to change are on the top of the values file:

.. list-table:: Title
   :widths: 25 25 50
   :header-rows: 1

   * - Key
     - Type
     - Description
   * - ingress
     - Dictionary
     - Configure ingress
   * - hostname
     - String
     - Configure hostname
   * - hostpath
     - String starting with '/'
     - Hostname of the service
   * - admin-password
     - String
     - Password for admin user


Adding Study specific data
--------------------------

In the values file under studygovernor --> project add the key studySecrets. Here we can add multiple secrets by creating a list. Each item in the list should have a name and a path (directory) where the file should end up.

.. code-block:: yaml

    studySecrets:
      - name: included-patients
        path: /project/study-data
      - name: scan-dates
        path: /project/study-data

The name references an existing secret and should either be created manually, through your specific deployment method or for example kubeseal.
After the deployment the data should be available for the workers in the directory specified. The filename is specified by the secret itself. The secret should look something like this:

.. code-block:: yaml

    apiVersion: v1
    kind: Secret
    metadata:
      name: study-csv
    data:
      included-patients.csv: MDAxOzAxLTAxLTIwMDQK
