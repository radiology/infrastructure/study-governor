********
How To's
********

StudyGovernor Deployment
------------------------

The StudyGovernor is a complex system that uses external services: the data
stored in a Postgres database, callbacks are executed on worker nodes, and to
communicate between workers and the main process Celery with RabbitMQ is used.
This means to run a StudyGovernor and have all functionality all these services
need to be configured correctly.

.. figure:: images/studygovernor_deployment.svg
    :alt: Components overview

    Components used in deployment of StudyGovernor

To make deployment easier we provide ways to run the StudyGovernor using containers.
For docker-compose and kubernetes basic configurations are provided.

Deployment using docker-compose
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In the repository there is a directory ``.docker`` that contains a docker-compose
file. Starting a StudyGovernor is as simple as:

.. code-block:: bash

    $ cd .docker
    $ docker-compose build
    $ docker-compose up

By default the StudyGovernor will be reachable at ``localhost:8000`` and the account ``admin``, password ``admin``,
will be available.
The configuration of the StudyGovernor can be done by setting environment variables in the ``docker-compose`` file.
For the list of variables to are used see the section 'StudyGovernor Configuration' at the end of this page.

Deployment using kubernetes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Setting up a StudyGovernor on you own computer in K8s is as simple as executing a helm install.

.. code-block:: bash

    $ kubectl create namespace studygovernor
    $ helm install registry.gitlab.com/radiology/infrastructure/study-governor:8.0.0

The StudyGovernor should now be running on ``localhost/studygovernor`` and is ready to be setup.

.. note::
    The repository contains helm charts for deployment using kubernetes. They can be
    found in the ``charts`` subdirectory.

Configuration values for kubernetes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following config values that map to environment variables are defined in
the ``values.yaml``:

===========================  ================================================== =============================================
Environment variable         Config value in Helm Charts                        Notes
===========================  ================================================== =============================================
STUDYGOV_PORT                studygovernor.port
STUDYGOV_INSTANCE_NAME       studygovernor.instanceName
STUDYGOV_PROJECT_NAME        studygovernor.projectName
STUDYGOV_EMAIL_PREFIX        studygovernor.emailPrefix
STUDYGOV_EMAIL_FROM          studygovernor.emailPrefix
STUDYGOV_EMAIL_TO            studygovernor.emailPrefix
STUDYGOV_CALLBACK_METHOD     studygovernor.callbacks.method
STUDYGOV_EXTERNAL_HOSTNAME   studygovernor.externalHostname
STUDYGOV_EXTERNAL_PROTO      studygovernor.externalProtocol
STUDYGOV_EXTERNAL_PORT       studygovernor.externalPort                         Default: service.port
GUNICORN_WORKER_CLASS        studygovernor.gunicorn.workerClass
GUNICORN_WORKER_CONNECTIONS  studygovernor.gunicorn.workerConnections
GUNICORN_TIMEOUT             studygovernor.gunicorn.timeout
DB_SCHEMA                    studygovernor.db.schema
DB_NAME                      studygovernor.db.name
DB_USER                      studygovernor.db.user
DB_HOST                      studygovernor.db.host
DB_PORT                      studygovernor.db.port
STUDYGOV_ADMIN_USERNAME      studygovernor.admin.username
STUDYGOV_ADMIN_EMAIL         studygovernor.admin.email
STUDYGOV_ADMIN_PASSWORD      studygovernor.admin.password
STUDYGOV_ADMIN_FORCE_UPDATE  studygovernor.admin.force_update
SCRIPT_NAME                  $pathPrefix                                        Default value derived from Ingress
APPLICATION_ROOT             $pathPrefix                                        Default value derived from Ingress
PROJECT_REPO                 studygovernor.project.git.url                      Enabled by studygovernor.project.git.enabled
PROJECT_BRANCH               studygovernor.project.git.branch
PROJECT_RELATIVE_PATH        studygovernor.project.git.relativePath
STUDYGOV_XNAT_PROJECT        studygovernor.fastr.xnatProject
STUDYGOV_PROJECT_HOME        studygovernor.fastr.projectHome
STUDYGOV_PROJECT_SCRATCH     studygovernor.fastr.projectScratch
SECURITY_REGISTERABLE        studygovernor.security.registerable
SECURITY_CHANGEABLE          studygovernor.security.changeable
SECURITY_RECOVERABLE         studygovernor.security.recoverable
SECURITY_CONFIRMABLE         studygovernor.security.confirmable
===========================  ================================================== =============================================

..
  For the explanation of these environment variables please see the :ref:`Configuration` section.

Setting up a development environment for kubernetes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need a number of tools to start developing for the StudyGovernor
on kubernetes locally.

* `Docker <https://www.docker.com>`_
* `k3d <https://k3d.io>`_
* `helm <https://helm.sh>`_
* `Tilt <https://tilt.dev>`_

Once you have docker, k3d, helm and Tilt installed a kubernetes cluster
need to be created. There is a Makefile that will create a basic cluster
using k3d:

.. code-block:: bash

    $ make up

The a development environment
can be started by using the ``Tiltfile`` in the repository root:

.. code-block:: bash

    $ tilt up

The StudyGovernor will be available at ``localhost/studygovernor`` by default.
The admin account will be set to ``admin``, password ``supersecretpassword``.

To stop running the system use the following commands:

.. code-block:: bash

    $ tilt down
    $ make down

The first command stops all StudyGovernor containers and the second command
clean up the kubernetes cluster.


Setting up local development environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to set up a local development
environment for either developing or just testing the StudyGovernor, a MySQL/mariadb
database and rabbitmq need to be provided.

The database needs to be initialized:

.. code-block:: bash

    # Go the mysql command line (add the -p if you have set a root password).
    $ sudo mysql (-p)

    # Create user
    mysql> CREATE USER '<username>'@'localhost' IDENTIFIED BY '<password>';

    # Create database
    mysql> CREATE DATABASE studygovernor;

    # Grant all permissions of the database to the user.
    mysql> GRANT ALL ON studygovernor.* TO '<username>'@'localhost'; ``

The <username> and <password> needs to be replaced with a username and
password of your choice.

Then you can create a ``.env`` in the directory from which you want to
run the StuydGovernor. A minimal example (matching the bove database
creation) would be:

.. code-block:: bash

    FLASK_APP="studygovernor"
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://<username>:<password>@localhost/studygovernor"
    SECURITY_PASSWORD_SALT="<password salt>"
    SECRET_KEY="<secret key>"
    STUDYGOV_XNAT_PROJECT="TEST"

The password salt and secret key are used for security purposes. The password salt is
described in the `FlaskSecurity documentation <https://flask-security-too.readthedocs.io/en/stable/configuration.html#SECURITY_PASSWORD_SALT>`_
The secret key is described in the
`Flask documentation <https://flask.palletsprojects.com/en/2.2.x/config/#SECRET_KEY>`_
The StudyGovernor needs to be installed in your environment before you can run it, this can simply be done from pypi using pip or from sources in case you want to develop StudyGovernor. Installation from pypi using pip is as simples as:

.. code-block:: bash

    pip install studygovernor

If you want to develop from source we assume that you know how to install a Python package yourself.
Now everything is set up, starting the app can be done using Flask:

.. code-block:: bash

    $ flask run

StudyGovernor Configuration
---------------------------

The StudyGovernor is configured using environment variables. Alternatively a .env
file can be provided to define config variables, for this we use
`python-dotenv <https://pypi.org/project/python-dotenv/>`_ There are a number of
import variables that are required:

========================== === ===========
Variable                   Req Description
========================== === ===========
SQLALCHEMY_DATABASE_URI    YES
STUDYGOV_CELERY_BROKER     YES
SECURITY_PASSWORD_SALT     YES
SECRET_KEY                 YES
STUDYGOV_XNAT_PROJECT      YES
STUDYGOV_PROJECT_HOME      NO
STUDYGOV_PROJECT_SCRATCH   NO
STUDYGOV_INSTANCE_NAME     NO
STUDYGOV_PROJECT_NAME      NO
STUDYGOV_PROJECT_BIN       NO
STUDYGOV_PROJECT_NETWORKS  NO
STUDYGOV_PROJECT_TASKS     NO
STUDYGOV_PROJECT_TEMPLATES NO
STUDYGOV_PROJECT_WORKFLOWS NO
STUDYGOV_EMAIL_FROM        NO
STUDYGOV_EMAIL_TO          NO
STUDYGOV_EMAIL_PREFIX      NO
SECURITY_REGISTERABLE      NO
SECURITY_CHANGEABLE        NO
SECURITY_RECOVERABLE       NO
SECURITY_CONFIRMABLE       NO
========================== === ===========
