************
Introduction
************

Studies are comprised of samples. In medical imaging often we see a scan
session as a sample. A scan session often is processed in steps. When seeing
those steps as states, a scan session can pass
through many states before the final state is achieved and the processing of
the sample is finished.

Imaging studies, and population imaging studies in particular, usually involve tons of samples.
As the number of samples increases, it becomes very complicated to
keep track of state each of sample. A StudyGovernor becomes
very handy in automatically keeping track of which state each sample is in.

The StudyGovernor is a tool developed by the BIGR infrastructure team. It is a
service that keeps track of the state (and previous states) of each sample.
It makes sure that each sample will go through a pre-defined workflow. A sample
will be assigned the root state when created. From there on it will follow
the workflow following the possible transitions that defined for each state.

.. figure:: images/workflow.png
    :scale: 70 %
    :alt: Workflow example

    A representation of a workflow. The boxes represent the states and
    the arrows the possible transitions.

Workflows are typically defined per study and can be fully customised as long
as they can be represented by a directed graph. Each state has callbacks defined
that direct automatic or manual processes. When all callbacks have finished,
the results will determine what transition will be triggered and what the next
state will be.

The StudyGovernor always knows the state for each sample and can give a good
overview of the study. Additionally, each callback and each transition is logged
for provenance, so a complete history of all samples will be available at all times.