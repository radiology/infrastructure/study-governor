******
Manual
******

Workflows
---------

Workflows are designed by defining the states and the allowed transitions. Using these states and transitions a graph is build. 
The study governor software will keep track of the states each experiment is in and has passed. It will also enforce valid transistions.
At each state/node a :ref:`Callback <Callbacks>` can be executed. These callbacks can be used to start custom script/commands, run pipelines, or send emails.

Workflows are defined using a YAML file, which has one main field "states". Transitions and callbacks are defined as part of the state.

There's two additinal options "external_systems" and "scantypes" which will be discussed later on.

States
~~~~~~

States are defined in the states field of the workflow YAML. Each workflow needs an "untracked" state. This is the state each of the experiments start in.
This state is limited to only one output transition and is transitioned by the study governor. This state is also the most basic state we can define.
Each state needs at least the following fields:

============ ===========
fields       Description
============ ===========
label        The name of this state, must be unique.
callbacks    A list of callback definitions.
freetext     A description of this state.
transitions  A list of possible transitions from this state
============ ===========

When a state is entered all callbacks will be executed (or skipped if the condition is unmet).
Then when all callback executions are finished, transitions will be checked in order to find the
first transition whose condition is met. This transition will be used to update the state of
the experiment.

Example of a state:

.. code-block:: YAML

     callbacks:
       - <callback definition>
       - <callback definition>
     freetext: "The pre-initial state for untracked data"
     label: untracked
     transitions:
       - <transition definition>
       - <transition definition>

Transitions
~~~~~~~~~~~

Besides states we need to define the possible tranistions between those states. We define a source a destination and a contition:

=========== ===========
fields      Description
=========== ===========
destination Label of the destination state.
condition    Extra condition for the transition. Should be one-liner Python code that results in a bool value
=========== ===========

Example of a transition:

.. code-block:: YAML

    destination: step3
    condition: "experiment.scandate > datetime.datetime(2019, 1, 1)"


Callbacks
~~~~~~~~~

==================== ======= ===========
fields               type    Description
==================== ======= ===========
label                string  Name to identify the callback
description          string  Just some text to describe the callback
function             string  Label of the destination state.
callback_arguments   mapping The arguments to pass to the callback function, should be a mapping for keyword arguments
run_timeout          int     Timeout (in minutes) before a callback execution run is considered failed
wait_timeout         int     Timeout (in minutes) before a callback execution waiting for manual response is considered failed
initial_delay        int     Time (in seconds) before the callback is allowed to start running
condition            string  A condition for the callback. Should be a one-line Python code that results in a bool
variable_map         mapping Dict that indicates what variables from the callback should be stored back into the
                             experiment. Has the form {"target": "source"}.
==================== ======= ===========

The callback function and arguments will be used to perform the actual execution of this callback. The
callback_arguments should be a mapping where the keys match the arguments of the callback function.

The `variable_map is` specified as ``{"target": "source"}``, this means that
``{"key_a": "key_b"}`` will result in an assignment similar to:
``experiment.variables['key_a'] = result_data['key_b']``.

The target can be a
`JSON pointer <https://datatracker.ietf.org/doc/html/rfc6901>`_ as implemented by the
`jsonpointer Python packge <https://python-json-pointer.readthedocs.io>`_.
This makes it possible to only take parts of results to be a variable. For example:

.. code-block:: Python

    # Assume result data
    result_data = {
         "names": ["John", "Eric", "Terry", "Graham", "Terry"],
    }

    # The following variable map:
    # {"name": "/names/1"}
    #  Would result in the following:
    experiment.variables['name'] = result_data['names'][1]  # That'd be Eric

.. note::

    If the wait_timeout is set to 0, there won't be a wait step and the callback will
    be considered finished as soon as the step finished.

.. note::

    By default variables in the result value of the callback are not store in the experiment,
    to enable storing result variables back into the experiment use the ``variable_map`` attribute.

Example of a callback:

.. code-block:: YAML

    label: step2_callback1
    function: test_callback
    callback_arguments:
      foo: bar
      answer: 42
    run_timeout: 30
    wait_timeout: 0
    initial_delay: 1
    description: "Test callback that just sets some vars"
    condition: "experiment.variables.get('test') == 42"


Conditions
~~~~~~~~~~

Conditions for callbacks and transitions are following the
same syntax. A condition string must be a line of Python code
that evaluates to a True or False when run.

Examples of two conditions:


.. code-block:: YAML

    condition: "experiment.variables.get('test') == 42"
    condition: "experiment.scandate > datetime.datetime(2019, 1, 1)"


For the condition
evaluation only a limited set of variables are available:

* `experiment`: An object with the properties: `label`, `scandate`, `variables` and `state`
* `datetime`: The python datetime module for time calculations/comparisons.
* `callbacks`: A `dict` with the callback label as the key and the callback exection as a the value.

.. note:: Only if a transition condition is evaluated there
          are callbacks set. For conditions on starting a callback
          the callbacks dictionary will be empty.


The Experiment has the following properties:


=========  ========  ============
Property   Type      Description
=========  ========  ============
label      str       The experiment label
scandate   datetime  Datetime object with the scan time of the experiment
state      str       Label of the current state of the experiment
variables  dict      Dict containing all variables set on this experiment,
                     the keys are the variable names, the values are variable
                     value and the value type depends on the variable type.
=========  ========  ============


The CallbackExecution has the following properties:


=============  =======================  ==========
Property       Type                     Description
=============  =======================  ==========
status         CallbackExecutionStatus  The status of the callback, and Enum that can be used as a string too
result         CallbackExecutionResult  The result of the callback, and Enum that can be used as a string too
result_values  dict                     Dictionary with the result values of the callback, the contents of this
                                        are what is returned by a callback and therefore depended on the callback
                                        function that was called.
=============  =======================  ==========


Permissions
-----------

The permissions are defined as follows:

=================================   =================================================================================================================================
Permission                          Description
=================================   =================================================================================================================================
``sample_state_update``             Allows updating a sample. (super-user)

``sample_add``                      Allows adding a sample. (super-user)
``sample_update``                   Allows updating a sample. (admin)
``sample_delete``                   Allows deleting a sample. (admin)

``roles_manage``                    Allows adding and removing roles from users
``user_read``                       Allows seeing your user information.
``user_read_all``                   Allows seeing all users.
``user_add``                        Allows adding users.
``user_update_all``                 Allows updating all users.
``user_delete``                     Allows deleting users.
``upload_data``                     Allows the uploading of data to the StudyGovernor (super-user)
=================================   =================================================================================================================================


.. _Callbacks:

Callbacks
---------

This is a description of the various callbacks that are available in the StudyGovernor.
The first two arguments of each allback are always ``callback_execution_data`` and ``config``.
These are automatically supplied by the StudyGovernor and do not need to be defined.
All callbacks should always accept these two arguments as the first two arguments.

.. autofunction:: studygovernor.callbacks.command.command

.. autofunction:: studygovernor.callbacks.create_task.create_task

.. autofunction:: studygovernor.callbacks.create_taskgroup.create_taskgroup

.. autofunction:: studygovernor.callbacks.external_program.external_program

.. autofunction:: studygovernor.callbacks.fastr.fastr

.. autofunction:: studygovernor.callbacks.python_function.python_function

.. autofunction:: studygovernor.callbacks.send_mail.send_mail

.. autofunction:: studygovernor.callbacks.sleep.sleep
