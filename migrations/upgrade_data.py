import argparse
from studygovernor import create_app
from studygovernor import models

from flask_security import SQLAlchemyUserDatastore
from flask_security.utils import hash_password

db = models.db


def add_roles(username, password):
    print(f'Adding default roles')
    admin_role = models.Role(name='admin', description='Admin Role', permissions='task_read_all,task_update_all,task_add,task_delete,template_add,template_update,template_delete,tag_add,tag_update,tag_delete,user_read_all,user_update_all,user_add,user_delete,group_read_all,group_add,group_update,group_delete,roles_manage')
    db.session.add(admin_role)
    super_user_role = models.Role(name='superuser', description='Super User Role', permissions='task_read_all,task_update_status_all,task_update_user,task_update_lock_all,tag_add,tag_update,user_read_all,user_update_assignment_weight,group_read_all,group_update')
    db.session.add(super_user_role)
    user_role = models.Role(name='user', description='Normal User Role', permissions='task_read,task_update_status,user_read,group_read')
    db.session.add(user_role)
    db.session.commit()

    print(f'Adding new user {username}')
    user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
    user = user_datastore.create_user(
        username=username,
        name='Admin User',
        email='',
        password=hash_password(password),
    )
    db.session.add(user)
    db.session.commit()

    user.roles = [admin_role]
    db.session.commit()


def upgrade(username, password):
    add_roles(username, password)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Migrate data to handle role base access. This inserts the default setup.")
    parser.add_argument('--username', default='admin', help="Defines the username for the new admin user")
    parser.add_argument('--password', required=True, help="Defines the password for the new admin user")
    args = parser.parse_args()
    print(args)

    with create_app().app_context():
        upgrade(args.username, args.password)
