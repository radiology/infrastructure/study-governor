-- Change DB engine for all tables
ALTER TABLE `transition_has_condition` ENGINE=InnoDB;
ALTER TABLE `transition` ENGINE=InnoDB;
ALTER TABLE `subject` ENGINE=InnoDB;
ALTER TABLE `state` ENGINE=InnoDB;
ALTER TABLE `scantype` ENGINE=InnoDB;
ALTER TABLE `scan` ENGINE=InnoDB;
ALTER TABLE `external_system` ENGINE=InnoDB;
ALTER TABLE `external_subject_links` ENGINE=InnoDB;
ALTER TABLE `external_experiment_links` ENGINE=InnoDB;
ALTER TABLE `experiment` ENGINE=InnoDB;
ALTER TABLE `condition` ENGINE=InnoDB;
ALTER TABLE `action` ENGINE=InnoDB;

-- Add all foreign constraints
ALTER TABLE action ADD CONSTRAINT `fk_action_transition_id_transition` FOREIGN KEY (`transition_id`) REFERENCES `transition` (`id`);
ALTER TABLE action ADD CONSTRAINT `fk_action_experiment_id_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`);
ALTER TABLE experiment ADD CONSTRAINT `fk_experiment_subject_id_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`);
ALTER TABLE external_experiment_links ADD CONSTRAINT `fk_external_experiment_links_experiment_id_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`);
ALTER TABLE external_experiment_links ADD CONSTRAINT `fk_external_experiment_links_external_system_id_external_system` FOREIGN KEY (`external_system_id`) REFERENCES `external_system` (`id`);
ALTER TABLE external_subject_links ADD CONSTRAINT `fk_external_subject_links_subject_id_subject` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`);
ALTER TABLE external_subject_links ADD CONSTRAINT `fk_external_subject_links_external_system_id_external_system` FOREIGN KEY (`external_system_id`) REFERENCES `external_system` (`id`);
ALTER TABLE scan ADD CONSTRAINT `fk_scan_experiment_id_experiment` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`);
ALTER TABLE scan ADD CONSTRAINT `fk_scan_scantype_id_scantype` FOREIGN KEY (`scantype_id`) REFERENCES `scantype` (`id`);
ALTER TABLE transition ADD CONSTRAINT `fk_transition_source_state_id_state` FOREIGN KEY (`source_state_id`) REFERENCES `state` (`id`);
ALTER TABLE transition ADD CONSTRAINT `fk_transition_destination_state_id_state` FOREIGN KEY (`destination_state_id`) REFERENCES `state` (`id`);
ALTER TABLE transition_has_condition ADD CONSTRAINT `fk_transition_has_condition_transition_id_transition` FOREIGN KEY (`transition_id`) REFERENCES `transition` (`id`);
ALTER TABLE transition_has_condition ADD CONSTRAINT `fk_transition_has_condition_condition_id_condition` FOREIGN KEY (`condition_id`) REFERENCES `condition` (`id`);
