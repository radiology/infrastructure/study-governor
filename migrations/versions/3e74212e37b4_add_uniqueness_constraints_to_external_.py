"""Add uniqueness constraints to external_ids

Revision ID: 3e74212e37b4
Revises: 8288ee679937
Create Date: 2020-04-25 11:36:28.114183

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm

from studygovernor import models

# revision identifiers, used by Alembic.
revision = '3e74212e37b4'
down_revision = '8288ee679937'
branch_labels = None
depends_on = None


def upgrade():
    # Bind ORM and Alembic session together
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    # Fix SubjectLinks data to avoid duplicates
    counts = session.query(
        models.ExternalSubjectLinks.external_system_id,
        models.ExternalSubjectLinks.subject_id,
        sa.func.count(models.ExternalSubjectLinks.id)
    ).group_by(
        models.ExternalSubjectLinks.external_system_id,
        models.ExternalSubjectLinks.subject_id
    ).all()

    # Go through all duplicates and remove them
    for system_id, sub_id, count in counts:
        # Only need to work when there are duplicates
        if count < 2:
            continue

        # Select all but last entry
        to_remove = session.query(models.ExternalSubjectLinks).filter(
            models.ExternalSubjectLinks.external_system_id == system_id,
            models.ExternalSubjectLinks.subject_id == sub_id
        ).order_by(models.ExternalSubjectLinks.id).limit(count-1).all()

        # Remove them from the database
        for item in to_remove:
            session.delete(item)

    # Fix ExperimentsLinks data to avoid duplicates
    counts = session.query(
        models.ExternalExperimentLinks.external_system_id,
        models.ExternalExperimentLinks.experiment_id,
        sa.func.count(models.ExternalExperimentLinks.id)
    ).group_by(
        models.ExternalExperimentLinks.external_system_id,
        models.ExternalExperimentLinks.experiment_id
    ).all()

    # Go through all duplicates and remove them
    for system_id, exp_id, count in counts:
        # Only need to work when there are duplicates
        if count < 2:
            continue

        # Select all but last entry
        to_remove = session.query(models.ExternalExperimentLinks).filter(
            models.ExternalExperimentLinks.external_system_id == system_id,
            models.ExternalExperimentLinks.experiment_id == exp_id
        ).order_by(models.ExternalExperimentLinks.id).limit(count-1).all()

        # Remove them from the database
        for item in to_remove:
            session.delete(item)

    # Commit to make sure duplicates are gone
    session.commit()

    # Add new constraints
    op.create_unique_constraint("uix_subject_id_external_system_id", "external_subject_links", ['subject_id', 'external_system_id'])
    op.create_unique_constraint("uix_experiment_id_external_system_id", "external_experiment_links", ['experiment_id', 'external_system_id'])


def downgrade():
    # Remove the constraints, FIXME: This is not possible!
    op.drop_constraint('uix_subject_id_external_system_id', 'external_subject_links', type="unique")
    op.drop_constraint('uix_experiment_id_external_system_id', 'external_experiment_links', type="unique")
