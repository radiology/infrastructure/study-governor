# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import deepdiff

from studygovernor.tests.helpers import basic_auth_authorization_header


def test_actions_put_and_get_as_admin(client, experiment_data):
    response = client.put('/api/v1/actions/1', headers=basic_auth_authorization_header("admin", "admin"), json={
        "success": True,
        "return_value": "string",
        "end_time": "2020-02-11T18:12:03.151540"
    })
    assert response.status_code == 200

    #   get action list
    response = client.get('/api/v1/actions', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'actions': ['/api/v1/actions/1', '/api/v1/actions/2', '/api/v1/actions/3', '/api/v1/actions/4']}

    #   get one action
    response = client.get('/api/v1/actions/1', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)

    difference = deepdiff.DeepDiff(data, {
        'uri': '/api/v1/actions/1',
        'api_uri': 'http://localhost/api/v1/actions/1',
        'web_uri': 'http://localhost/actions/1',
        'experiment': '/api/v1/experiments/1',
        'transition': '/api/v1/transitions/1',
        'success': True,
        'return_value': 'string',
        'freetext': None,
        'executions': [],
    }, exclude_paths=[
        "root['start_time']",
        "root['end_time']",
    ])

    assert difference == {}

#   try to get a non-present action
    response = client.get('/api/v1/actions/999', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_actions_put_and_get_as_superuser(client, experiment_data):
    response = client.put('/api/v1/actions/1', headers=basic_auth_authorization_header("superuser", "superuser"), json={
        "success": True,
        "return_value": "string",
        "end_time": "2020-02-11T18:12:03.151540"
    })
    assert response.status_code == 403

#   get action list
    response = client.get('/api/v1/actions', headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'actions': ['/api/v1/actions/1', '/api/v1/actions/2', '/api/v1/actions/3', '/api/v1/actions/4']}

#   get one action
    response = client.get('/api/v1/actions/1', headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    difference = deepdiff.DeepDiff(data, {
        'uri': '/api/v1/actions/1',
        'api_uri': 'http://localhost/api/v1/actions/1',
        'web_uri': 'http://localhost/actions/1',
        'experiment': '/api/v1/experiments/1',
        'transition': '/api/v1/transitions/1',
        'success': True,
        'return_value': 'No callback for state',
        'freetext': None,
        'executions': [],
    }, exclude_paths=[
        "root['start_time']",
        "root['end_time']",
    ])

    assert difference == {}

    #   try to get a non-present action
    response = client.get('/api/v1/actions/999', headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 404


def test_actions_put_and_get_as_user(client, experiment_data):
    response = client.put('/api/v1/actions/1', headers=basic_auth_authorization_header("user2", "user2"), json={
        "success": True,
        "return_value": "string",
        "end_time": "2020-02-11T18:12:03.151540"
    })
    assert response.status_code == 403

#   get action list
    response = client.get('/api/v1/actions', headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'actions': ['/api/v1/actions/1', '/api/v1/actions/2', '/api/v1/actions/3', '/api/v1/actions/4']}

#   get one action
    response = client.get('/api/v1/actions/1', headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    difference = deepdiff.DeepDiff(data, {
        'uri': '/api/v1/actions/1',
        'api_uri': 'http://localhost/api/v1/actions/1',
        'web_uri': 'http://localhost/actions/1',
        'experiment': '/api/v1/experiments/1',
        'transition': '/api/v1/transitions/1',
        'success': True,
        'return_value': 'No callback for state',
        'freetext': None,
        'executions': [],
    }, exclude_paths=[
        "root['start_time']",
        "root['end_time']",
    ])

    assert difference == {}

#   try to get a non-present action
    response = client.get('/api/v1/actions/999', headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 404


def test_actions_put_and_get_as_worker(client, experiment_data):
    response = client.put(
        '/api/v1/actions/1',
        headers=basic_auth_authorization_header("localworker", "localworker"),
        json={
            "success": True,
            "return_value": "string",
            "end_time": "2020-02-11T18:12:03.151540"
        }
    )
    assert response.status_code == 200

# TODO is it ok that a worker can put an action. Probably yes.

#   get action list
    response = client.get('/api/v1/actions', headers=basic_auth_authorization_header("localworker", "localworker"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'actions': ['/api/v1/actions/1', '/api/v1/actions/2', '/api/v1/actions/3', '/api/v1/actions/4']}

#   get one action
    response = client.get('/api/v1/actions/1', headers=basic_auth_authorization_header("localworker", "localworker"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    difference = deepdiff.DeepDiff(data, {
        'uri': '/api/v1/actions/1',
        'api_uri': 'http://localhost/api/v1/actions/1',
        'web_uri': 'http://localhost/actions/1',
        'experiment': '/api/v1/experiments/1',
        'transition': '/api/v1/transitions/1',
        'success': True,
        'return_value': 'string',
        'freetext': None,
        'executions': [],
    }, exclude_paths=[
        "root['start_time']",
        "root['end_time']",
    ])

    assert difference == {}

    #   try to get a non-present action
    response = client.get('/api/v1/actions/999', headers=basic_auth_authorization_header("localworker", "localworker"))
    assert response.status_code == 404
