# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header
from studygovernor.tests.helpers import admin_auth
from studygovernor.tests.helpers import superuser_auth
from studygovernor.tests.helpers import user_1_auth
from studygovernor.tests.helpers import user_2_auth
from studygovernor.tests.helpers import localworker_auth


def test_subjects_get(client, app_config, cohort_data):
    # Get subject list
    response = client.get('/api/v1/cohorts', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {"cohorts": ['/api/v1/cohorts/1']}


def test_subject_get(client, app_config, cohort_data):
    response = client.get('/api/v1/cohorts/1', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)

    assert data == {'description': 'Some test cohort',
                    'external_urls': {},
                    'label': 'test_cohort',
                    'subjects': [],
                    'uri': '/api/v1/cohorts/1'}


def test_cohort_post(client, app_config, cohort_data):
    response = client.post('/api/v1/cohorts', headers=admin_auth, json={
        "label": "cohort2",
        "description": "second test cohort"
    })
    assert response.status_code == 201

    data = response.get_json()
    print(data)
    assert data == {'uri': '/api/v1/cohorts/2',
                    'label': 'cohort2',
                    'description': 'second test cohort',
                    'external_urls': {},
                    'subjects': []}

    # Get subject list again, now with data
    response = client.get('/api/v1/cohorts', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {"cohorts": ['/api/v1/cohorts/1',  '/api/v1/cohorts/2']}
