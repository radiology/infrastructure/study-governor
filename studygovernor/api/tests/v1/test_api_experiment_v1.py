# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header


def _test_experiments_get(client, user, password):
    # Get experiment list
    response = client.get('/api/v1/experiments',
                          headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        "experiments": [
            '/api/v1/experiments/1',
            '/api/v1/experiments/2',
            '/api/v1/experiments/3',
            '/api/v1/experiments/4'
        ]
    }

    response = client.get('/api/v1/experiments/2?filter_field=subject',
                          headers=basic_auth_authorization_header(user, password))

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/experiments/2',
        'api_uri': 'http://localhost/api/v1/experiments/2',
        'web_uri': 'http://localhost/experiments/2',
        'subject': '/api/v1/subjects/1',
        'label': 'Experiment_1_2',
        'scandate': '2019-01-02T00:00:00',
        'state': '/api/v1/experiments/2/state',
        'external_ids': {},
        'variable_map': {},
    }


def test_experiments_get_as_admin(client, experiment_data):
    _test_experiments_get(client, "admin", "admin")


def test_experiments_get_as_superuser(client, experiment_data):
    _test_experiments_get(client, "superuser", "superuser")


def test_experiments_get_as_user(client, experiment_data):
    _test_experiments_get(client, "user1", "user1")


def test_experiments_get_as_worker(client, experiment_data):
    _test_experiments_get(client, "localworker", "localworker")


def test_experiment_get_nonexisting(client, experiment_data):
    # Get non-existing experiment
    response = client.get('/api/v1/experiments/100', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_experiment_post_as_admin(client, workflow_test_data, subject_data):
    # Post a valid experiment
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_1",
        "subject": "Test Subject_1",
        "scandate": "2001-05-28T09:30:45.123456"
    })
    assert response.status_code == 201

    # Post an invalid experiment (subject reference doesn't exist)
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_7",
        "subject": "Test Subject_7",
        "scandate": "2001-05-28T09:30:45.123456"
    })
    assert response.status_code == 404

    # Post a valid experiment
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_2",
        "subject": "Test Subject_2",
        "scandate": "2001-05-28T09:30:45.123456"
    })
    assert response.status_code == 201


def test_experiment_post_with_subject_filter_field(client, workflow_test_data, subject_data):
    # Post a valid subject label
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("admin", "admin"),
                           query_string={
                               "subject_filter_field": "label"
                           },
                           json={
                               "label": "test_experiment_6",
                               "subject": "Test Subject_1",
                               "scandate": "2001-05-28T09:30:45.123456"

                           })
    assert response.status_code == 201

    # Post an invalid subject label
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("admin", "admin"),
                           query_string={
                               "subject_filter_field": "label"
                           },
                           json={
                               "label": "test_experiment_7",
                               "subject": "1",
                               "scandate": "2001-05-28T09:30:45.123456"

                           })
    assert response.status_code == 404

    # Post a invalid subject id
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("admin", "admin"),
                           query_string={
                               "subject_filter_field": "id"
                           },
                           json={
                               "label": "test_experiment_8",
                               "subject": "Test Subject_1",
                               "scandate": "2001-05-28T09:30:45.123456"

                           })
    assert response.status_code == 404

    # Post an valid subject id
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("admin", "admin"),
                           query_string={
                               "subject_filter_field": "id"
                           },
                           json={
                               "label": "test_experiment_9",
                               "subject": "1",
                               "scandate": "2001-05-28T09:30:45.123456"

                           })
    assert response.status_code == 201

    # Post an valid subject id
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("admin", "admin"),
                           json={
                               "label": "test_experiment_10",
                               "subject": "1",
                               "scandate": "2001-05-28T09:30:45.123456"

                           })
    assert response.status_code == 201

    # Post an valid subject id
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("admin", "admin"),
                           json={
                               "label": "test_experiment_11",
                               "subject": "Test Subject_1",
                               "scandate": "2001-05-28T09:30:45.123456"

                           })
    assert response.status_code == 201


def test_experiment_post_as_superuser(client, workflow_test_data, subject_data):
    # Post a valid experiment as superuser
    data = {
        "label": "Experiment_2_99",
        "subject": "Test Subject_2",
        "scandate": "2001-05-28T09:30:45.123456"
    }
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("superuser", "superuser"),
                           json=data)
    assert response.status_code == 201


def test_experiment_post_as_user(client, workflow_test_data, subject_data):
    # Post a valid experiment as user is not allowed
    data = {
        "label": "test_experiment_1",
        "subject": "Test Subject_1",
        "scandate": "2001-05-28T09:30:45.123456"
    }
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("user1", "user1"),
                           json=data)
    assert response.status_code == 403


def test_experiment_post_as_worker(client, workflow_test_data, subject_data):
    # Post a valid experiment as worker is not allowed
    data = {
        "label": "test_experiment_1",
        "subject": "Test Subject_1",
        "scandate": "2001-05-28T09:30:45.123456"
    }
    response = client.post('/api/v1/experiments',
                           headers=basic_auth_authorization_header("localworker", "localworker"),
                           json=data)
    assert response.status_code == 403


def test_experiment_post_missing_fields(client, workflow_test_data, subject_data):
    # Post an invalid experiment (missing fields)
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_0",
    })
    assert response.status_code == 400


def test_experiment_post_invalid_date(client, workflow_test_data, subject_data):
    # Post with invalid date format
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_0",
        "subject": "Test Subject_2",
        "scandate": "2000/25/05"
    })
    assert response.status_code == 400


def test_experiment_post_nonexisting_subject(client, workflow_test_data, subject_data):
    # Post an experiment relating to a non-existing subject
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_10",
        "subject": "Test Subject_42",
        "scandate": "2000-05-28T09:30:45.123456"
    })
    assert response.status_code == 404

#    data = response.get_json()
#    assert data == {'uri': '/api/v1/experiments/1',
#                    'subject': '/api/v1/subjects/1',
#                    'label': 'test_experiment_1',
#                    'scandate': '2001-05-28T09:30:45.123456',
#                    'state': '/api/v1/experiments/1/state',
#                    'external_ids': {}}
#    assert response.status_code == 201

#    data = response.get_json()
#    assert data == {'uri': '/api/v1/experiments/2',
#                    'subject': '/api/v1/subjects/1',
#                    'label': 'test_experiment_2',
#                    'scandate': '2003-07-22T11:38:25.512456',
#                    'state': '/api/v1/experiments/2/state',
#                    'external_ids': {}}
# TODO .... don't understand this last part yet, copied initially from existing overall testfile


def test_experiments_get_relist(client, experiment_data):
    # Re-list experiments
    response = client.get('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'experiments': [
            '/api/v1/experiments/1',
            '/api/v1/experiments/2',
            '/api/v1/experiments/3',
            '/api/v1/experiments/4'
        ]
    }

    # Get the experiment
    response = client.get('/api/v1/experiments/1', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'uri': '/api/v1/experiments/1',
                    'api_uri': 'http://localhost/api/v1/experiments/1',
                    'web_uri': 'http://localhost/experiments/1',
                    'subject': '/api/v1/subjects/1',
                    'label': 'Experiment_1_1',
                    'scandate': '2019-01-01T00:00:00',
                    'state': '/api/v1/experiments/1/state',
                    'external_ids': {},
                    'variable_map': {},
                    }


def test_experiment_get_by_label(client, experiment_data):
    # Get the experiment by label
    response = client.get('/api/v1/experiments/1', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'uri': '/api/v1/experiments/1',
                    'api_uri': 'http://localhost/api/v1/experiments/1',
                    'web_uri': 'http://localhost/experiments/1',
                    'subject': '/api/v1/subjects/1',
                    'label': 'Experiment_1_1',
                    'scandate': '2019-01-01T00:00:00',
                    'state': '/api/v1/experiments/1/state',
                    'external_ids': {},
                    'variable_map': {},
                    }

    response = client.get('/api/v1/experiments/Experiment_1_1',
                          headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'uri': '/api/v1/experiments/1',
                    'api_uri': 'http://localhost/api/v1/experiments/1',
                    'web_uri': 'http://localhost/experiments/1',
                    'subject': '/api/v1/subjects/1',
                    'label': 'Experiment_1_1',
                    'scandate': '2019-01-01T00:00:00',
                    'state': '/api/v1/experiments/1/state',
                    'external_ids': {},
                    'variable_map': {},
                    }


def test_experiment_get_state(client, experiment_data):
    # Get experiment data
    response = client.get('/api/v1/experiments/1/state', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'state': '/api/v1/states/2', 'workflow': '/api/v1/workflows/1'}

    # Put the state to non-exiting state
    non_existing_data = {'state': 'non-existing-state'}
    response = client.put('/api/v1/experiments/1/state',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json=non_existing_data)

    print(f"{response.get_json()}")
    assert response.status_code == 404

    data = response.get_json()
    assert data == {'state': '/api/v1/states/2',
                    'error':
                        {'errorclass': 'StateNotFoundError',
                         'requested_state': 'non-existing-state',
                         'message': 'Could not find requested state "non-existing-state"'
                         },
                    'success': False}

    # Put the state to invalid target (no transition)
    response = client.put('/api/v1/experiments/1/state',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json={'state': 'step3'})
    assert response.status_code == 409

    data = response.get_json()
    assert data == {
        'state': '/api/v1/states/2',
        'error': {
            'errorclass': 'NoValidTransitionError',
            'sourcestate': '/api/v1/states/2',
            'targetstate': '/api/v1/states/5',
            'message': 'Could not find a valid transition for requested state change (from step1 [2] to step3 [5])'
        },
        'success': False
    }

    # Put the state to a new valid state
    response = client.put('/api/v1/experiments/1/state',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json={'state': 'step2a'})
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'state': '/api/v1/states/3',
                    'error': None,
                    'success': True}

    # Get experiment state again
    response = client.get('/api/v1/experiments/1/state', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'state': '/api/v1/states/3', 'workflow': '/api/v1/workflows/1'}


def test_experiment_post_workflow(client, app_config, experiment_data, second_workflow_test_data):
    # Post a valid experiment, in test_workflow
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_wf1",
        "subject": "Test Subject_1",
        "scandate": "2001-05-28T09:30:45.123456",
        "workflow": "test_workflow"
    })
    assert response.status_code == 201

    new_experiment = response.get_json()
    response = client.get(new_experiment['state'], headers=basic_auth_authorization_header("admin", "admin"))
    assert response.get_json() == {'state': '/api/v1/states/2', 'workflow': '/api/v1/workflows/1'}

    # Post a valid experiment in second_workflow
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_wf2",
        "subject": "Test Subject_1",
        "scandate": "2001-05-28T09:31:45.123456",
        "workflow": "second_workflow"
    })
    assert response.status_code == 201

    new_experiment = response.get_json()
    response = client.get(new_experiment['state'], headers=basic_auth_authorization_header("admin", "admin"))
    assert response.get_json() == {'state': '/api/v1/states/8', 'workflow': '/api/v1/workflows/2'}

    # Post a valid experiment without workflow specification
    response = client.post('/api/v1/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_wf3",
        "subject": "Test Subject_1",
        "scandate": "2001-05-28T09:32:45.123456"
    })
    assert response.status_code == 201

    new_experiment = response.get_json()
    response = client.get(new_experiment['state'], headers=basic_auth_authorization_header("admin", "admin"))
    assert response.get_json() == {'state': '/api/v1/states/8', 'workflow': '/api/v1/workflows/2'}
