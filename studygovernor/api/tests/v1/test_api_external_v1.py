# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header
from studygovernor.tests.helpers import admin_auth
from studygovernor.tests.helpers import superuser_auth
from studygovernor.tests.helpers import user_1_auth
from studygovernor.tests.helpers import user_2_auth
from studygovernor.tests.helpers import localworker_auth


def _test_systems_get(client, user, password):
    response = client.get('/api/v1/external_systems', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'external_systems': [
            '/api/v1/external_systems/1',
            '/api/v1/external_systems/2',
            '/api/v1/external_systems/3',
        ]
    }


def test_systems_get_as_admin(client, app_config):
    _test_systems_get(client, "admin", "admin")


def test_systems_get_as_superuser(client, app_config):
    _test_systems_get(client, "superuser", "superuser")


def test_systems_get_as_user(client, app_config):
    _test_systems_get(client, "user2", "user2")


def test_systems_get_as_worker(client, app_config):
    _test_systems_get(client, "localworker", "localworker")


def _test_system_get(client, user, password):
    response = client.get('/api/v1/external_systems/1', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/external_systems/1',
        'url': 'https://127.0.0.1',
        'system_name': 'XNAT'
    }
    # try to get by name
    response = client.get('/api/v1/external_systems/IFDB', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/external_systems/2',
        'url': 'http://127.0.0.1:5002/v1/',
        'system_name': 'IFDB'
    }

    # try to get unknown system
    response = client.get('/api/v1/external_systems/DRONES', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 404


def test_system_get_as_admin(client, app_config):
    _test_system_get(client, "admin", "admin")


def test_system_get_as_superuser(client, app_config):
    _test_system_get(client, "superuser", "superuser")


def test_system_get_as_user(client, app_config):
    _test_system_get(client, "user2", "user2")


def test_system_get_as_worker(client, app_config):
    _test_system_get(client, "localworker", "localworker")


def test_subject_links_get_as_admin(client, app_config, subject_data):
    response = client.get('/api/v1/external_subject_links', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_subject_links': []}


def test_subject_links_get_as_superuser(client, app_config, subject_data):
    response = client.get('/api/v1/external_subject_links', headers=superuser_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_subject_links': []}


def test_subject_links_get_as_user(client, app_config, subject_data):
    response = client.get('/api/v1/external_subject_links', headers=user_2_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_subject_links': []}


def test_subject_links_get_as_localworker(client, app_config, subject_data):
    response = client.get('/api/v1/external_subject_links', headers=localworker_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_subject_links': []}


def test_subject_link_get_as_admin(client, app_config, subject_links):
    response = client.get('/api/v1/external_subject_links/1', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/external_subject_links/1',
        'external_id': 'External_SubjectID_1',
        'subject': '/api/v1/subjects/1',
        'external_system': '/api/v1/external_systems/1',
    }


def test_subject_link_get_as_superuser(client, app_config, subject_links):
    response = client.get('/api/v1/external_subject_links/1', headers=superuser_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/external_subject_links/1',
        'subject': '/api/v1/subjects/1',
        'external_system': '/api/v1/external_systems/1',
        'external_id': 'External_SubjectID_1'
    }


def test_subject_link_get_as_user(client, app_config, subject_links):
    response = client.get('/api/v1/external_subject_links/2', headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/external_subject_links/2',
        'subject': '/api/v1/subjects/2',
        'external_system': '/api/v1/external_systems/2',
        'external_id': 'External_SubjectID_2'
    }


def test_subject_link_get_as_worker(client, app_config, subject_links):
    response = client.get('/api/v1/external_subject_links/3', headers=localworker_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'uri': '/api/v1/external_subject_links/3',
        'subject': '/api/v1/subjects/2',
        'external_system': '/api/v1/external_systems/3',
        'external_id': 'External_SubjectID_2'
    }


def _test_subject_links_post(client, user, password):
    #TODO: Make this work for testing uniqueness
    data = {
        "subject": "Test Subject_1",
        "external_system": "XNAT",
        "external_id": "External_SubjectID_1"
    }
    expected_response = {
        'external_id': 'External_SubjectID_1',
        'external_system': '/api/v1/external_systems/1',
        'subject': '/api/v1/subjects/1',
        'uri': '/api/v1/external_subject_links/1'
    }

    response = client.post('/api/v1/external_subject_links',
                           headers=basic_auth_authorization_header(user, password),
                           json=data)
    assert response.status_code == 201

    # Try it 2 times, it should return the external subject link, but only 1 should be stored.
    #response2 = client.post('/api/v1/external_subject_links',
    #                        headers=basic_auth_authorization_header(user, password),
    #                        json=data)
    #assert response2.status_code == 201

    # There can be only one
    response = client.get('/api/v1/external_subject_links', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200
    assert response.get_json() == {'external_subject_links': ['/api/v1/external_subject_links/1']}

    response = client.get('/api/v1/external_subject_links/1', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    response_data = response.get_json()
    print(response_data)
    assert response_data == expected_response

    # Check it for the second post as well
    #response_data2 = response2.get_json()
    #print(response_data2)
    #assert response_data2 == expected_response


def test_subject_links_post_as_admin(client, app_config, subject_data):
    _test_subject_links_post(client, "admin", "admin")


# TODO is a superuser allowed to post an external_subject_link?
def test_subject_links_post_as_superuser(client, app_config, subject_data):
    _test_subject_links_post(client, "superuser", "superuser")


def test_subject_links_post_as_user(client, app_config, subject_data):
    response = client.post('/api/v1/external_subject_links', headers=user_1_auth, json={
        "subject": "Test Subject_1",
        "external_system": "NewSystemName_1",
        "external_id": "External_SubjectID_1"
    })
    assert response.status_code == 403

    # There can be only one
    response = client.get('/api/v1/external_subject_links', headers=user_1_auth)
    assert response.status_code == 200
    assert response.get_json() == {'external_subject_links': []}

    response = client.get('/api/v1/external_subject_links/1', headers=user_1_auth)
    assert response.status_code == 404

    data = response.get_json()
    print(data)
    assert data != {
        'external_id': 'External_SubjectID_1',
        'external_system': '/api/v1/external_systems/1',
        'subject': '/api/v1/subjects/1',
        'uri': '/api/v1/external_subject_links/1'
    }


def test_subject_links_post_as_worker(client, app_config, subject_data):
    response = client.post('/api/v1/external_subject_links', headers=localworker_auth, json={
        "subject": "Test Subject_1",
        "external_system": "NewSystemName_1",
        "external_id": "External_SubjectID_1"
    })
    assert response.status_code == 403

    # There can be only one
    response = client.get('/api/v1/external_subject_links', headers=user_1_auth)
    assert response.status_code == 200
    assert response.get_json() == {'external_subject_links': []}

    response = client.get('/api/v1/external_subject_links/4', headers=user_1_auth)
    assert response.status_code == 404

    data = response.get_json()
    print(data)
    assert data != {
        'external_id': 'External_SubjectID_1',
        'external_system': '/api/v1/external_systems/1',
        'subject': '/api/v1/subjects/1',
        'uri': '/api/v1/external_subject_links/1'
    }


def test_experiment_links_get_as_admin(client, app_config, experiment_data):
    response = client.get('/api/v1/external_experiment_links', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_experiment_links': []}


def test_experiment_links_get_as_superuser(client, app_config, experiment_data):
    response = client.get('/api/v1/external_experiment_links', headers=superuser_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_experiment_links': []}


def test_experiment_links_get_as_user(client, app_config, experiment_data):
    response = client.get('/api/v1/external_experiment_links', headers=user_2_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_experiment_links': []}


def test_experiment_links_get_as_localworker(client, app_config, experiment_data):
    response = client.get('/api/v1/external_experiment_links', headers=localworker_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_experiment_links': []}


def _test_experiment_link_get(client, user, password):
    response = client.get('/api/v1/external_experiment_links/1',
                          headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {
        'experiment': '/api/v1/experiments/1',
        'external_id': 'External_ExperimentID_1',
        'external_system': '/api/v1/external_systems/1',
        'uri': '/api/v1/external_experiment_links/1'
    }


def test_experiment_link_get_as_admin(client, app_config, experiment_links):
    _test_experiment_link_get(client, "admin", "admin")


def test_experiment_link_get_as_superuser(client, app_config, experiment_links):
    _test_experiment_link_get(client, "superuser", "superuser")


def test_experiment_link_get_as_user(client, app_config, experiment_links):
    _test_experiment_link_get(client, "user2", "user2")


def test_experiment_link_get_as_worker(client, app_config, experiment_links):
    _test_experiment_link_get(client, "localworker", "localworker")


def _test_experiment_links_post(client, user, password):
    #TODO: Make this work for testing uniqueness
    data = {
        "experiment": "Experiment_1_1",
        "external_system": "XNAT",
        "external_id": "External_Experiment_1_1"
    }

    response = client.post('/api/v1/external_experiment_links',
                           headers=basic_auth_authorization_header(user, password),
                           json=data)
    assert response.status_code == 201

    #response2 = client.post('/api/v1/external_experiment_links',
    #                        headers=basic_auth_authorization_header(user, password),
    #                        json=data)
    #assert response2.status_code == 201

    response = client.get('/api/v1/external_experiment_links', headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'external_experiment_links': ['/api/v1/external_experiment_links/1']}

    #data2 = response2.get_json()
    #print(data2)
    #assert data2 == {'external_experiment_links': ['/api/v1/external_experiment_links/1']}


def test_experiment_links_post_as_admin(client, experiment_data):
    _test_experiment_links_post(client, "admin", "admin")


def test_experiment_links_post_as_superuser(client, experiment_data):
    _test_experiment_links_post(client, "superuser", "superuser")


def test_experiment_links_post_as_user(client, experiment_data):
    response = client.post('/api/v1/external_experiment_links', headers=user_1_auth, json={
        "experiment": "Experiment_1_1",
        "external_system": "XNAT",
        "external_id": "pietjepuk"
    })

    # user is not allowed to post an external_subject_link
    assert response.status_code == 403
    data = response.get_json()
    assert data != {'external_experiment_links': ['/api/v1/external_experiment_links/1']}


def test_experiment_links_post_as_worker(client, experiment_data):
    response = client.post('/api/v1/external_experiment_links', headers=localworker_auth, json={
        "experiment": "Experiment_1_1",
        "external_system": "XNAT",
        "external_id": "pietjepuk"
    })

    # user is not allowed to post an external_subject_link
    assert response.status_code == 403
    data = response.get_json()
    assert data != {'external_experiment_links': ['/api/v1/external_experiment_links/1']}
