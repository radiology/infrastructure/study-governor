# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header



def test_scans_get_as_admin(client, app_config):
    # Get scans list
    response = client.get('/api/v1/scans', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'scans': []}


def test_scans_get_as_superuser(client, app_config):
    # Get scans list
    response = client.get('/api/v1/scans', headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'scans': []}


def test_scans_get_as_user(client, app_config):
    # Get scans list
    response = client.get('/api/v1/scans', headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'scans': []}


def test_scans_get_as_worker(client, app_config):
    # Get scans list
    response = client.get('/api/v1/scans', headers=basic_auth_authorization_header("localworker", "localworker"))
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'scans': []}

def test_scans_post_as_admin(client, app_config, experiment_data):
    # Get scans list
    response = client.post('/api/v1/scans', headers=basic_auth_authorization_header("admin", "admin"), json={
        "experiment": '/api/v1/experiments/1',
        "scantype": {'modality': 'MR', 'protocol': 'T1'} 
    })
    assert response.status_code == 404

# Does not seem to work like this. Anyway, scans will not be used.