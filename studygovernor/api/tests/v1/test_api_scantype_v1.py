# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header


def test_scantypes_get(client, app_config, workflow_test_data):
    response = client.get('/api/v1/scantypes', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {"scantypes": [
        '/api/v1/scantypes/1',
        '/api/v1/scantypes/2',
    ]}
    

def test_scantype_get(client, app_config, workflow_test_data):
    response = client.get('/api/v1/scantypes/1', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'uri': '/api/v1/scantypes/1',
        'modality': 'MR',
        'protocol': 'T1',
    }


def test_scantype_get_nonexisting(client, app_config, workflow_test_data):
    response = client.get('/api/v1/scantypes/4', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404

