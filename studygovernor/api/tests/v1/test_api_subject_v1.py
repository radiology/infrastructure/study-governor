# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header
from studygovernor.tests.helpers import admin_auth
from studygovernor.tests.helpers import superuser_auth
from studygovernor.tests.helpers import user_1_auth
from studygovernor.tests.helpers import user_2_auth
from studygovernor.tests.helpers import localworker_auth


def test_subjects_get(client, app_config, subject_data):
    # Get subject list
    response = client.get('/api/v1/subjects', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {"subjects": ['/api/v1/subjects/1', '/api/v1/subjects/2']}


def test_subject_get(client, app_config, subject_links, experiment_data):
    # Get the subject, via ID
    response = client.get('/api/v1/subjects/1', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    print('hv1')
    assert data == {'uri': '/api/v1/subjects/1',
                    'api_uri': 'http://localhost/api/v1/subjects/1',
                    'web_uri': 'http://localhost/subjects/1',
                    'label': 'Test Subject_1',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2019-01-01',
                    'external_ids':  {'XNAT': 'External_SubjectID_1'},
                    'experiments': ['/api/v1/experiments/1', '/api/v1/experiments/2']}

    # Get the subject, via label
    response = client.get('/api/v1/subjects/Test Subject_2', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print('hv2')
    assert data == {'uri': '/api/v1/subjects/2',
                    'api_uri': 'http://localhost/api/v1/subjects/2',
                    'web_uri': 'http://localhost/subjects/2',
                    'label': 'Test Subject_2',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2019-01-02',
                    'external_ids':  {'IFDB': 'External_SubjectID_2', 'TASKMANAGER': 'External_SubjectID_2'},
                    'experiments': ['/api/v1/experiments/3',
                                    '/api/v1/experiments/4']}

    # Get the subject, explicitly via ID
    response = client.get('/api/v1/subjects/1?filter_field=id', headers=admin_auth)
    print('hv3')
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    print('hv4')
    assert data == {'uri': '/api/v1/subjects/1',
                    'api_uri': 'http://localhost/api/v1/subjects/1',
                    'web_uri': 'http://localhost/subjects/1',
                    'label': 'Test Subject_1',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2019-01-01',
                    'external_ids': {'XNAT': 'External_SubjectID_1'},
                    'experiments': ['/api/v1/experiments/1', '/api/v1/experiments/2']}

    # Get the subject, explicitly via label
    response = client.get('/api/v1/subjects/Test Subject_2?filter_field=label', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    print('hv5')
    assert data == {'uri': '/api/v1/subjects/2',
                    'api_uri': 'http://localhost/api/v1/subjects/2',
                    'web_uri': 'http://localhost/subjects/2',
                    'label': 'Test Subject_2',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2019-01-02',
                    'external_ids': {'IFDB': 'External_SubjectID_2', 'TASKMANAGER': 'External_SubjectID_2'},
                    'experiments': ['/api/v1/experiments/3', '/api/v1/experiments/4']}

    # Get the subject, explicitly via invalid (wrong type) ID
    response = client.get('/api/v1/subjects/test?filter_field=id', headers=admin_auth)
    assert response.status_code == 404

    # Get the subject, explicitly via non-existing ID
    response = client.get('/api/v1/subjects/12345?filter_field=id', headers=admin_auth)
    assert response.status_code == 404

    # Get the subject, explicitly non-existing label
    response = client.get('/api/v1/subjects/nonexisting?filter_field=label', headers=admin_auth)
    assert response.status_code == 404

    # Get the subject, explicitly non-existing label (but valid ID)
    response = client.get('/api/v1/subjects/2?filter_field=label', headers=admin_auth)
    assert response.status_code == 404

    # Get the subject, explicitly via non-existing ID (but valid label)
    response = client.get('/api/v1/subjects/Test Subject_1?filter_field=id', headers=admin_auth)
    assert response.status_code == 404

# TODO filtering doesnt seem to work. Because of space in label?


def test_subject_put(client, app_config, subject_links, experiment_data):
    # Update both fields
    response = client.put('/api/v1/subjects/1', headers=admin_auth, json={
        'label': 'test_subject_1_changed',
        'date_of_birth': '2001-10-31'
    })
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {'uri': '/api/v1/subjects/1',
                    'api_uri': 'http://localhost/api/v1/subjects/1',
                    'web_uri': 'http://localhost/subjects/1',
                    'label': 'test_subject_1_changed',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2001-10-31',
                    'external_ids': {'XNAT': 'External_SubjectID_1'},
                    'experiments': ['/api/v1/experiments/1', '/api/v1/experiments/2']}

    # Update label
    response = client.put('/api/v1/subjects/2', headers=admin_auth, json={
        'label': 'try_another_test_subject_2',
    })
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'uri': '/api/v1/subjects/2',
                    'api_uri': 'http://localhost/api/v1/subjects/2',
                    'web_uri': 'http://localhost/subjects/2',
                    'label': 'try_another_test_subject_2',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2019-01-02',
                    'external_ids': {'IFDB': 'External_SubjectID_2', 'TASKMANAGER': 'External_SubjectID_2'},
                    'experiments': ['/api/v1/experiments/3', '/api/v1/experiments/4']}

    # Update date of birth
    response = client.put('/api/v1/subjects/1', headers=admin_auth, json={
        'date_of_birth': '1959-10-30'
    })
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'uri': '/api/v1/subjects/1',
                    'api_uri': 'http://localhost/api/v1/subjects/1',
                    'web_uri': 'http://localhost/subjects/1',
                    'label': 'test_subject_1_changed',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '1959-10-30',
                    'external_ids': {'XNAT': 'External_SubjectID_1'},
                    'experiments': ['/api/v1/experiments/1', '/api/v1/experiments/2']}

    # Update both fields
    response = client.put('/api/v1/subjects/4', headers=admin_auth, json={
        'label': 'test_subject_4_changed',
        'date_of_birth': '2005-11-28'
    })

#    print('RESPONSE: [{}] {}'.format(response.status_code, response.data))
# TODO .... why is this print above not working .... gives wrong URL

    assert response.status_code == 404


def test_subject_post(client, cohort_data):
    # Add a subject with invalid JSON data
    response = client.post('/api/v1/subjects', headers=admin_auth, json={
        "label": "test_subject_1",
    })
    assert response.status_code == 400

    response = client.post('/api/v1/subjects', headers=admin_auth, json={
        "date_of_birth": "2000-11-30",
    })
    assert response.status_code == 400

    # Add a subject
    response = client.post('/api/v1/subjects', headers=admin_auth, json={
        "label": "test_subject_2",
        "date_of_birth": "2000-10-30",
        "cohort": "test_cohort"
    })
    assert response.status_code == 201

    data = response.get_json()
    print(data)
    assert data == {'uri': '/api/v1/subjects/1',
                    'api_uri': 'http://localhost/api/v1/subjects/1',
                    'web_uri': 'http://localhost/subjects/1',
                    'label': 'test_subject_2',
                    'cohort': '/api/v1/cohorts/1',
                    'date_of_birth': '2000-10-30',
                    'external_ids': {},
                    'experiments': []}

    # Get subject list again, now with data
    response = client.get('/api/v1/subjects', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    assert data == {"subjects": ['/api/v1/subjects/1']}
