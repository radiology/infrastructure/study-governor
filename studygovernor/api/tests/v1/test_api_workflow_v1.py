# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header


def test_workflows_get(client, workflow_test_data, second_workflow_test_data):
    response = client.get('/api/v1/workflows', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'workflows': ['/api/v1/workflows/1', '/api/v1/workflows/2']}


def test_get_workflow_nonexisting(client, workflow_test_data, second_workflow_test_data):
    # Get a non-existing state
    response = client.get('/api/v1/workflows/0', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_get_workflow_integer(client, workflow_test_data, second_workflow_test_data):
    # Get state by integer id
    response = client.get('/api/v1/workflows/2', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'uri': '/api/v1/workflows/2',
                    'label': 'second_workflow'}


def test_get_workflow_label(client, workflow_test_data, second_workflow_test_data):
    # Get state by string label
    response = client.get('/api/v1/workflows/test_workflow', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'uri': '/api/v1/workflows/1',
                    'label': 'test_workflow'}


