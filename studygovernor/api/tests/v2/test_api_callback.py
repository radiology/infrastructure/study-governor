# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime

import deepdiff
import isodate
import yaml
from pytest_mock import MockerFixture

from studygovernor.control import set_state, set_variable
from studygovernor.models import Experiment, State, CallbackExecution
from studygovernor.tests.helpers import basic_auth_authorization_header


def _test_experiments_get_callbacks(client, user, password):
    response = client.get('/api/v2/callbacks',
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()
    assert data == {'callbacks':
                        [{'id': 1, 'label': 'step2_callback1', 'uri': '/api/v2/callbacks/1'},
                         {'id': 2, 'label': 'step2_callback2', 'uri': '/api/v2/callbacks/2'}]}


def test_get_callbacks_as_admin(client, workflow_with_callbacks_test_data):
    _test_experiments_get_callbacks(client, "admin", "admin")


def test_get_callbacks_as_user(client, workflow_with_callbacks_test_data):
    _test_experiments_get_callbacks(client, "user1", "user1")


def test_get_callbacks_wrong_login(client, workflow_with_callbacks_test_data):
    response = client.get('/api/v2/callbacks',
                          headers=basic_auth_authorization_header('non-existing-user', "password"))

    assert response.status_code == 401
    data = response.get_json()

    assert data is None


def test_get_callback(client, workflow_with_callbacks_test_data):
    user, password = 'admin', 'admin'
    response = client.get('/api/v2/callbacks/1',
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()

    # Unpack JSON for easy comparison
    data['callback_arguments'] = yaml.safe_load(data.get('callback_arguments', ''))
    assert data == \
           {'uri': '/api/v2/callbacks/1',
            'state': {'id': 3, 'label': 'step2', 'uri': '/api/v2/states/3'},
            'label': 'step2_callback1',
            'executions': [],
            'function': 'test_callback',
            'callback_arguments': {'answer': 42, 'foo': 'bar'},
            'run_timeout': 30,
            'wait_timeout': 0,
            'initial_delay': 1,
            'description': 'Test callback that just sets some vars',
            'condition': "experiment.variables.get('test') == 42"}


def test_callback_execution_http_auth(client,
                                      experiment_data_with_callback,
                                      mocker: MockerFixture):
    user = 'admin'
    password = 'admin'

    # Disable callbacks
    mocker.patch('studygovernor.control.dispatch_callback')
    destination_state = State.query.filter(State.label == 'step2').one()

    # Set all experiments ready to state2
    experiment = Experiment.query.first()
    set_variable(experiment, 'test', 42)
    set_state(experiment, destination_state)

    response = client.get(f'/api/v2/experiments/{experiment.id}/actions',
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()

    assert data == {'actions':
                        [{'id': 1, 'success': True, 'return_value': 'No callback for state', 'uri': '/api/v2/actions/1'},
                         {'id': 5, 'success': False, 'return_value': None, 'uri': '/api/v2/actions/5'}]
                    }

    response = client.get(data['actions'][-1]['uri'],
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()
    
    # Fix start time for comparison
    data['start_time'] = '2022-06-22T17:40:08.153598'

    assert data == {
        'id': 5,
        'uri': '/api/v2/actions/5',
        'api_uri': 'http://localhost/api/v2/actions/5',
        'web_uri': 'http://localhost/actions/5',
        'experiment': {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
        'transition': {'id': 2, 'uri': '/api/v2/transitions/2'},
        'executions': [{'id': 1, 'uri': '/api/v2/callback_executions/1'},
                       {'id': 2, 'uri': '/api/v2/callback_executions/2'}],
        'success': False,
        'return_value': None,
        'freetext': 'Transition triggered by setting state to step2 (3)',
        'start_time': '2022-06-22T17:40:08.153598',
        'end_time': None
    }

    response = client.get('/api/v2/callback_executions/1',
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()
    # Fix the timestamp
    if isinstance(data.get('created'), str):
        data['created'] = '2022-06-22T16:21:00'

    difference = deepdiff.DeepDiff(data, 
        {
            'id': 1,
            'uri': '/api/v2/callback_executions/1',
            'api_uri': 'http://localhost/api/v2/callback_executions/1',
            'web_uri': 'http://localhost/actions/5',
            'callback': {
                'uri': '/api/v2/callbacks/1',
                'state': {'id': 3, 'label': 'step2', 'uri': '/api/v2/states/3'},
                'label': 'step2_callback1',
                'executions': [{'id': 1, 'uri': '/api/v2/callback_executions/1'}],
                'function': 'test_callback',
                'callback_arguments': 'answer: 42\nfoo: bar\n',
                'run_timeout': 30,
                'wait_timeout': 0,
                'initial_delay': 1,
                'description': 'Test callback that just sets some vars', 'condition': "experiment.variables.get('test') == 42"
            },
            'cohort': {
                'id': 1,
                'label': 'test_cohort',
                'uri': '/api/v2/cohorts/1',
                'description': 'Some test cohort',
                'external_urls': {},
                'subjects': [
                    {'id': 1, 'label': 'Test Subject_1', 'uri': '/api/v2/subjects/1'},
                    {'id': 2, 'label': 'Test Subject_2', 'uri': '/api/v2/subjects/2'}
                ]
            },
            'subject': {
                'id': 1,
                'label': 'Test Subject_1',
                'uri': '/api/v2/subjects/1',
                'api_uri': 'http://localhost/api/v2/subjects/1',
                'web_uri': 'http://localhost/subjects/1',
                'cohort': {'id': 1, 'label': 'test_cohort', 'uri': '/api/v2/cohorts/1'},
                'date_of_birth': '2019-01-01',
                'external_ids': {},
                'experiments': [
                    {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
                    {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'}]
            },
            'experiment': {
                'id': 1,
                'label': 'Experiment_1_1',
                'uri': '/api/v2/experiments/1',
                'api_uri': 'http://localhost/api/v2/experiments/1',
                'web_uri': 'http://localhost/experiments/1',
                'subject': {'id': 1, 'label': 'Test Subject_1', 'uri': '/api/v2/subjects/1'},
                'scandate': '2019-01-01T00:00:00',
                'state': {'id': 3, 'label': 'step2', 'uri': '/api/v2/states/3'},
                'external_ids': {},
                'variable_map': {'test': 42}
            },
            'action': {
                'id': 5,
                'uri': '/api/v2/actions/5',
                'api_uri': 'http://localhost/api/v2/actions/5',
                'web_uri': 'http://localhost/actions/5',
                'experiment': {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
                'transition': {'id': 2, 'uri': '/api/v2/transitions/2'},
                'executions': [
                    {'id': 1, 'uri': '/api/v2/callback_executions/1'},
                    {'id': 2, 'uri': '/api/v2/callback_executions/2'}
                ],
                'success': False,
                'return_value': None,
                'freetext': 'Transition triggered by setting state to step2 (3)',
                'start_time': '2023-02-06T17:55:05.330154',
                'end_time': None
            },
            'external_systems': {
                'XNAT': 'https://127.0.0.1',
                'IFDB': 'http://127.0.0.1:5002/v1/',
                'TASKMANAGER': 'http://127.0.0.1:5001'
            },
            'status': 'created',
            'result': 'none',
            'run_log': None,
            'result_log': None,
            'result_values': None,
            'created': '2022-06-22T16:21:00',
            'run_start': None,
            'wait_start': None,
            'finished': None
        }, 
    exclude_paths=[
        "root['action']['start_time']"
    ])

    assert difference == {}

    response = client.get('/api/v2/callback_executions/2',
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()
    # Fix the timestamp
    if isinstance(data.get('created'), str):
        data['created'] = '2022-06-22T16:22:00'

    difference = deepdiff.DeepDiff(data, {
        'id': 2,
        'created': '2022-06-22T16:22:00',
        'finished': None,
        'result': 'none',
        'result_log': None,
        'result_values': None,
        'run_log': None,
        'run_start': None,
        'status': 'skipped',
        'uri': '/api/v2/callback_executions/2',
        'api_uri': 'http://localhost/api/v2/callback_executions/2',
        'web_uri': 'http://localhost/actions/5',
        'wait_start': None
    }, exclude_paths=[
        "root['action']",
        "root['callback']",
        "root['experiment']",
        "root['subject']",
        "root['cohort']",
        "root['external_systems']",
    ])

    assert difference == {}

    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'running'},
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()

    assert data['status'] == 'running'
    run_start = isodate.parse_datetime(data['run_start'])
    assert isinstance(run_start, datetime.datetime)
    assert data['wait_start'] is None

    # User secret instead of login headers
    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'waiting',
                                'run_log': 'Run was successful'},
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()
    assert data['status'] == 'waiting'
    wait_start = isodate.parse_datetime(data['wait_start'])
    assert isinstance(wait_start, datetime.datetime)
    assert data['run_log'] == 'Run was successful'

    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'finished',
                                'result_log': 'All finished!',
                                'result_values': {
                                    'test': 1,
                                    'foo': 'bar',
                                    'list': [1, 2, 3],
                                }},
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 200

    data = response.get_json()
    assert data['status'] == 'finished'
    finished = isodate.parse_datetime(data['finished'])
    assert isinstance(finished, datetime.datetime)
    assert data['run_log'] == 'Run was successful'
    assert data['result_log'] == 'All finished!'
    assert yaml.safe_load(data['result_values']) == {
        'test': 1,
        'foo': 'bar',
        'list': [1, 2, 3],
    }

    # Check that a finished callback execution cannot be modified again
    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'waiting', 'run_log': 'Run finished'},
                          headers=basic_auth_authorization_header(user, password))

    assert response.status_code == 409


def test_callback_execution_secret(client,
                                   experiment_data_with_callback,
                                   mocker: MockerFixture):
    # Disable callbacks
    mocker.patch('studygovernor.control.dispatch_callback')
    destination_state = State.query.filter(State.label == 'step2').one()

    # Set all experiments ready to state2
    experiment = Experiment.query.first()
    set_variable(experiment, 'test', 42)
    set_state(experiment, destination_state)

    # Get correct secret keys from DB
    callback_execution_from_db1 = CallbackExecution.query.filter_by(id=1).one()
    secret_key1 = callback_execution_from_db1.secret_key

    callback_execution_from_db2 = CallbackExecution.query.filter_by(id=2).one()
    secret_key2 = callback_execution_from_db2.secret_key

    response = client.get('/api/v2/callback_executions/1',
                          json={"secret_key": secret_key1})

    assert response.status_code == 200

    data = response.get_json()
    # Fix the timestamp
    if isinstance(data.get('created'), str):
        data['created'] = '2022-06-22T16:21:00'

    difference = deepdiff.DeepDiff(data, {
        'id': 1,
        'action': '/api/v2/actions/5',
        'callback': '/api/v2/callbacks/1',
        'created': '2022-06-22T16:21:00',
        'finished': None,
        'result': 'none',
        'result_log': None,
        'result_values': None,
        'run_log': None,
        'run_start': None,
        'status': 'created',
        'uri': '/api/v2/callback_executions/1',
        'api_uri': 'http://localhost/api/v2/callback_executions/1',
        'web_uri': 'http://localhost/actions/5',
        'wait_start': None
    }, exclude_paths=[
        "root['action']",
        "root['callback']",
        "root['experiment']",
        "root['subject']",
        "root['cohort']",
        "root['external_systems']",
    ])

    assert difference == {}

    # Check invalid secret key
    response = client.get('/api/v2/callback_executions/2',
                          json={"secret_key": secret_key1})

    assert response.status_code == 403
    assert response.get_json() == {'message': 'Cannot access specified callback_execution'}

    response = client.get('/api/v2/callback_executions/2',
                          json={"secret_key": secret_key2})

    assert response.status_code == 200

    data = response.get_json()
    # Fix the timestamp
    if isinstance(data.get('created'), str):
        data['created'] = '2022-06-22T16:22:00'

    difference = deepdiff.DeepDiff(data, {
        'id': 2,
        'action': '/api/v2/actions/5',
        'callback': '/api/v2/callbacks/2',
        'created': '2022-06-22T16:22:00',
        'finished': None,
        'result': 'none',
        'result_log': None,
        'result_values': None,
        'run_log': None,
        'run_start': None,
        'status': 'skipped',
        'uri': '/api/v2/callback_executions/2',
        'api_uri': 'http://localhost/api/v2/callback_executions/2',
        'web_uri': 'http://localhost/actions/5',
        'wait_start': None
    }, exclude_paths=[
        "root['action']",
        "root['callback']",
        "root['experiment']",
        "root['subject']",
        "root['cohort']",
        "root['external_systems']",
    ])

    assert difference == {}

    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'running',
                                'secret_key': secret_key1})

    assert response.status_code == 200

    data = response.get_json()

    assert data['status'] == 'running'
    run_start = isodate.parse_datetime(data['run_start'])
    assert isinstance(run_start, datetime.datetime)
    assert data['wait_start'] is None

    # User secret instead of login headers
    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'waiting',
                                'secret_key': secret_key1,
                                'run_log': 'Run was successful'})

    assert response.status_code == 200

    data = response.get_json()
    assert data['status'] == 'waiting'
    wait_start = isodate.parse_datetime(data['wait_start'])
    assert isinstance(wait_start, datetime.datetime)
    assert data['run_log'] == 'Run was successful'

    # Test failing auth
    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'finished',
                                'result_log': 'All finished!',
                                'result_values': {
                                    'test': 1,
                                    'foo': 'bar',
                                    'list': [1, 2, 3],
                                }})

    assert response.status_code == 403

    # Check experiment state before finishing the callback execution
    assert experiment.state == destination_state

    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'finished',
                                'secret_key': secret_key1,
                                'result_log': 'All finished!',
                                'result_values': {
                                    'test': 1,
                                    'foo': 'bar',
                                    'list': [1, 2, 3],
                                }})

    assert response.status_code == 200

    # Check that the state got updated
    new_state = State.query.filter(State.label == 'step3b').one()
    assert experiment.state == new_state

    data = response.get_json()
    assert data['status'] == 'finished'
    finished = isodate.parse_datetime(data['finished'])
    assert isinstance(finished, datetime.datetime)
    assert data['run_log'] == 'Run was successful'
    assert data['result_log'] == 'All finished!'
    assert yaml.safe_load(data['result_values']) == {
        'test': 1,
        'foo': 'bar',
        'list': [1, 2, 3],
    }

    # Check that a finished callback execution cannot be modified again
    response = client.put('/api/v2/callback_executions/1',
                          json={'status': 'waiting',
                                'run_log': 'Run finished',
                                'secret_key': secret_key1})

    assert response.status_code == 409
