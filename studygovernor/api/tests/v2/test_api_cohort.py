# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header
from studygovernor.tests.helpers import admin_auth
from studygovernor.tests.helpers import superuser_auth
from studygovernor.tests.helpers import user_1_auth
from studygovernor.tests.helpers import user_2_auth
from studygovernor.tests.helpers import localworker_auth


def test_cohorts_get(client, app_config, cohort_data):
    # Get subject list
    response = client.get('/api/v2/cohorts', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {"cohorts": [{"id":1, "uri":"/api/v2/cohorts/1", "label":"test_cohort"}]}


def test_cohort_get(client, app_config, cohort_data):
    response = client.get('/api/v2/cohorts/1', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()

    assert data == {'id': 1,
                    'description': 'Some test cohort',
                    'external_urls': {},
                    'label': 'test_cohort',
                    'subjects': [],
                    'uri': '/api/v2/cohorts/1'}


def test_cohort_get_by_label(client, app_config, cohort_data):
    response = client.get('/api/v2/cohorts/by-label/test_cohort', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()

    assert data == {'id': 1,
                    'description': 'Some test cohort',
                    'external_urls': {},
                    'label': 'test_cohort',
                    'subjects': [],
                    'uri': '/api/v2/cohorts/1'}


def test_cohort_get_nonexisting(client, app_config, cohort_data):
    response = client.get('/api/v2/cohorts/100', headers=admin_auth)
    assert response.status_code == 404

    response = client.get('/api/v2/cohorts/by-label/nonexisting', headers=admin_auth)
    assert response.status_code == 404


def test_cohort_subjects(client, app_config, cohort_subject_data):
    response = client.get('/api/v2/cohorts/test_cohort/subjects', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'subjects': [{'id': 1, 'label': 'Test_Subject_1_cohort', 'uri': '/api/v2/subjects/1'},
                                 {'id': 2, 'label': 'Test_Subject_2_cohort', 'uri': '/api/v2/subjects/2'}]}


def test_cohort_subject(client, app_config, cohort_subject_data):
    response = client.get('/api/v2/cohorts/test_cohort/subjects/Test_Subject_1_cohort', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'id': 1,
                    'label': 'Test_Subject_1_cohort',
                    'uri': '/api/v2/subjects/1',
                    'api_uri': 'http://localhost/api/v2/subjects/1',
                    'web_uri': 'http://localhost/subjects/1',
                    'cohort': {'id': 1, 'uri': '/api/v2/cohorts/1', 'label': 'test_cohort'},
                    'date_of_birth': '2019-01-01',
                    'external_ids': {},
                    'experiments': [{'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
                                    {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'}]}


def test_cohort_subject_nonexisting(client, app_config, cohort_subject_data):
    response = client.get('/api/v2/cohorts/test_cohort/subjects/nonexisting', headers=admin_auth)
    assert response.status_code == 404


def test_get_cohort_subject_experiments(client, app_config, cohort_subject_data):
    response = client.get('/api/v2/cohorts/test_cohort/subjects/Test_Subject_1_cohort/experiments', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'experiments': [{'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
                                    {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'}]}


def test_get_cohort_subject_experiment(client, app_config, cohort_subject_data):
    response = client.get('/api/v2/cohorts/test_cohort/subjects/Test_Subject_1_cohort/experiments/Experiment_1_1', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'id': 1,
                    'label': 'Experiment_1_1',
                    'uri': '/api/v2/experiments/1',
                    'api_uri': 'http://localhost/api/v2/experiments/1',
                    'web_uri': 'http://localhost/experiments/1',
                    'subject': {'id': 1, 'label': 'Test_Subject_1_cohort', 'uri': '/api/v2/subjects/1'},
                    'scandate': '2019-01-01T00:00:00',
                    'state': {'id': 2, 'label': 'step1', 'uri': '/api/v2/states/2'},
                    'external_ids': {},
                    'variable_map': {}}


def test_cohort_post(client, app_config, cohort_data):
    response = client.post('/api/v2/cohorts', headers=admin_auth, json={
        "label": "cohort2",
        "description": "second test cohort"
    })
    assert response.status_code == 201

    data = response.get_json()
    assert data == {'id': 2,
                    'uri': '/api/v2/cohorts/2',
                    'label': 'cohort2',
                    'description': 'second test cohort',
                    'external_urls': {},
                    'subjects': []}

    # Get cohort list again, now with new data
    response = client.get('/api/v2/cohorts', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {"cohorts": [{"id":1, "uri":"/api/v2/cohorts/1", "label":"test_cohort"}, {"id":2, "uri":"/api/v2/cohorts/2", "label":"cohort2"}]}


## External_ids
def test_cohort_external_urls_get(client, cohort_links):
    response = client.get('/api/v2/cohorts/1', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    assert data == {
        'id': 1,
        'description': 'Some test cohort',
        'external_urls': {
            'IFDB': 'https://external-url/test_cohort',
            'XNAT': 'https://external-url/test_cohort'
        },
        'label': 'test_cohort',
        'subjects': [],
        'uri': '/api/v2/cohorts/1'
    }

    response = client.get('/api/v2/cohorts/1/external_urls', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    assert data == {
        'cohort_id': 1,
        "cohort_label": "test_cohort",
        'external_urls': {
            'IFDB': 'https://external-url/test_cohort',
            'XNAT': 'https://external-url/test_cohort'
        }
    }

    response = client.get('/api/v2/cohorts/2/external_urls', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    assert data == {
        'cohort_id': 2,
        "cohort_label": "second_cohort",
        'external_urls': {}
    }

    response = client.get('/api/v2/cohorts/100/external_urls', headers=admin_auth)
    assert response.status_code == 404


def test_cohort_external_urls_normal_post(client, cohort_links):
    data = {
        'external_urls': {
            'XNAT': 'https://new-xnat-url/test_cohort'
        }
    }

    # Normal scenario
    response = client.post('/api/v2/cohorts/1/external_urls', headers=admin_auth, json=data)
    assert response.status_code == 201
    data = response.get_json()
    assert data == {
        'cohort_id': 1,
        "cohort_label": "test_cohort",
        'external_urls': {
            'XNAT': 'https://new-xnat-url/test_cohort'
        }
    }

def test_cohort_external_urls_nonexisting_cohort_post(client, cohort_links):
    data = {
        'external_urls': {
            'XNAT': 'https://new-xnat-url/test_cohort'
        }
    }

    # Non-existing cohort
    response = client.post('/api/v2/cohorts/100/external_urls', headers=admin_auth, json=data)
    assert response.status_code == 404

def test_cohort_external_urls_nonexisting_system_post(client, cohort_links):
    nonexisting_system_data = {
        'external_urls': {
            'XNAT-NEW': 'https://new-xnat-url/test_cohort'
        }
    }

    # Non-existing external_system
    response = client.post('/api/v2/cohorts/1/external_urls', headers=admin_auth, json=nonexisting_system_data)
    assert response.status_code == 404
    
    # Check that external ids hasn't changed
    response = client.get('/api/v2/cohorts/1/external_urls', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    assert data == {
        'cohort_id': 1,
        "cohort_label": "test_cohort",
        'external_urls': {
            'IFDB': 'https://external-url/test_cohort',
            'XNAT': 'https://external-url/test_cohort'
        }
    }


def test_cohort_external_urls_wrong_format_post(client, cohort_links):
    incorrect_data = {
        'external_urls': [
            'https://new-xnat-url/test_cohort'
        ]
    }

    # Non-existing external_system
    response = client.post('/api/v2/cohorts/1/external_urls', headers=admin_auth, json=incorrect_data)
    assert response.status_code == 400
    
    # Check that external ids hasn't changed
    response = client.get('/api/v2/cohorts/1/external_urls', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    assert data == {
        'cohort_id': 1,
        "cohort_label": "test_cohort",
        'external_urls': {
            'IFDB': 'https://external-url/test_cohort',
            'XNAT': 'https://external-url/test_cohort'
        }
    }


def test_cohort_user_post(client, cohort_links):
    data = {
        'external_urls': {
            'XNAT': 'https://new-xnat-url/test_cohort'
        }
    }

    # Posting as user is forbidden
    response = client.post('/api/v2/cohorts/1/external_urls', headers=user_1_auth, json=data)
    assert response.status_code == 403

    # Check that external ids hasn't changed
    response = client.get('/api/v2/cohorts/1/external_urls', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    assert data == {
        'cohort_id': 1,
        "cohort_label": "test_cohort",
        'external_urls': {
            'IFDB': 'https://external-url/test_cohort',
            'XNAT': 'https://external-url/test_cohort'
        }
    }


def test_cohort_external_url_put(client, cohort_links):
    data = {
        "external_url": "https://new-xnat-url/test_cohort"
    }

    # Normal scenario
    response = client.put('/api/v2/cohorts/1/external_urls/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 201

    data = response.get_json()
    assert data == {
        'cohort_id': 1,
        "cohort_label": "test_cohort",
        'external_urls': {
            'IFDB': 'https://external-url/test_cohort',
            'XNAT': 'https://new-xnat-url/test_cohort'
        }
    }


def test_cohort_external_url_nonexisting_cohort_put(client, cohort_links):
    data = {
        "external_url": "https://new-xnat-url/test_cohort"
    }

    # Put a url for nonexisting cohort
    response = client.put('/api/v2/cohorts/100/external_urls/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 404


def test_cohort_external_url_nonexisting_system_put(client, cohort_links):
    data = {
        "external_url": "https://new-xnat-url/test_cohort"
    }

    # Put a url in a nonexisting system
    response = client.put('/api/v2/cohorts/1/external_urls/CASTOR', headers=admin_auth, json=data)
    assert response.status_code == 404


def test_cohort_external_url_wrong_format_put(client, cohort_links):
    data = {
        "url": "https://new-xnat-url/test_cohort"
    }

    # Putting data in wrong format
    response = client.put('/api/v2/cohorts/1/external_urls/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 400


def test_cohort_external_url_user_put(client, cohort_links):
    data = {
        "external_url": "https://new-xnat-url/test_cohort"
    }

    # Putting as user is forbidden
    response = client.put('/api/v2/cohorts/1/external_urls/XNAT', headers=user_1_auth, json=data)
    assert response.status_code == 403
