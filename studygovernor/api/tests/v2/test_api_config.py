# Copyright 2023 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pathlib

from studygovernor.tests.helpers import basic_auth_authorization_header, admin_auth, user_1_auth, superuser_auth, localworker_auth


def test_config_post(app, admin_config):
    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config'/ 'test_config.yaml'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.post('/api/v2/service/config/import-config', headers=admin_auth, json={'config_definition': config_definition})
    assert response.status_code == 201
    data = response.get_json()
    assert data.get('updated_at') is not None
    
    assert data['users'] ==  [
        {'id': 2, 'username': 'superuser', 'name': 'Super User', 'uri': '/api/v2/users/2'},
        {'id': 3, 'username': 'user1', 'name': 'User 1', 'uri': '/api/v2/users/3'},
        {'id': 4, 'username': 'user2', 'name': 'User 2', 'uri': '/api/v2/users/4'},
        {'id': 5, 'username': 'halfuser1', 'name': 'Half User 1', 'uri': '/api/v2/users/5'},
        {'id': 6, 'username': 'halfuser2', 'name': 'Half User 2', 'uri': '/api/v2/users/6'},
        {'id': 7, 'username': 'inactiveuser', 'name': 'Inactive User', 'uri': '/api/v2/users/7'},{'id': 8, 'username': 'localworker', 'name': 'localworker', 'uri': '/api/v2/users/8'}
    ]

    assert data['roles'] == [
        {'id': 2, 'name': 'worker', 'uri': '/api/v2/roles/2'},
        {'id': 3, 'name': 'superuser', 'uri': '/api/v2/roles/3'},
        {'id': 4, 'name': 'user', 'uri': '/api/v2/roles/4'}
    ]

    assert data['external_systems'] == [
        {'id': 1, 'uri': '/api/v2/external_systems/1', 'url': 'https://127.0.0.1', 'system_name': 'XNAT'},
        {'id': 2, 'uri': '/api/v2/external_systems/2', 'url': 'http://127.0.0.1:5002/v1/', 'system_name': 'IFDB'},
        {'id': 3, 'uri': '/api/v2/external_systems/3', 'url': 'http://127.0.0.1:5001', 'system_name': 'TASKMANAGER'}
    ]

    assert data['scantypes'] == [
        {'id': 1, 'uri': '/api/v2/scantypes/1', 'modality': 'MR', 'protocol': 'T1'},
        {'id': 2, 'uri': '/api/v2/scantypes/2', 'modality': 'MR', 'protocol': 'PD'}
    ]


def test_forbidden_config_post(app, app_config):
    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config'/ 'test_config.yaml'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.post('/api/v2/service/config/import-config', headers=user_1_auth, json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.post('/api/v2/service/config/import-config', headers=superuser_auth, json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.post('/api/v2/service/config/import-config', headers=localworker_auth, json={'config_definition': config_definition})
    assert response.status_code == 403


def test_config_put(app, app_config):
    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config'/ 'test_put_config.yaml'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.put('/api/v2/service/config/import-config', headers=user_1_auth, json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.put('/api/v2/service/config/import-config', headers=superuser_auth, json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.put('/api/v2/service/config/import-config', headers=localworker_auth, json={'config_definition': config_definition})
    assert response.status_code == 403
    
    response = client.put('/api/v2/service/config/import-config', headers=admin_auth, json={'config_definition': config_definition})
    assert response.status_code == 200

    data = response.get_json()
    assert data.get('updated_at') is not None
    
    # Only updated objects have been changed
    assert data['users'] ==  [
        {'id': 4, 'username': 'user2', 'name': 'New User 2', 'uri': '/api/v2/users/4'},
    ]
    assert data['roles'] == [{'id': 4, 'name': 'user', 'uri': '/api/v2/roles/4'}]
    assert data['external_systems'] == [
        {'id': 1, 'uri': '/api/v2/external_systems/1', 'url': 'https://127.1.2.1', 'system_name': 'XNAT'},
        {'id': 4, 'uri': '/api/v2/external_systems/4', 'url': 'http://orthanc:5001', 'system_name': 'PACS'}
    ]
    assert data['scantypes'] == [
        {'id': 1, 'uri': '/api/v2/scantypes/1', 'modality': 'CT', 'protocol': 'T1'},
        {'id': 3, 'uri': '/api/v2/scantypes/3', 'modality': 'Mammo', 'protocol': 'MG'}
    ]

    roles_json = client.get('/api/v2/roles', headers=admin_auth).get_json()
    assert roles_json['roles'] == [
        {'id': 1, 'name': 'admin', 'uri': '/api/v2/roles/1'},
        {'id': 2, 'name': 'worker', 'uri': '/api/v2/roles/2'},
        {'id': 3, 'name': 'superuser', 'uri': '/api/v2/roles/3'},
        {'id': 4, 'name': 'user', 'uri': '/api/v2/roles/4'}
    ]

    external_systems_json = client.get('/api/v2/external_systems', headers=admin_auth).get_json()
    assert external_systems_json['external_systems'] == [
        {'id': 1, 'uri': '/api/v2/external_systems/1', 'url': 'https://127.1.2.1', 'system_name': 'XNAT'},
        {'id': 2, 'uri': '/api/v2/external_systems/2', 'url': 'http://127.0.0.1:5002/v1/', 'system_name': 'IFDB'},
        {'id': 3, 'uri': '/api/v2/external_systems/3', 'url': 'http://127.0.0.1:5001', 'system_name': 'TASKMANAGER'},
        {'id': 4, 'uri': '/api/v2/external_systems/4', 'url': 'http://orthanc:5001', 'system_name': 'PACS'}
    ]

    scantypes_json = client.get('/api/v2/scantypes', headers=admin_auth).get_json()
    assert scantypes_json['scantypes'] == [
        {'id': 1, 'uri': '/api/v2/scantypes/1', 'modality': 'CT', 'protocol': 'T1'},
        {'id': 2, 'uri': '/api/v2/scantypes/2', 'modality': 'MR', 'protocol': 'PD'},
        {'id': 3, 'uri': '/api/v2/scantypes/3', 'modality': 'Mammo', 'protocol': 'MG'}
    ]
