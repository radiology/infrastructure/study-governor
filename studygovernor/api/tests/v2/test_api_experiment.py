# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header, admin_auth, user_1_auth


def _test_experiments_get(client, user, password):
    # Get experiment list
    response = client.get('/api/v2/experiments',
                          headers=basic_auth_authorization_header(user, password))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'experiments': [
        {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
        {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'},
        {'id': 3, 'label': 'Experiment_2_1', 'uri': '/api/v2/experiments/3'},
        {'id': 4, 'label': 'Experiment_2_2', 'uri': '/api/v2/experiments/4'}]}

    response = client.get('/api/v2/experiments/2',
                          headers=basic_auth_authorization_header(user, password))

    data = response.get_json()
    assert data == {
        'id': 2,
        'label': 'Experiment_1_2',
        'uri': '/api/v2/experiments/2',
        'api_uri': 'http://localhost/api/v2/experiments/2',
        'web_uri': 'http://localhost/experiments/2',
        'subject': {'id': 1, 'label': 'Test Subject_1', 'uri': '/api/v2/subjects/1'},
        'scandate': '2019-01-02T00:00:00',
        'state': {'id': 2, 'label': 'step1', 'uri': '/api/v2/states/2'},
        'external_ids': {},
        'variable_map': {}}


def test_experiments_get_as_admin(client, experiment_data):
    _test_experiments_get(client, "admin", "admin")


def test_experiments_get_as_superuser(client, experiment_data):
    _test_experiments_get(client, "superuser", "superuser")


def test_experiments_get_as_user(client, experiment_data):
    _test_experiments_get(client, "user1", "user1")


def test_experiments_get_as_worker(client, experiment_data):
    _test_experiments_get(client, "localworker", "localworker")


def test_experiment_get_nonexisting(client, experiment_data):
    # Get non-existing experiment
    response = client.get('/api/v2/experiments/100', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_experiment_post_as_admin(client, workflow_test_data, subject_data):
    # Post a valid experiment
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_1",
        "subject_id": 1,
        "scandate": "2001-05-28T09:30:45.123456"
    })
    assert response.status_code == 201

    # Post an invalid experiment (subject reference doesn't exist)
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_7",
        "subject_id": 100,
        "scandate": "2001-05-28T09:30:45.123456"
    })
    assert response.status_code == 404

    # Post a valid experiment
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_2",
        "subject_id": 2,
        "scandate": "2001-05-28T09:30:45.123456"
    })
    assert response.status_code == 201


def test_experiment_post_as_superuser(client, workflow_test_data, subject_data):
    # Post a valid experiment as superuser
    data = {
        "label": "Experiment_2_99",
        "subject_id": 2,
        "scandate": "2001-05-28T09:30:45.123456"
    }
    response = client.post('/api/v2/experiments',
                           headers=basic_auth_authorization_header("superuser", "superuser"),
                           json=data)
    assert response.status_code == 201


def test_experiment_post_as_user(client, workflow_test_data, subject_data):
    # Post a valid experiment as user is not allowed
    data = {
        "label": "test_experiment_1",
        "subject_id": 2,
        "scandate": "2001-05-28T09:30:45.123456"
    }
    response = client.post('/api/v2/experiments',
                           headers=basic_auth_authorization_header("user1", "user1"),
                           json=data)
    assert response.status_code == 403


def test_experiment_post_as_worker(client, workflow_test_data, subject_data):
    # Post a valid experiment as worker is not allowed
    data = {
        "label": "test_experiment_1",
        "subject_id": 2,
        "scandate": "2001-05-28T09:30:45.123456"
    }
    response = client.post('/api/v2/experiments',
                           headers=basic_auth_authorization_header("localworker", "localworker"),
                           json=data)
    assert response.status_code == 403


def test_experiment_post_missing_fields(client, workflow_test_data, subject_data):
    # Post an invalid experiment (missing fields)
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_0",
    })
    assert response.status_code == 400


def test_experiment_post_invalid_date(client, workflow_test_data, subject_data):
    # Post with invalid date format
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_0",
        "subject_id": 1,
        "scandate": "2000/25/05"
    })
    assert response.status_code == 400


def test_experiment_post_nonexisting_subject(client, workflow_test_data, subject_data):
    # Post an experiment relating to a non-existing subject
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_10",
        "subject_id": 100,
        "scandate": "2000-05-28T09:30:45.123456"
    })
    assert response.status_code == 404

#    data = response.get_json()
#    assert data == {'uri': '/api/v2/experiments/1',
#                    'subject': '/api/v2/subjects/1',
#                    'label': 'test_experiment_1',
#                    'scandate': '2001-05-28T09:30:45.123456',
#                    'state': '/api/v2/experiments/1/state',
#                    'external_ids': {}}
#    assert response.status_code == 201

#    data = response.get_json()
#    assert data == {'uri': '/api/v2/experiments/2',
#                    'subject': '/api/v2/subjects/1',
#                    'label': 'test_experiment_2',
#                    'scandate': '2003-07-22T11:38:25.512456',
#                    'state': '/api/v2/experiments/2/state',
#                    'external_ids': {}}
# TODO .... don't understand this last part yet, copied initially from existing overall testfile


def test_experiments_get_relist(client, experiment_data):
    # Re-list experiments
    response = client.get('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'experiments': [
            {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
            {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'},
            {'id': 3, 'label': 'Experiment_2_1', 'uri': '/api/v2/experiments/3'},
            {'id': 4, 'label': 'Experiment_2_2', 'uri': '/api/v2/experiments/4'}
        ]
    }

    # Get the experiment
    response = client.get('/api/v2/experiments/1', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 1,
        'uri': '/api/v2/experiments/1',
        'api_uri': 'http://localhost/api/v2/experiments/1',
        'web_uri': 'http://localhost/experiments/1',
        'subject': {
            'id': 1,
            'label': 'Test Subject_1',
            'uri': '/api/v2/subjects/1'
        },
        'label': 'Experiment_1_1',
        'scandate': '2019-01-01T00:00:00',
        'state': {
            'id': 2,
            'label': 'step1',
            'uri': '/api/v2/states/2'
        },
        'external_ids': {},
        'variable_map': {},
    }


def test_experiment_get_state(client, experiment_data):
    # Get experiment data
    response = client.get('/api/v2/experiments/1/state', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'state': {'id': 2, 'label': 'step1', 'uri': '/api/v2/states/2'}}

    # Put the state to non-exiting state
    non_existing_data = {'state_label': 'non-existing-state'}
    response = client.put('/api/v2/experiments/1/state',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json=non_existing_data)
    assert response.status_code == 404

    data = response.get_json()
    assert data == {
        'state': {
            'id': 2, 
            'label': 'step1', 
            'uri': '/api/v2/states/2'
        },
        'error': {
            'errorclass': 'StateNotFoundError',
            'requested_state': 'non-existing-state',
            'message': 'Could not find requested state "non-existing-state"'
        },
        'success': False
    }

    # Put the state to invalid target (no transition)
    response = client.put('/api/v2/experiments/1/state',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json={'state_label': 'step3'})
    assert response.status_code == 409

    data = response.get_json()
    assert data == {
        'state': {
            'id': 2, 
            'label': 'step1', 
            'uri': '/api/v2/states/2'
        },
        'error': {
            'errorclass': 'NoValidTransitionError',
            'sourcestate': '/api/v2/states/2',
            'targetstate': '/api/v2/states/5',
            'message': 'Could not find a valid transition for requested state change (from step1 [2] to step3 [5])'
        },
        'success': False
    }

    # Put the state to a new valid state
    response = client.put('/api/v2/experiments/1/state',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json={'state_label': 'step2a'})
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'state': {
            'id': 3,
            'label': 'step2a',
            'uri': '/api/v2/states/3'
        },
        'error': None,
        'success': True
    }

    # Get experiment state again
    response = client.get('/api/v2/experiments/1/state', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'state': {
            'id': 3,
            'label': 'step2a',
            'uri': '/api/v2/states/3'
        }
    }


def test_experiment_post_workflow(client, app_config, experiment_data, second_workflow_test_data):
    # Post a valid experiment, in test_workflow
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_wf1",
        "subject_id": 1,
        "scandate": "2001-05-28T09:30:45.123456",
        "workflow_label": "test_workflow"
    })
    assert response.status_code == 201

    new_experiment = response.get_json()
    response = client.get(new_experiment['state']['uri'], headers=basic_auth_authorization_header("admin", "admin"))
    assert response.get_json() == {
        'id': 2,
        'label': 'step1',
        'uri': '/api/v2/states/2',
        'callbacks': [],
        'freetext': 'Second state',
        'workflow': {
            'id': 1,
            'label': 'test_workflow',
            'uri': '/api/v2/workflows/1'
        },
        'experiments': [
            {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
            {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'},
            {'id': 3, 'label': 'Experiment_2_1', 'uri': '/api/v2/experiments/3'},
            {'id': 4, 'label': 'Experiment_2_2', 'uri': '/api/v2/experiments/4'},
            {'id': 5, 'label': 'test_experiment_wf1', 'uri': '/api/v2/experiments/5'}
        ]
    }

    # Post a valid experiment in second_workflow
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_wf2",
        "subject_id": 1,
        "scandate": "2001-05-28T09:31:45.123456",
        "workflow_label": "second_workflow"
    })
    assert response.status_code == 201

    new_experiment = response.get_json()
    
    response = client.get(new_experiment['state']['uri'], headers=basic_auth_authorization_header("admin", "admin"))
    assert response.get_json() == {
        'id': 8,
        'label': 'step1',
        'uri': '/api/v2/states/8',
        'callbacks': [],
        'freetext': 'Second state',
        'workflow': {
            'id': 2,
            'label': 'second_workflow',
            'uri': '/api/v2/workflows/2'
        },
        'experiments': [
            {
                'id': 6,
                'label': 'test_experiment_wf2',
                'uri': '/api/v2/experiments/6'
            }
        ]
    }

    # Post a valid experiment without workflow specification
    response = client.post('/api/v2/experiments', headers=basic_auth_authorization_header("admin", "admin"), json={
        "label": "test_experiment_wf3",
        "subject_id": 1,
        "scandate": "2001-05-28T09:32:45.123456"
    })
    assert response.status_code == 201

    new_experiment = response.get_json()
    response = client.get(new_experiment['state']['uri'], headers=basic_auth_authorization_header("admin", "admin"))
    assert response.get_json() == {
        'id': 8,
        'label': 'step1',
        'uri': '/api/v2/states/8',
        'callbacks': [],
        'freetext': 'Second state',
        'workflow': {
            'id': 2,
            'label': 'second_workflow',
            'uri': '/api/v2/workflows/2'
        },
        'experiments': [
            {'id': 6, 'label': 'test_experiment_wf2', 'uri': '/api/v2/experiments/6'},
            {'id': 7, 'label': 'test_experiment_wf3', 'uri': '/api/v2/experiments/7'}
        ]
    }


## External_ids
def test_experiment_external_ids_get(client, experiment_links):
    response = client.get('/api/v2/experiments/1', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    print(data)
    assert data == {
        'id': 1,
        'label': 'Experiment_1_1',
        'uri': '/api/v2/experiments/1',
        'api_uri': 'http://localhost/api/v2/experiments/1',
        'web_uri': 'http://localhost/experiments/1',
        'subject': {'id': 1, 'label': 'Test Subject_1', 'uri': '/api/v2/subjects/1'},
        'scandate': '2019-01-01T00:00:00',
        'state': {'id': 2, 'label': 'step1', 'uri': '/api/v2/states/2'},
        'external_ids': {'XNAT': 'External_ExperimentID_1'},
        'variable_map': {}
    }

    response = client.get('/api/v2/experiments/1/external_ids', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json()
    print(data) 
    assert data == {
        'experiment_id': 1,
        'experiment_label': 'Experiment_1_1',
        'external_ids': {'XNAT': 'External_ExperimentID_1'}
    }


    response = client.get('/api/v2/experiments/2/external_ids', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    print(data)
    assert data == {
        'experiment_id': 2,
        'experiment_label': 'Experiment_1_2',
        'external_ids': {
            'IFDB': 'External_ExperimentID_2',
            'TASKMANAGER': 'External_ExperimentID_2'
        }
    }

    response = client.get('/api/v2/experiments/100/external_ids', headers=admin_auth)
    assert response.status_code == 404


def test_experiment_external_ids_normal_post(client, experiment_links):
    data = {
        'external_ids': {
            'TASKMANAGER': 'new-external-id',
            'XNAT': 'new-xnat-id'
        }
    }

    # Normal scenario
    response = client.post('/api/v2/experiments/1/external_ids', headers=admin_auth, json=data)
    assert response.status_code == 201
    data = response.get_json()
    print(data)
    assert data == {
        'experiment_id': 1,
        'experiment_label': 'Experiment_1_1',
        'external_ids': {
            'XNAT': 'new-xnat-id',
            'TASKMANAGER': 'new-external-id'
        }
    }

def test_experiment_external_ids_nonexisting_experiment_post(client, experiment_links):
    data = {
        'external_ids': {
            'XNAT': 'new-xnat-id'
        }
    }

    # Non-existing experiment
    response = client.post('/api/v2/experiments/100/external_ids', headers=admin_auth, json=data)
    assert response.status_code == 404

def test_experiment_external_ids_nonexisting_system_post(client, experiment_links):
    nonexisting_system_data = {
        'external_ids': {
            'XNAT-NEW': 'new-xnat-id'
        }
    }

    # Non-existing external_system
    response = client.post('/api/v2/experiments/1/external_ids', headers=admin_auth, json=nonexisting_system_data)
    assert response.status_code == 404
    
    # Check that external ids haven't changed
    response = client.get('/api/v2/experiments/1/external_ids', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    print(data)
    assert data == {
        'experiment_id': 1,
        'experiment_label': 'Experiment_1_1',
        'external_ids': {'XNAT': 'External_ExperimentID_1'}
    }


def test_experiment_external_ids_wrong_format_post(client, experiment_links):
    incorrect_data = {
        'external_ids': [
            'https://new-xnat-url/test_cohort'
        ]
    }

    # Incorrect data format
    response = client.post('/api/v2/experiments/1/external_ids', headers=admin_auth, json=incorrect_data)
    assert response.status_code == 400
    
    # Check that external ids haven't changed
    response = client.get('/api/v2/experiments/1/external_ids', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    print(data)
    assert data == {
        'experiment_id': 1,
        'experiment_label': 'Experiment_1_1',
        'external_ids': {'XNAT': 'External_ExperimentID_1'}
    }


def test_experiment_user_post(client, experiment_links):
    data = {
        'external_ids': {
            'XNAT': 'new-xnat-id'
        }
    }

    # Posting as user is forbidden
    response = client.post('/api/v2/experiments/1/external_ids', headers=user_1_auth, json=data)
    assert response.status_code == 403



def test_experiment_external_id_put(client, experiment_links):
    data = {
        "external_id": "new-xnat-id"
    }

    # Normal scenario
    response = client.put('/api/v2/experiments/1/external_ids/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 201

    data = response.get_json()
    print(data)
    assert data == {
        'experiment_id': 1,
        'experiment_label': 'Experiment_1_1',
        'external_ids': {'XNAT': 'new-xnat-id'}
    }


def test_experiment_external_id_nonexisting_experiment_put(client, experiment_links):
    data = {
        "external_id": "new-xnat-id"
    }

    # Put a new id for nonexisting experiment
    response = client.put('/api/v2/experiments/100/external_ids/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 404


def test_experiment_external_id_nonexisting_system_put(client, experiment_links):
    data = {
        "external_id": "new-castor-id"
    }

    # Put an id in a nonexisting system
    response = client.put('/api/v2/experiments/1/external_ids/CASTOR', headers=admin_auth, json=data)
    assert response.status_code == 404


def test_experiment_external_id_wrong_format_put(client, experiment_links):
    data = {
        "url": "https://new-xnat-url/"
    }

    # Putting data in wrong format
    response = client.put('/api/v2/experiments/1/external_ids/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 400


def test_cohort_external_url_user_put(client, experiment_links):
    data = {
        "external_id": "new-xnat-id"
    }

    # Putting as user is forbidden
    response = client.put('/api/v2/experiments/1/external_ids/XNAT', headers=user_1_auth, json=data)
    assert response.status_code == 403
