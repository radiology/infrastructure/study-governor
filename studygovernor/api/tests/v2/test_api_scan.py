# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header

expected_response = {
        'scans': [
            {
                'id': 1,
                'uri': '/api/v2/scans/1',
                'experiment': {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'}, 'scantype': {
                    'id': 3,
                    'uri': '/api/v2/scantypes/3',
                    'modality': 'CT',
                    'protocol': 'Head'
                }
            }, 
            {
                'id': 2, 
                'uri': '/api/v2/scans/2',
                'experiment': {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'}, 'scantype': {
                    'id': 4,
                    'uri': '/api/v2/scantypes/4',
                    'modality': 'MR',
                    'protocol': 'FLAIR'
                }
            }
        ]
    }


def test_scans_get_as_admin(client, scan_data):
    # Get scans list
    response = client.get('/api/v2/scans', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == expected_response


def test_scans_get_as_superuser(client, scan_data):
    # Get scans list
    response = client.get('/api/v2/scans', headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == expected_response


def test_scans_get_as_user(client, scan_data):
    # Get scans list
    response = client.get('/api/v2/scans', headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == expected_response


def test_scans_get_as_worker(client, scan_data):
    # Get scans list
    response = client.get('/api/v2/scans', headers=basic_auth_authorization_header("localworker", "localworker"))
    assert response.status_code == 200

    data = response.get_json()
    
    assert data == expected_response

def test_scans_post_as_admin(client, app_config, experiment_data):
    # Get scans list
    response = client.post('/api/v2/scans', headers=basic_auth_authorization_header("admin", "admin"), json={
        'experiment_id': 1,
        'scantype_protocol': 'T1'
    })
    assert response.status_code == 201

    data = response.get_json()
    
    
    assert data == {
        'id': 1,
        'uri': '/api/v2/scans/1',
        'experiment': {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'}, 'scantype': {'id': 1, 'uri': '/api/v2/scantypes/1', 'modality': 'MR', 'protocol': 'T1'}
    }

# Does not seem to work like this. Anyway, scans will not be used.