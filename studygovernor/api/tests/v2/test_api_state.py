# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header


def test_states_get(client, workflow_test_data):
    response = client.get('/api/v2/states', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    
    assert data == {
        'states': [
            {'id': 1, 'label': 'untracked', 'uri': '/api/v2/states/1'},
            {'id': 2, 'label': 'step1', 'uri': '/api/v2/states/2'},
            {'id': 3, 'label': 'step2a', 'uri': '/api/v2/states/3'},
            {'id': 4, 'label': 'step2b', 'uri': '/api/v2/states/4'},
            {'id': 5, 'label': 'step3', 'uri': '/api/v2/states/5'},
            {'id': 6, 'label': 'finished', 'uri': '/api/v2/states/6'}
            ]
        }

def test_get_state_nonexisting(client, workflow_test_data):
    # Get a non-existing state
    response = client.get('/api/v2/states/0', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_get_state_by_integer(client, workflow_test_data):
    # Get state by integer id
    response = client.get('/api/v2/states/3', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    
    assert data == {
        'id': 3, 
        'label': 'step2a', 
        'uri': '/api/v2/states/3', 
        'callbacks': [], 
        'freetext': '', 
        'workflow': {
            'id': 1, 
            'label': 'test_workflow',
            'uri': '/api/v2/workflows/1'
        }, 
        'experiments': []
    }



def test_get_state_label(client, workflow_test_data):
    # Get state by string label
    response = client.get('/api/v2/states/step1', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_get_state_experiments(client, experiment_data):

    response = client.get('/api/v2/states/2/experiments', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    
    assert data == {
        'experiments': [
            {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
            {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'},
            {'id': 3, 'label': 'Experiment_2_1', 'uri': '/api/v2/experiments/3'},
            {'id': 4, 'label': 'Experiment_2_2', 'uri': '/api/v2/experiments/4'}
            ]
        }

    response = client.get('/api/v2/states/100/experiments', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404
