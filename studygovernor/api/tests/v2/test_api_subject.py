# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header
from studygovernor.tests.helpers import admin_auth
from studygovernor.tests.helpers import superuser_auth
from studygovernor.tests.helpers import user_1_auth
from studygovernor.tests.helpers import user_2_auth
from studygovernor.tests.helpers import localworker_auth


def test_subjects_get(client, app_config, subject_data):
    # Get subject list
    response = client.get('/api/v2/subjects', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'subjects': [
            {'id': 1, 'label': 'Test Subject_1', 'uri': '/api/v2/subjects/1'}, 
            {'id': 2, 'label': 'Test Subject_2', 'uri': '/api/v2/subjects/2'}
        ]
    }


def test_subject_get(client, app_config, subject_links, experiment_data):
    # Get the subject, via ID
    response = client.get('/api/v2/subjects/1', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 1, 
        'label': 'Test Subject_1',
        'uri': '/api/v2/subjects/1',
        'api_uri': 'http://localhost/api/v2/subjects/1',
        'web_uri': 'http://localhost/subjects/1',
        'cohort': {'id': 1, 'label': 'test_cohort', 'uri': '/api/v2/cohorts/1'},
        'date_of_birth': '2019-01-01',
        'external_ids': {'XNAT': 'External_SubjectID_1'},
        'experiments': [
            {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
            {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'}
        ]
    }


def test_subject_put(client, app_config, subject_links, experiment_data):
    # Update both fields
    response = client.put('/api/v2/subjects/1', headers=admin_auth, json={
        'label': 'test_subject_1_changed',
        'date_of_birth': '2001-10-31'
    })
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 1,
        'label': 'test_subject_1_changed',
        'uri': '/api/v2/subjects/1',
        'api_uri': 'http://localhost/api/v2/subjects/1',
        'web_uri': 'http://localhost/subjects/1',
        'cohort': {'id': 1, 'label': 'test_cohort', 'uri': '/api/v2/cohorts/1'},
        'date_of_birth': '2001-10-31',
        'external_ids': {'XNAT': 'External_SubjectID_1'},
        'experiments': [
            {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
            {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'}
        ]
    }

    # Update label
    response = client.put('/api/v2/subjects/2', headers=admin_auth, json={
        'label': 'try_another_test_subject_2',
    })
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 2,
        'label': 'try_another_test_subject_2',
        'uri': '/api/v2/subjects/2',
        'api_uri': 'http://localhost/api/v2/subjects/2',
        'web_uri': 'http://localhost/subjects/2',
        'cohort': {'id': 1, 'label': 'test_cohort', 'uri': '/api/v2/cohorts/1'},
        'date_of_birth': '2019-01-02',
        'external_ids': {'IFDB': 'External_SubjectID_2', 'TASKMANAGER': 'External_SubjectID_2'},
        'experiments': [
            {'id': 3, 'label': 'Experiment_2_1', 'uri': '/api/v2/experiments/3'},
            {'id': 4, 'label': 'Experiment_2_2', 'uri': '/api/v2/experiments/4'}
        ]
    }

    # Update date of birth
    response = client.put('/api/v2/subjects/1', headers=admin_auth, json={
        'date_of_birth': '1959-10-30'
    })
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 1,
        'label': 'test_subject_1_changed',
        'uri': '/api/v2/subjects/1',
        'api_uri': 'http://localhost/api/v2/subjects/1',
        'web_uri': 'http://localhost/subjects/1',
        'cohort': {'id': 1, 'label': 'test_cohort', 'uri': '/api/v2/cohorts/1'},
        'date_of_birth': '1959-10-30',
        'external_ids': {'XNAT': 'External_SubjectID_1'},
        'experiments': [
            {'id': 1, 'label': 'Experiment_1_1', 'uri': '/api/v2/experiments/1'},
            {'id': 2, 'label': 'Experiment_1_2', 'uri': '/api/v2/experiments/2'}
        ]
    }

    # Update non-existing subject
    response = client.put('/api/v2/subjects/100', headers=admin_auth, json={
        'label': 'test_subject_4_changed',
        'date_of_birth': '2005-11-28'
    })
    assert response.status_code == 404


def test_subject_post(client, cohort_data):
    # Add a subject with invalid JSON data
    response = client.post('/api/v2/subjects', headers=admin_auth, json={
        "label": "test_subject_1",
    })
    assert response.status_code == 400

    response = client.post('/api/v2/subjects', headers=admin_auth, json={
        "date_of_birth": "2000-11-30",
    })
    assert response.status_code == 400

    # Add a subject
    response = client.post('/api/v2/subjects', headers=admin_auth, json={
        "cohort_label": "test_cohort",
        "label": "test_subject_2",
        "date_of_birth": "2000-10-30"
    })
    assert response.status_code == 201

    data = response.get_json()
    assert data == {
        'id': 1,
        'label': 'test_subject_2',
        'uri': '/api/v2/subjects/1',
        'api_uri': 'http://localhost/api/v2/subjects/1',
        'web_uri': 'http://localhost/subjects/1',
        'cohort': {'id': 1, 'label': 'test_cohort', 'uri': '/api/v2/cohorts/1'},
        'date_of_birth': '2000-10-30',
        'external_ids': {},
        'experiments': []
    }

    # Get subject list again, now with data
    response = client.get('/api/v2/subjects', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'subjects': [{'id': 1, 'label': 'test_subject_2', 'uri': '/api/v2/subjects/1'}]}


## External_ids
def test_subject_external_ids_get(client, subject_links):
    response = client.get('/api/v2/subjects/1', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    print(data)
    assert data == {
        'id': 1,
        'label': 'Test Subject_1',
        'uri': '/api/v2/subjects/1',
        'api_uri': 'http://localhost/api/v2/subjects/1',
        'web_uri': 'http://localhost/subjects/1',
        'cohort': {
            'id': 1,
            'uri': '/api/v2/cohorts/1',
            'label': 'test_cohort'
        },
        'date_of_birth': '2019-01-01',
        'external_ids': {'XNAT': 'External_SubjectID_1'},
        'experiments': []
    }


    response = client.get('/api/v2/subjects/1/external_ids', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json()
    print(data) 
    assert data == {
        'subject_id': 1,
        'subject_label': 'Test Subject_1',
        'external_ids': {'XNAT': 'External_SubjectID_1'}
    }

    response = client.get('/api/v2/subjects/2/external_ids', headers=admin_auth)
    assert response.status_code == 200
    data = response.get_json() 
    print(data)
    assert data == {
        'subject_id': 2,
        'subject_label': 'Test Subject_2',
        'external_ids': {'IFDB': 'External_SubjectID_2', 'TASKMANAGER': 'External_SubjectID_2'}
    }

    response = client.get('/api/v2/subjects/100/external_ids', headers=admin_auth)
    assert response.status_code == 404


def test_subject_external_ids_normal_post(client, subject_links):
    data = {
        'external_ids': {
            'TASKMANAGER': 'new-external-id',
            'XNAT': 'new-xnat-id'
        }
    }

    # Normal scenario
    response = client.post('/api/v2/subjects/1/external_ids', headers=admin_auth, json=data)
    assert response.status_code == 201
    data = response.get_json()
    print(data)
    assert data == {
        'subject_id': 1,
        'subject_label': 'Test Subject_1',
        'external_ids': {'TASKMANAGER': 'new-external-id', 'XNAT': 'new-xnat-id'}
    }

def test_subject_external_ids_nonexisting_subject_post(client, subject_links):
    data = {
        'external_ids': {
            'XNAT': 'new-xnat-id'
        }
    }

    # Non-existing subject
    response = client.post('/api/v2/subjects/100/external_ids', headers=admin_auth, json=data)
    assert response.status_code == 404

def test_subject_external_ids_nonexisting_system_post(client, subject_links):
    nonexisting_system_data = {
        'external_ids': {
            'XNAT-NEW': 'new-xnat-id'
        }
    }

    # Non-existing external_system
    response = client.post('/api/v2/subjects/1/external_ids', headers=admin_auth, json=nonexisting_system_data)
    assert response.status_code == 404
    
    # Check that external ids haven't changed
    response = client.get('/api/v2/subjects/1/external_ids', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    print(data)
    assert data == {
        'subject_id': 1,
        'subject_label': 'Test Subject_1',
        'external_ids': {'XNAT': 'External_SubjectID_1'}
    }


def test_subject_external_ids_wrong_format_post(client, subject_links):
    incorrect_data = {
        'external_ids': [
            'https://new-xnat-url/test_cohort'
        ]
    }

    # Incorrect data format
    response = client.post('/api/v2/subjects/1/external_ids', headers=admin_auth, json=incorrect_data)
    assert response.status_code == 400
    
    # Check that external ids haven't changed
    response = client.get('/api/v2/subjects/1/external_ids', headers=admin_auth)
    assert response.status_code == 200
    
    data = response.get_json()
    print(data)
    assert data == {
        'subject_id': 1,
        'subject_label': 'Test Subject_1',
        'external_ids': {'XNAT': 'External_SubjectID_1'}
    }



def test_subject_user_post(client, subject_links):
    data = {
        'external_ids': {
            'XNAT': 'new-xnat-id'
        }
    }

    # Posting as user is forbidden
    response = client.post('/api/v2/subjects/1/external_ids', headers=user_1_auth, json=data)
    assert response.status_code == 403



def test_subject_external_id_put(client, subject_links):
    data = {
        "external_id": "new-xnat-id"
    }

    # Normal scenario
    response = client.put('/api/v2/subjects/1/external_ids/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 201

    data = response.get_json()
    print(data)
    assert data == {
        'subject_id': 1,
        'subject_label': 'Test Subject_1',
        'external_ids': {'XNAT': 'new-xnat-id'}
    }


def test_subject_external_id_nonexisting_subject_put(client, subject_links):
    data = {
        "external_id": "new-xnat-id"
    }

    # Put a new id for nonexisting subject
    response = client.put('/api/v2/subjects/100/external_ids/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 404


def test_subject_external_id_nonexisting_system_put(client, subject_links):
    data = {
        "external_id": "new-castor-id"
    }

    # Put an id in a nonexisting system
    response = client.put('/api/v2/subjects/1/external_ids/CASTOR', headers=admin_auth, json=data)
    assert response.status_code == 404


def test_subject_external_id_wrong_format_put(client, subject_links):
    data = {
        "url": "https://new-xnat-url/"
    }

    # Putting data in wrong format
    response = client.put('/api/v2/subjects/1/external_ids/XNAT', headers=admin_auth, json=data)
    assert response.status_code == 400


def test_cohort_external_url_user_put(client, subject_links):
    data = {
        "external_id": "new-xnat-id"
    }

    # Putting as user is forbidden
    response = client.put('/api/v2/subjects/1/external_ids/XNAT', headers=user_1_auth, json=data)
    assert response.status_code == 403

