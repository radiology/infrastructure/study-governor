# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from studygovernor.tests.helpers import basic_auth_authorization_header


def test_transitions_get(client, app_config, workflow_test_data):
    response = client.get('/api/v2/transitions', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'transitions': [
            {'id': 1, 'uri': '/api/v2/transitions/1'},
            {'id': 2, 'uri': '/api/v2/transitions/2'},
            {'id': 3, 'uri': '/api/v2/transitions/3'},
            {'id': 4, 'uri': '/api/v2/transitions/4'},
            {'id': 5, 'uri': '/api/v2/transitions/5'},
            {'id': 6, 'uri': '/api/v2/transitions/6'},
            {'id': 7, 'uri': '/api/v2/transitions/7'}
        ]
    }


def test_transition_get(client, app_config, workflow_test_data):
    # Get non-existing transition
    response = client.get('/api/v2/transitions/0', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404

    # Get transition by integer id
    response = client.get('/api/v2/transitions/2', headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 2,
        'uri': '/api/v2/transitions/2',
        'destination_state': {
            'id': 3,
            'label': 'step2a',
            'uri': '/api/v2/states/3'
        },
        'source_state': {
            'id': 2,
            'label': 'step1',
            'uri': '/api/v2/states/2'
        },
        'condition': None
    }



