# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pathlib

from studygovernor.tests.helpers import admin_auth, user_1_auth, superuser_auth, localworker_auth


def test_workflows_get(client, workflow_test_data, second_workflow_test_data):
    response = client.get('/api/v2/workflows', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'workflows': [
            {'id': 1, 'label': 'test_workflow', 'uri': '/api/v2/workflows/1'}, 
            {'id': 2, 'label': 'second_workflow', 'uri': '/api/v2/workflows/2'}]
    }


def test_get_workflow_nonexisting(client, workflow_test_data, second_workflow_test_data):
    # Get a non-existing state
    response = client.get('/api/v2/workflows/0', headers=admin_auth)
    assert response.status_code == 404


def test_get_workflow_by_id(client, workflow_test_data, second_workflow_test_data):
    # Get state by integer id
    response = client.get('/api/v2/workflows/2', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 2,
        'uri': '/api/v2/workflows/2',
        'label': 'second_workflow'
    }


def test_get_workflow_by_label(client, workflow_test_data, second_workflow_test_data):
    # Get state by string label
    response = client.get('/api/v2/workflows/by-label/test_workflow', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 1,
        'uri': '/api/v2/workflows/1',
        'label': 'test_workflow'
    }

def test_post_new_workflow(client, workflow_test_data):
    workflow_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'second_workflow.yaml'
    with open(workflow_filepath) as workflow_file:
        workflow_definition = workflow_file.read()

    response = client.post('/api/v2/service/config/import-workflow',
                            headers=user_1_auth,
                            json = {'workflow_definition': workflow_definition})
    assert response.status_code == 403

    response = client.post('/api/v2/service/config/import-workflow',
                            headers=superuser_auth,
                            json = {'workflow_definition': workflow_definition})
    assert response.status_code == 403
    
    response = client.post('/api/v2/service/config/import-workflow',
                            headers=localworker_auth,
                            json = {'workflow_definition': workflow_definition})
    assert response.status_code == 403

    response = client.post('/api/v2/service/config/import-workflow',
                            headers=admin_auth,
                            json = {'workflow_definition': workflow_definition})
    assert response.status_code == 201
    
    data = response.get_json()
    assert data == {
        'id': 2,
        'label': 'second_workflow',
        'uri': '/api/v2/workflows/2'
    }


def test_post_existing_workflow(client, second_workflow_test_data):
    workflow_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'second_workflow.yaml'
    with open(workflow_filepath) as workflow_file:
        workflow_definition = workflow_file.read()

    response = client.post('/api/v2/service/config/import-workflow',
                            headers=admin_auth,
                            json = {'workflow_definition': workflow_definition})
    assert response.status_code == 409


def test_post_workflow_no_label(client, app_config):
    workflow_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'test_workflow_no_label.yaml'
    with open(workflow_filepath) as workflow_file:
        workflow_definition = workflow_file.read()

    response = client.post('/api/v2/service/config/import-workflow',
                            headers=admin_auth,
                            json = {'workflow_definition': workflow_definition})
    assert response.status_code == 400


def test_get_workflow_states(client, workflow_test_data, second_workflow_test_data):
    response = client.get('/api/v2/workflows/test_workflow/states', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'states': [
            {'id': 1, 'label': 'untracked', 'uri': '/api/v2/states/1'}, 
            {'id': 2, 'label': 'step1', 'uri': '/api/v2/states/2'}, 
            {'id': 3, 'label': 'step2a', 'uri': '/api/v2/states/3'}, 
            {'id': 4, 'label': 'step2b', 'uri': '/api/v2/states/4'}, 
            {'id': 5, 'label': 'step3', 'uri': '/api/v2/states/5'}, 
            {'id': 6, 'label': 'finished', 'uri': '/api/v2/states/6'}
        ]
    }

    response = client.get('/api/v2/workflows/nonexisting/states', headers=admin_auth)
    assert response.status_code == 404

def test_get_workflow_state(client, workflow_test_data, second_workflow_test_data):
    response = client.get('/api/v2/workflows/test_workflow/states/step3', headers=admin_auth)
    assert response.status_code == 200

    data = response.get_json()
    assert data == {
        'id': 5, 
        'label': 'step3', 
        'uri': '/api/v2/states/5', 
        'callbacks': [], 
        'freetext': '', 
        'workflow': {
            'id': 1, 
            'label': 'test_workflow', 
            'uri': '/api/v2/workflows/1'
        }, 
        'experiments': []
    }

    response = client.get('/api/v2/workflows/nonexisting/states/step3', headers=admin_auth)
    assert response.status_code == 404

    response = client.get('/api/v2/workflows/test_workflow/states/step100', headers=admin_auth)
    assert response.status_code == 404