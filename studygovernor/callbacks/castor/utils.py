import json


def pprint(json_obj):
    print(json.dumps(json_obj, indent=2))
