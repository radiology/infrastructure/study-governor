# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pathlib import Path
from typing import Any, Dict

import pytest
import yaml
from pytest_mock import MockerFixture

from studygovernor.callbacks import castor


@pytest.fixture
def template_data():
    template_path = Path(__file__).parent / "data" / "template.yaml"
    assert template_path.is_file()

    with open(template_path) as fin:
        template = yaml.safe_load(fin)

    return template


@pytest.fixture
def if_data():
    if_data_path = Path(__file__).parent / "data" / "if_data.yaml"
    assert if_data_path.is_file()

    with open(if_data_path) as fin:
        if_data = yaml.safe_load(fin)

    return if_data


class FakeTaskManagerSession:
    def __init__(self):
        self.template_data = {
            "comment": "Emtpy template",
            "qa_fields": {}
        }

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def connect(self, uri, *args, **kwargs):
        print(f"Fake TaskManager client connect to {uri}")
        return self

    def get_json(self, uri):
        if not uri.startswith('/task_templates/'):
            raise NotImplementedError(f'There should be no non-template queries in these tests, requested uri: {uri}')

        return_data = {
            "uri": uri,
            "label": uri.split('/')[-1],
            "content": yaml.safe_dump(self.template_data),
        }

        return return_data


class FakeXNATSession:
    def __init__(self):
        self.calls = []
        self.result_data = {
            "some": "json data"
        }

    def connect(self, server, *args, **kwargs):
        print(f"Fake XNAT session connect to {server}")
        return self

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def wipe(self):
        self.calls = []

    def get_json(self, uri):
        self.calls.append(uri)

        if uri.endswith('wrong.json'):
            return None

        if uri.endswith('/files'):
            return {'ResultSet': {'Result': [
                {'Name': 'test_2022-10-17T14:11:26.323532.json'},
                {'Name': 'test_2022-10-19T09:20:41.623586.json'},
                {'Name': 'single_file.json'},
                {'Name': 'test_2022-10-21T08:49:51.784578.json'},
                {'Name': 'second_2022-10-18T16:10:33.json'},
                {'Name': 'test_2022-10-18T16:09:33.341586.json'},
                {'Name': 'second_2022-10-19T16:10:33.json'},
            ]}}
        else:
            return self.result_data


class FakeNetRc:
    def __init__(self):
        self.requests = []

    def netrc(self):
        return self

    def authenticators(self, netloc):
        self.requests.append(netloc)
        print(f"FakeNetRc request for {netloc}")
        return 'test_user', 'unused', 'test_password'


class FakeResponse:
    def __init__(self, status_code: int):
        self.status_code = status_code


class FakeCastorClient:
    def __init__(self, castor_server=None):
        self.castor_server = castor_server
        self.client_id = None
        self.client_secret = None
        self.posts = []
        self.meta_posts = []

    def __call__(self, castor_server):
        self.castor_server = castor_server
        return self

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def login(self, client_id, client_secret):
        print(f"Fake Castor client loggin into: {self.castor_server} with user {client_id}")
        self.client_id = client_id
        self.client_secret = client_secret

    def get_items(self, endpoint, item_key):
        if item_key == 'study':
            return [{
                'name': 'Rotterdam Scan Study',
                'study_id': 'RSS_STUDY_ID',
            }]
        elif item_key == 'sites':
            return [{
                'name': 'Erasmus MC',
                'id': 'EMC_SITE_ID',
            }]

        raise NotImplementedError(f"Fake Castor client cannot get items for {item_key} (uising endpoint {endpoint}.")

    def post(self, endpoint, data):
        self.posts.append({
            'endpoint': endpoint,
            'data': data,
        })

        return FakeResponse(status_code=201)

    def post_meta(self,
                  study_id: str,
                  participant_id: str,
                  data: Dict[str, Any],
                  visit_name: str):
        self.meta_posts.append({
            "study_id": study_id,
            "participant_id": participant_id,
            "data": data,
            "visit_name": visit_name,
        })


def test_download():
    session = FakeXNATSession()
    result = castor.download_latest_file_version(session, 'resources/FIELDS/files/test_{timestamp}.json')
    assert result == {"some": "json data"}
    assert session.calls == ['resources/FIELDS/files', 'resources/FIELDS/files/test_2022-10-21T08:49:51.784578.json']
    session.wipe()

    result = castor.download_latest_file_version(session, 'resources/OTHER/files/second_{timestamp}.json')
    assert result == {"some": "json data"}
    assert session.calls == ['resources/OTHER/files', 'resources/OTHER/files/second_2022-10-19T16:10:33.json']
    session.wipe()

    result = castor.download_latest_file_version(session, 'resources/FIELDS/files/single_file.json')
    assert result == {"some": "json data"}
    assert session.calls == ['resources/FIELDS/files/single_file.json']
    session.wipe()

    result = castor.download_latest_file_version(session, 'resources/FIELDS/files/wrong.json')
    assert result is None
    session.wipe()

    result = castor.download_latest_file_version(session, 'resources/FIELDS/files/nonexisting_{timestamp}.json')
    assert result is None
    session.wipe()


def test_collect_fields(template_data, if_data):
    # Test against tiny bit of data
    result = castor.collect_fields(template_data['qa_fields'], {
        "t1_non_usable_checkbox": False,
        "swi_non_usable_checkbox": True,
        "tabs": {
            "cortical_infarcts": [],
            "if_tabs": {
                "if_calcifications": {
                    "if_other_choroid_plexus": False,
                    "if_other_falx_cerebri": False,
                    "if_other_globus_pallidus": True,
                    "if_other_hippocampus": False
                }
            }
        }
    })

    assert result == {
        'cortical_infarcts_count': 0,
        "cortical_infarcts": [],
        "t1_non_usable_checkbox": False,
        "swi_non_usable_checkbox": True,
        "if_other_choroid_plexus": False,
        "if_other_falx_cerebri": False,
        "if_other_globus_pallidus": True,
        "if_other_hippocampus": False
    }

    # Test against a more real data file
    result = castor.collect_fields(template_data['qa_fields'], if_data)

    assert result == {
        'aneurysms_count': 2,
        'aneurysms': [
            {
                'if_aneurysm_consensus': False,
                'if_aneurysm_consensus_description': '',
                'if_aneurysm_marker_x': -140.13665771484375,
                'if_aneurysm_marker_y': -147.46087646484375,
                'if_aneurysm_marker_z': 69.5999526977539,
                'if_aneurysm_site': 'Ophtalmic a.',
                'if_aneurysm_size': 5.0
            },
            {
                'if_aneurysm_consensus': False,
                'if_aneurysm_consensus_description': '',
                'if_aneurysm_marker_x': -119.62884521484375,
                'if_aneurysm_marker_y': -151.3671112060547,
                'if_aneurysm_marker_z': 69.5999526977539,
                'if_aneurysm_site': 'ACoA',
                'if_aneurysm_size': 6.0}
        ],
        'cortical_infarcts_count': 0,
        'cortical_infarcts': [],
        'cysts_count': 0,
        'cysts': [],
        'flair_non_usable_checkbox': False,
        'if_other_AVM': False,
        'if_other_DVA': False,
        'if_other_PTA': False,
        'if_other_carotid_occl_l': False,
        'if_other_carotid_occl_r': False,
        'if_other_cavum_sept': False,
        'if_other_choroid_plexus': False,
        'if_other_description': '',
        'if_other_dural_av': False,
        'if_other_ectopic_gm': False,
        'if_other_empty_sella': False,
        'if_other_falx_cerebri': False,
        'if_other_fibrous_dyspl': False,
        'if_other_globus_pallidus': False,
        'if_other_hemorrhage': False,
        'if_other_hippocampus': False,
        'if_other_hyperostosis': False,
        'if_other_kele_cerebellum': False,
        'if_other_kele_cerebrum': False,
        'if_other_macroadenoma': False,
        'if_other_mastoid_fluid': False,
        'if_other_microadenoma': False,
        'if_other_sdh': False,
        'if_other_sup_siderosis': False,
        'lacunar_infarcts_count': 0,
        'lacunar_infarcts': [],
        'microbleeds_count': 0,
        'microbleeds': [],
        'no_aneurysms': False,
        'no_cortical_infarcts': True,
        'no_cysts': True,
        'no_lacunar_infarcts': True,
        'no_microbleeds': True,
        'no_other': True,
        'no_tumors': False,
        'pd_non_usable_checkbox': False,
        'swi_non_usable_checkbox': False,
        't1_non_usable_checkbox': False,
        'tumors_count': 2,
        'tumors': [
            {
                'if_tumor_consensus': False,
                'if_tumor_consensus_description': '',
                'if_tumor_marker_x': -171.87490844726562,
                'if_tumor_marker_y': -85.93745422363281,
                'if_tumor_marker_z': 69.5999526977539,
                'if_tumor_size': 5.0,
                'if_tumor_type': 'Lipoma'
            },
            {
                'if_tumor_consensus': False,
                'if_tumor_consensus_description': '',
                'if_tumor_marker_x': -124.99993896484375,
                'if_tumor_marker_y': -85.93745422363281,
                'if_tumor_marker_z': 69.5999526977539,
                'if_tumor_size': 52.0,
                'if_tumor_type': 'Salivary gland tumor'
            }
        ]
    }


def test_castor_no_netrc(mocker: MockerFixture,
                         callback_execution_data: Dict):
    mocker.patch.object(castor, attribute='xnat', new=FakeXNATSession())
    mocker.patch.object(castor, attribute='taskclient', new=FakeTaskManagerSession())

    # Check if the netrc error is thrown correctly if no login information is present
    netrc_mock = mocker.patch.object(castor, attribute='netrc')
    netrc_mock.netrc.side_effect = IOError('Mocked Error')
    with pytest.raises(ValueError, match="Could not get login information for (.+) from netrc"):
        castor.castor(callback_execution_data=callback_execution_data,
                      config={
                          "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
                      },
                      field_files='resources/FIELDS/files/test_{timestamp}.json',
                      templates='test_template',
                      castor_study_name='Rotterdam Scan Study',
                      castor_site_name='Erasmus MC',
                      castor_visit_name='ERGO-1',
                      extra_variable_map={'ergoid': '/subject/label'},
                      translation_map={"if_hippo_vol": "hippocampus volume"},
                      castor_external_system_name='XNAT')


def test_castor(mocker: MockerFixture,
                callback_execution_data: Dict,
                if_data: Dict,
                template_data: Dict):
    # Setup fake xnat session
    xnat_session = FakeXNATSession()
    xnat_session.result_data = if_data
    mocker.patch.object(castor, attribute='xnat', new=xnat_session)

    # Setup fake taskclient session
    task_client = FakeTaskManagerSession()
    task_client.template_data = template_data
    mocker.patch.object(castor, attribute='taskclient', new=task_client)

    # Setup fake castor client
    castor_client = FakeCastorClient()
    mocker.patch.object(castor, attribute="CastorConnection", new=castor_client)

    # Insert our fake netrc to supply login information
    mocker.patch.object(castor, attribute='netrc', new=FakeNetRc())

    # Test error on unknown study
    with pytest.raises(KeyError, match='CRITICAL: Cannot find study "Other Study" on Castor, aborting!') as exception:
        castor.castor(callback_execution_data=callback_execution_data,
                      config={
                          "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
                      },
                      field_files='resources/FIELDS/files/test_{timestamp}.json',
                      templates='test_template',
                      castor_study_name='Other Study',
                      castor_site_name='Erasmus MC',
                      castor_visit_name='ERGO-1',
                      extra_variable_map={
                          'ergoid': '/subject/label',
                          'xnat_exp_id': '/experiment/external_ids/XNAT',
                      },
                      translation_map={"if_hippo_vol": "hippocampus volume"},
                      castor_external_system_name='XNAT')

    # Test error on unknown site
    with pytest.raises(KeyError, match='CRITICAL: Cannot find site "UMC" on Castor, aborting!'):
        castor.castor(callback_execution_data=callback_execution_data,
                      config={
                          "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
                      },
                      field_files='resources/FIELDS/files/test_{timestamp}.json',
                      templates='test_template',
                      castor_study_name='Rotterdam Scan Study',
                      castor_site_name='UMC',
                      castor_visit_name='ERGO-1',
                      extra_variable_map={
                          'ergoid': '/subject/label',
                          'xnat_exp_id': '/experiment/external_ids/XNAT',
                      },
                      translation_map={"if_hippo_vol": "hippocampus volume"},
                      castor_external_system_name='XNAT')

    # Test successful call
    result = castor.castor(callback_execution_data=callback_execution_data,
                           config={
                               "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
                           },
                           field_files='resources/FIELDS/files/test_{timestamp}.json',
                           templates='test_template',
                           castor_study_name='Rotterdam Scan Study',
                           castor_site_name='Erasmus MC',
                           castor_visit_name='ERGO-1',
                           extra_variable_map={
                               'ergoid': '/subject/label',
                               'xnat_exp_id': '/experiment/external_ids/XNAT',
                           },
                           translation_map={"if_hippo_vol": "hippocampus volume"},
                           castor_external_system_name='XNAT')

    assert len(castor_client.posts) == 1
    assert castor_client.posts[0] == {
        'endpoint': '/study/RSS_STUDY_ID/participant',
        'data': {'participant_id': 'Test Subject_1', 'site_id': 'EMC_SITE_ID'}
    }
    assert len(castor_client.meta_posts) == 1
    print(castor_client.meta_posts[0])
    assert castor_client.meta_posts[0] == {
        'study_id': 'RSS_STUDY_ID',
        'participant_id': 'Test Subject_1',
        'data': {
            'flair_non_usable_checkbox': False,
            'pd_non_usable_checkbox': False,
            'swi_non_usable_checkbox': False,
            't1_non_usable_checkbox': False,
            'cortical_infarcts': [],
            'cortical_infarcts_count': 0,
            'no_cortical_infarcts': True,
            'aneurysms': [
                {
                    'if_aneurysm_consensus': False,
                    'if_aneurysm_consensus_description': '',
                    'if_aneurysm_marker_x': -140.13665771484375,
                    'if_aneurysm_marker_y': -147.46087646484375,
                    'if_aneurysm_marker_z': 69.5999526977539,
                    'if_aneurysm_site': 'Ophtalmic a.',
                    'if_aneurysm_size': 5.0
                },
                {
                    'if_aneurysm_consensus': False,
                    'if_aneurysm_consensus_description': '',
                    'if_aneurysm_marker_x': -119.62884521484375,
                    'if_aneurysm_marker_y': -151.3671112060547,
                    'if_aneurysm_marker_z': 69.5999526977539,
                    'if_aneurysm_site': 'ACoA',
                    'if_aneurysm_size': 6.0
                }
            ],
            'aneurysms_count': 2,
            'no_aneurysms': False,
            'cysts': [],
            'cysts_count': 0,
            'no_cysts': True,
            'if_other_choroid_plexus': False,
            'if_other_falx_cerebri': False,
            'if_other_globus_pallidus': False,
            'if_other_hippocampus': False,
            'if_other_carotid_occl_l': False,
            'if_other_carotid_occl_r': False,
            'if_other_cavum_sept': False,
            'if_other_fibrous_dyspl': False,
            'if_other_hyperostosis': False,
            'if_other_ectopic_gm': False,
            'if_other_kele_cerebellum': False,
            'if_other_kele_cerebrum': False,
            'if_other_mastoid_fluid': False,
            'if_other_sdh': False,
            'if_other_description': '',
            'if_other_empty_sella': False,
            'if_other_hemorrhage': False,
            'if_other_macroadenoma': False,
            'if_other_microadenoma': False,
            'if_other_AVM': False,
            'if_other_DVA': False,
            'if_other_PTA': False,
            'if_other_dural_av': False,
            'if_other_sup_siderosis': False,
            'no_other': True,
            'no_tumors': False,
            'tumors': [
                {
                    'if_tumor_consensus': False,
                    'if_tumor_consensus_description': '',
                    'if_tumor_marker_x': -171.87490844726562,
                    'if_tumor_marker_y': -85.93745422363281,
                    'if_tumor_marker_z': 69.5999526977539,
                    'if_tumor_size': 5.0,
                    'if_tumor_type': 'Lipoma'
                }, {
                    'if_tumor_consensus': False,
                    'if_tumor_consensus_description': '',
                    'if_tumor_marker_x': -124.99993896484375,
                    'if_tumor_marker_y': -85.93745422363281,
                    'if_tumor_marker_z': 69.5999526977539,
                    'if_tumor_size': 52.0,
                    'if_tumor_type': 'Salivary gland tumor'
                }
            ],
            'tumors_count': 2,
            'lacunar_infarcts': [],
            'lacunar_infarcts_count': 0,
            'no_lacunar_infarcts': True,
            'microbleeds': [],
            'microbleeds_count': 0,
            'no_microbleeds': True,
            'ergoid': 'Test Subject_1',
            'xnat_exp_id': 'XNAT_EXP_ID'
            },
        'visit_name': 'ERGO-1'
    }

    print(f"Found result {result}")
    assert result == {
        'study_name': 'Rotterdam Scan Study',
        'study_id': 'RSS_STUDY_ID',
        'site_name': 'Erasmus MC',
        'site_id': 'EMC_SITE_ID',
        'participant_id': 'Test Subject_1',
        'field_files': ['resources/FIELDS/files/test_{timestamp}.json'],
        'templates': ['test_template'],
        'participant_existed': False,
        'data_inserted': {'test_template': None},
        '__traceback__': None,
        '__exception__': None,
        '__success__': True
    }
