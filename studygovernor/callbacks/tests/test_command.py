# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest
from pytest_mock import MockerFixture
from unittest.mock import ANY

from studygovernor.callbacks.command import command
from studygovernor.callbacks.tests.test_external_program import MockedProcess


# Test if external_program callback properly calls popen.subproccess
def test_command(callback_execution_data, mocker: MockerFixture):
    mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=MockedProcess(0))

    result = command(
        callback_execution_data=callback_execution_data,
        config={
            "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
        },
        binary='test_script',
        args=["some", "arguments"],
        kwargs={'--extra': "42"},
    )

    assert result == {
        "command": ["test_script", "some", "arguments", "--extra", "42"],
        "stdout": b'STDOUT TEST data',
        "stderr": b'STDERR TEST data',
        "return_code": 0,
        "values": None,
        "__success__": True,
    }

    mocked_popen.assert_called_once_with(["test_script", "some", "arguments", "--extra", "42"], stdout=ANY, stderr=ANY)

    popen_return_value = MockedProcess(0)
    stdout_str = b'This is some test log\nWith some lines\n__VALUES__ = {"test": ["a", "b"], "other": 3.14}\nAll done!'
    popen_return_value.stdout = stdout_str
    mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=popen_return_value)

    result = command(
        callback_execution_data=callback_execution_data,
        config={
            "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
        },
        binary='/usr/bin/test_script2',
        args=["foo", "bar"],
        kwargs=None,
    )

    assert result == {
        "command": ["/usr/bin/test_script2", "foo", "bar"],
        "stdout": stdout_str,
        "stderr": b'STDERR TEST data',
        "return_code": 0,
        "values": {
            'other': 3.14,
            'test': ['a', 'b']
        },
        "__success__": True,
    }

    mocked_popen.assert_called_once_with(['/usr/bin/test_script2', 'foo', 'bar'], stdout=ANY, stderr=ANY)


def test_command_return_code(callback_execution_data, mocker: MockerFixture):
    mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=MockedProcess(1))

    # Test with expected return code 0 (default), which should have __success__=False
    result = command(
        callback_execution_data=callback_execution_data,
        config={
            "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
        },
        binary='test_script',
        args=["some", "arguments"],
        kwargs={'--extra': "42"},
    )

    assert result == {
        "command": ["test_script", "some", "arguments", "--extra", "42"],
        "stdout": b'STDOUT TEST data',
        "stderr": b'STDERR TEST data',
        "return_code": 1,
        "values": None,
        "__success__": False,
    }

    mocked_popen.assert_called_once_with(["test_script", "some", "arguments", "--extra", "42"], stdout=ANY, stderr=ANY)

    # Test with expected return code 1,  which should have __success__=True
    result = command(
        callback_execution_data=callback_execution_data,
        config={
            "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
        },
        binary='test_script',
        args=["some", "arguments"],
        kwargs={'--extra': "42"},
        expected_return_code=1,
    )

    assert result == {
        "command": ["test_script", "some", "arguments", "--extra", "42"],
        "stdout": b'STDOUT TEST data',
        "stderr": b'STDERR TEST data',
        "return_code": 1,
        "values": None,
        "__success__": True,
    }

    # Test without expected return code,  which should have __success__=True
    result = command(
        callback_execution_data=callback_execution_data,
        config={
            "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
        },
        binary='test_script',
        args=["some", "arguments"],
        kwargs={'--extra': "42"},
        expected_return_code=None,
    )

    assert result == {
        "command": ["test_script", "some", "arguments", "--extra", "42"],
        "stdout": b'STDOUT TEST data',
        "stderr": b'STDERR TEST data',
        "return_code": 1,
        "values": None,
        "__success__": True,
    }

    # Test with expected return code 1,  which should have __success__=False
    mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=MockedProcess(0))

    result = command(
        callback_execution_data=callback_execution_data,
        config={
            "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
        },
        binary='test_script',
        args=["some", "arguments"],
        kwargs={'--extra': "42"},
        expected_return_code=1,
    )

    assert result == {
        "command": ["test_script", "some", "arguments", "--extra", "42"],
        "stdout": b'STDOUT TEST data',
        "stderr": b'STDERR TEST data',
        "return_code": 0,
        "values": None,
        "__success__": False,
    }

    mocked_popen.assert_called_once_with(["test_script", "some", "arguments", "--extra", "42"], stdout=ANY, stderr=ANY)
