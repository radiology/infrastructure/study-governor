# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from pathlib import Path
from typing import Dict

import pytest
from pytest_mock import MockerFixture

from studygovernor.callbacks import create_task


@pytest.fixture
def task_base_config(tmp_path: Path) -> Dict[str, str]:
    (tmp_path / 'task_base.yaml').write_text(
        '# TEST TASK\nSUB: $SUBJECT_ID\nEXP: $EXPERIMENT_ID\nLABEL: $LABEL\nURL $SYSTEM_URL'
    )

    (tmp_path / 'task_sub1.yaml').write_text(
        '# TEST SUB TASK 1\nSUB: $SUBJECT_ID\nEXP: $EXPERIMENT_ID\nLABEL: $LABEL\nURL $SYSTEM_URL'
    )
    (tmp_path / 'task_sub2.yaml').write_text(
        '# TEST SUB TASK 2\nSUB: $SUBJECT_ID\nEXP: $EXPERIMENT_ID\nLABEL: $LABEL\nURL $SYSTEM_URL'
    )

    config = {
        'STUDYGOV_PROJECT_TASKS': str(tmp_path)
    }

    return config


class MockedRequest:
    def __init__(self, uri, status_code=201):
        self._uri = uri
        self._json = {'uri': self._uri + '/42'}
        self._status_code = status_code

    @property
    def status_code(self):
        return self._status_code

    @property
    def text(self):
        return json.dumps(self._json)

    def json(self):
        return self._json


def test_create_task(task_base_config: Dict[str, str],
                     mocker: MockerFixture,
                     callback_execution_data: Dict):
    # Mock requests module
    requests_mock = mocker.patch.object(create_task, attribute='requests')
    requests_mock.post.return_value = MockedRequest('https://taskmanager.example.com/api/v1/tasks')
    netrc_mock = mocker.patch.object(create_task, attribute='netrc')
    netrc_mock.netrc.side_effect = IOError('Mocked Error')

    result = create_task.create_task(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        task_base='task_base.yaml',
        task_info={'tags': ['tagged']},
    )

    # Check if correct result has been given
    assert result == {
        'response_status_code': 201,
        'response_text': '{"uri": "https://taskmanager.example.com/api/v1/tasks/42"}',
        'task_info': {'generator_url': 'http://localhost/callback_executions/1',
                      'tags': ['tagged'],
                      'tracking_id': 'Experiment_1_1',
                      'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
                      'callback_url': 'http://localhost/api/v1/callback_executions/1',
                      'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}'},
        'task_uri': 'https://taskmanager.example.com/api/v1/tasks/42',
    }

    # Check if correct requests call has be performed
    requests_mock.post.assert_called_once_with(
        'https://taskmanager.example.com/api/v1/tasks',
        json={
            'generator_url': 'http://localhost/callback_executions/1',
            'tags': ['tagged'],
            'tracking_id': 'Experiment_1_1',
            'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
            'callback_url': 'http://localhost/api/v1/callback_executions/1',
            'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}'
        }
    )

    # Set extra tags but not base tag
    result = create_task.create_task(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        task_base='task_base.yaml',
        task_info={},
        extra_tags_var='tag_var',
    )

    assert result == {
        'response_status_code': 201,
        'response_text': '{"uri": "https://taskmanager.example.com/api/v1/tasks/42"}',
        'task_info': {'generator_url': 'http://localhost/callback_executions/1',
                      'tracking_id': 'Experiment_1_1',
                      'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
                      'callback_url': 'http://localhost/api/v1/callback_executions/1',
                      'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}',
                      'tags': ['tag1', 'tag2']},
        'task_uri': 'https://taskmanager.example.com/api/v1/tasks/42',
    }

    # Set extra tag and base tags
    result = create_task.create_task(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        task_base='task_base.yaml',
        task_info={'tags': ['base_tag']},
        extra_tags_var='tag_var2',
    )

    assert result == {
        'response_status_code': 201,
        'response_text': '{"uri": "https://taskmanager.example.com/api/v1/tasks/42"}',
        'task_info': {'generator_url': 'http://localhost/callback_executions/1',
                      'tracking_id': 'Experiment_1_1',
                      'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
                      'callback_url': 'http://localhost/api/v1/callback_executions/1',
                      'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}',
                      'tags': ['base_tag', 'extra_tag']},
        'task_uri': 'https://taskmanager.example.com/api/v1/tasks/42',
    }

    # Set extra tag and base tags
    result = create_task.create_task(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        task_base='task_base.yaml',
        task_info={'tags': ['base_tag']},
        extra_tags_var='tag_var3',
    )

    assert result == {
        'response_status_code': 201,
        'response_text': '{"uri": "https://taskmanager.example.com/api/v1/tasks/42"}',
        'task_info': {'generator_url': 'http://localhost/callback_executions/1',
                      'tracking_id': 'Experiment_1_1',
                      'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
                      'callback_url': 'http://localhost/api/v1/callback_executions/1',
                      'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}',
                      'tags': ['base_tag']},
        'task_uri': 'https://taskmanager.example.com/api/v1/tasks/42',
    }


    # Set extra tag and base tags
    with pytest.raises(ValueError):
        create_task.create_task(
            callback_execution_data=callback_execution_data,
            config=task_base_config,
            task_base='non_existing_task_base.yaml',
            task_info={'tags': ['base_tag']},
            extra_tags_var='tag_var',
        )

    requests_mock.post.return_value = MockedRequest('https://taskmanager.example.com/api/v1/random', status_code=404)
    with pytest.raises(ValueError):
        create_task.create_task(
            callback_execution_data=callback_execution_data,
            config=task_base_config,
            task_base='task_base.yaml',
            task_info={'tags': ['base_tag']},
            extra_tags_var='tag_var',
        )

    # Attempt to mock the auth
    requests_mock.post.return_value = MockedRequest('https://taskmanager.example.com/api/v1/tasks', status_code=201)
    netrc_mock = mocker.patch.object(create_task, attribute='netrc')
    res = netrc_mock.netrc()
    res.authenticators.return_value = ('user', 'realm', 'pass')

    result = create_task.create_task(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        task_base='task_base.yaml',
        task_info={'tags': ['auth_test']},
    )

    # Check if correct result has been given
    assert result == {
        'response_status_code': 201,
        'response_text': '{"uri": "https://taskmanager.example.com/api/v1/tasks/42"}',
        'task_info': {'generator_url': 'http://localhost/callback_executions/1',
                      'tags': ['auth_test'],
                      'tracking_id': 'Experiment_1_1',
                      'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
                      'callback_url': 'http://localhost/api/v1/callback_executions/1',
                      'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}'},
        'task_uri': 'https://taskmanager.example.com/api/v1/tasks/42',
    }

    # Check if correct requests call has be performed
    requests_mock.post.assert_called_with(
        'https://taskmanager.example.com/api/v1/tasks',
        json={
            'generator_url': 'http://localhost/callback_executions/1',
            'tags': ['auth_test'],
            'tracking_id': 'Experiment_1_1',
            'content': '# TEST TASK\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com',
            'callback_url': 'http://localhost/api/v1/callback_executions/1',
            'callback_content': '{"status": "finished", "result": "success", "secret_key": "ncsnipcaklsdfj"}'
        },
        auth=('user', 'pass')
    )
