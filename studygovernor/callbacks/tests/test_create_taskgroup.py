# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from typing import Dict

import pytest
from pytest_mock import MockerFixture

from studygovernor.callbacks import create_task, create_taskgroup
from studygovernor.callbacks.tests.test_create_task import MockedRequest, task_base_config


def test_create_taskgroup(task_base_config: Dict[str, str],
                          mocker: MockerFixture,
                          callback_execution_data: Dict):
    # Mock requests module
    requests_mock = mocker.patch.object(create_task, attribute='requests')
    requests_mock.post.return_value = MockedRequest('https://taskmanager.example.com/api/v1/task_groups')

    tasks = [
        {
            'base': 'task_sub1.yaml',
            'template': 'sub_template_1',
            'tags': ['test task', 'part 1'],
            'callback_url': None,
        },
        {
            'base': 'task_sub2.yaml',
            'template': 'sub_template_2',
            'tags': ['test task', 'part 2'],
            'callback_url': None,
        }
    ]

    result = create_taskgroup.create_taskgroup(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        label='test_task_group_1',
        tasks=tasks,
        tags=['base tag']
    )

    # Check if correct result has been given
    assert result == {
        "response_status_code": 201,
        "response_text": "{\"uri\": \"https://taskmanager.example.com/api/v1/task_groups/42\"}",
        "task_info": {
            "label": "test_task_group_1",
            "tasks": [
                {
                    "tags": [
                        "base tag",
                        "test task",
                        "part 1"
                    ],
                    "project": None,
                    "callback_url": None,
                    "callback_content": "",
                    "template": "sub_template_1",
                    "generator_url": "http://localhost/callback_executions/1",
                    "content": "# TEST SUB TASK 1\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com"
                },
                {
                    "tags": [
                        "base tag",
                        "test task",
                        "part 2"
                    ],
                    "project": None,
                    "callback_url": None,
                    "callback_content": "",
                    "template": "sub_template_2",
                    "generator_url": "http://localhost/callback_executions/1",
                    "content": "# TEST SUB TASK 2\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com"
                }
            ],
            "callback_url": "http://localhost/api/v1/callback_executions/1",
            "callback_content": "{\"status\": \"finished\", \"result\": \"success\", \"secret_key\": \"ncsnipcaklsdfj\"}"
        },
        "task_uri": "https://taskmanager.example.com/api/v1/task_groups/42"
    }

    # Check if correct requests call has be performed
    requests_mock.post.assert_called_once_with(
        'https://taskmanager.example.com/api/v1/taskgroups',
        json={
            "label": "test_task_group_1",
            "tasks": [
                {
                    "tags": [
                        "base tag",
                        "test task",
                        "part 1"
                    ],
                    "project": None,
                    "callback_url": None,
                    "callback_content": "",
                    "template": "sub_template_1",
                    "generator_url": "http://localhost/callback_executions/1",
                    "content": "# TEST SUB TASK 1\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com"
                },
                {
                    "tags": [
                        "base tag",
                        "test task",
                        "part 2"
                    ],
                    "project": None,
                    "callback_url": None,
                    "callback_content": "",
                    "template": "sub_template_2",
                    "generator_url": "http://localhost/callback_executions/1",
                    "content": "# TEST SUB TASK 2\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com"
                }
            ],
            "callback_url": "http://localhost/api/v1/callback_executions/1",
            "callback_content": "{\"status\": \"finished\", \"result\": \"success\", \"secret_key\": \"ncsnipcaklsdfj\"}"
        }
    )

    # Add more filled tasks
    tasks = [
        {
            'base': 'task_sub1.yaml',
            'template': 'sub_template_1',
            'tags': ['test task', 'part 1'],
            'callback_url': 'https://fake.sub.callback/1',
            'application_name': 'ViewR',
            'application_version': '6.0.0'
        },
        {
            'base': 'task_sub2.yaml',
            'template': 'sub_template_2',
            'tags': ['test task', 'part 2'],
            'callback_url': 'https://fake.sub.callback/2',
            'application_name': 'ViewR',
            'application_version': '8.3.1'
        }
    ]

    # Test create taskgroup with more parametesr filled
    result = create_taskgroup.create_taskgroup(
        callback_execution_data=callback_execution_data,
        config=task_base_config,
        label='test_task_group_1',
        tasks=tasks,
        extra_tags_var='tag_var',
        distribute_in_group='test_group',
        distribute_method='test_distribution',
    )

    assert result == {
        "response_status_code": 201,
        "response_text": "{\"uri\": \"https://taskmanager.example.com/api/v1/task_groups/42\"}",
        "task_info": {
            "label": "test_task_group_1",
            "tasks": [
                {
                    "tags": [
                        "tag1",
                        "tag2",
                        "test task",
                        "part 1"
                    ],
                    "project": None,
                    "callback_url": "https://fake.sub.callback/1",
                    "callback_content": "{\"status\": \"finished\", \"result\": \"success\", \"secret_key\": \"ncsnipcaklsdfj\"}",
                    "template": "sub_template_1",
                    "generator_url": "http://localhost/callback_executions/1",
                    "application_name": "ViewR",
                    "application_version": "6.0.0",
                    "content": "# TEST SUB TASK 1\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com"
                },
                {
                    "tags": [
                        "tag1",
                        "tag2",
                        "test task",
                        "part 2"
                    ],
                    "project": None,
                    "callback_url": "https://fake.sub.callback/2",
                    "callback_content": "{\"status\": \"finished\", \"result\": \"success\", \"secret_key\": \"ncsnipcaklsdfj\"}",
                    "template": "sub_template_2",
                    "generator_url": "http://localhost/callback_executions/1",
                    "application_name": "ViewR",
                    "application_version": "8.3.1",
                    "content": "# TEST SUB TASK 2\nSUB: XNAT_SUB_ID\nEXP: XNAT_EXP_ID\nLABEL: Experiment_1_1\nURL https://xnat.example.com"
                }
            ],
            "distribute_in_group": "test_group",
            "distribute_method": "test_distribution",
            "callback_url": "http://localhost/api/v1/callback_executions/1",
            "callback_content": "{\"status\": \"finished\", \"result\": \"success\", \"secret_key\": \"ncsnipcaklsdfj\"}"
        },
        "task_uri": "https://taskmanager.example.com/api/v1/task_groups/42"
    }

    tasks[0]['base'] = 'random_non_existing_task.yaml'

    with pytest.raises(ValueError):
        create_taskgroup.create_taskgroup(
            callback_execution_data=callback_execution_data,
            config=task_base_config,
            label='test_task_group_1',
            tasks=tasks,
            tags=['base tag']
        )
