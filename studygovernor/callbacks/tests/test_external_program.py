# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import tempfile

import os
import pytest
from pytest_mock import MockerFixture
from unittest.mock import ANY

from studygovernor.callbacks.external_program import external_program


class MockedProcess:
    def __init__(self, returncode):
        self.stdout = b'STDOUT TEST data'
        self.stderr = b'STDERR TEST data'
        self.returncode = returncode

    def communicate(self):
        return self.stdout, self.stderr


# Test if external program callbak raises ERROR when executable does not exist
def test_external_program_no_executable(callback_execution_data, mocker: MockerFixture):
    mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=MockedProcess(0))
    with tempfile.TemporaryDirectory() as bin_dir: 
        with pytest.raises(ValueError):
            result = external_program(
                callback_execution_data=callback_execution_data,
                config={
                    "STUDYGOV_PROJECT_BIN": bin_dir,
                    "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
                },
                binary='sleep',
                args=0,
                kwargs=None,
            )


# Test if external_program callback properly calls popen.subproccess
def test_external_program(callback_execution_data, mocker: MockerFixture):
    with tempfile.TemporaryDirectory() as bin_dir:
        mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=MockedProcess(0))

        with open(bin_dir + '/sleep', 'w') as fout:
            fout.write('Executable file!')

        binary = os.path.join(bin_dir, "sleep")

        result = external_program(
            callback_execution_data=callback_execution_data,
            config={
                "STUDYGOV_PROJECT_BIN": bin_dir,
                "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
            },
            binary='sleep',
            args=["0"],
            kwargs=None,
        )

        assert result == {
            "command": [binary, "0"],
            "stdout": b'STDOUT TEST data',
            "stderr": b'STDERR TEST data',
            "return_code": 0,
            "values": None,
            "__success__": True,
        }

        mocked_popen.assert_called_once_with([binary, '0'], stdout=ANY, stderr=ANY)

        popen_return_value = MockedProcess(0)
        stdout_str = b'This is some test log\nWith some lines\n__VALUES__ = {"tags": ["tags", "tag2"], "other": 12}\nAll done!'
        popen_return_value.stdout = stdout_str
        mocked_popen = mocker.patch('studygovernor.callbacks.command.subprocess.Popen', return_value=popen_return_value)

        result = external_program(
            callback_execution_data=callback_execution_data,
            config={
                "STUDYGOV_PROJECT_BIN": bin_dir,
                "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
            },
            binary='sleep',
            args=["1"],
            kwargs=None,
        )

        assert result == {
            "command": [binary, "1"],
            "stdout": stdout_str,
            "stderr": b'STDERR TEST data',
            "return_code": 0,
            "values": {
                'other': 12,
                'tags': ['tags', 'tag2']
            },
            "__success__": True,
        }

        mocked_popen.assert_called_once_with([binary, '1'], stdout=ANY, stderr=ANY)

