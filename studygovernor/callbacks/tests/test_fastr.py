# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pathlib
import tempfile
import os
from typing import Dict
from unittest.mock import ANY

import pytest
from pytest_mock import MockerFixture

from studygovernor.callbacks.fastr import fastr, create_uri, create_source_sink_data, run_network


def _mock_resource_request(mocker: MockerFixture):
    # Mock the requests call via XNAT to find latest file
    class MockedResponse:
        def json(self):
            return {'ResultSet': {'Result': [
                {'Name': 'test_2022-10-17T14:11:26.323532.json'},
                {'Name': 'test_2022-10-19T09:20:41.623586.json'},
                {'Name': 'test_2022-10-21T08:49:51.784578.json'},
                {'Name': 'test_2022-10-18T16:09:33.341586.json'},
            ]}}

    mocker.patch('studygovernor.callbacks.fastr.requests.get', return_value=MockedResponse())


class MockedProcess:
    def __init__(self, path=None):
        self.path = path

    def wait(self):
        if self.path:
            os.mkdir(self.path)

        return 0


def test_create_uri(mocker: MockerFixture):
    uri_00 = create_uri('http://xnat.example.com', '/data/experiments/EXPLABEL00')
    assert uri_00 == 'xnat+http://xnat.example.com/data/experiments/EXPLABEL00'

    uri_01 = create_uri('https://xnat.example.com', '/data/experiments/EXPLABEL01')
    assert uri_01 == 'xnat+https://xnat.example.com/data/experiments/EXPLABEL01'

    uri_02 = create_uri('https://xnat.example.com', '/data/experiments/EXPLABEL02', {'testvar': 42})
    assert uri_02 == 'xnat+https://xnat.example.com/data/experiments/EXPLABEL02?testvar=42'

    _mock_resource_request(mocker)
    uri_03 = create_uri('https://xnat.example.com', '/data/experiments/EXPLABEL02/resources/FIELDS/files/test_{latest_timestamp}.json', {'testvar': 42})
    assert uri_03 == 'xnat+https://xnat.example.com/data/experiments/EXPLABEL02/resources/FIELDS/files/test_2022-10-21T08:49:51.784578.json?testvar=42'

    with pytest.raises(ValueError):
        uri_04 = create_uri('ftp://xnat.example.com', '/data/experiments/EXPLABEL00')


def test_create_source_sink_data(mocker: MockerFixture):
    # Test most standard use
    data = create_source_sink_data(
        xnat_uri='https://xnat.example.com',
        project='governor_test',
        experiment='EXP_LABEL',
        source_mapping={
            "t1": "/scans/T1W*/resources/DICOM",
            "flair": "/scans/*FLAIR*/resources/DICOM"
        },
        sink_mapping={
            "t1_nii": "/scans/T1W*/resources/NIFTI/files/image{ext}",
            "flair_nii": "/scans/*FLAIR*/resources/NIFTI/files/image{ext}",
            "flair_coregistered": "/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}",
        },
        subject='SUBJECT_ID',
        label='FASTR_LABEL'
    )

    assert data == (
        {
            'flair': {
                'FASTR_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/DICOM'
            },
            't1': {
                'FASTR_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/DICOM'
            }}
        , {
            'flair_coregistered': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            'flair_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            't1_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
        })

    # Test automatic values for subject and label
    data2 = create_source_sink_data(
        xnat_uri='https://xnat.example.com',
        project='governor_test',
        experiment='EXP_LABEL',
        source_mapping={
            "t1": "/scans/T1W*/resources/DICOM",
            "flair": "/scans/*FLAIR*/resources/DICOM"
        },
        sink_mapping={
            "t1_nii": "/scans/T1W*/resources/NIFTI/files/image{ext}",
            "flair_nii": "/scans/*FLAIR*/resources/NIFTI/files/image{ext}",
            "flair_coregistered": "/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}",
        },
    )

    assert data2 == (
        {
            'flair': {
                'EXP_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/*/experiments/EXP_LABEL/scans/*FLAIR*/resources/DICOM'
            },
            't1': {
                'EXP_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/*/experiments/EXP_LABEL/scans/T1W*/resources/DICOM'
            }}
        , {
            'flair_coregistered': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/*/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            'flair_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/*/experiments/EXP_LABEL/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            't1_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/*/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
        })

    # Test the {latest_timestamp} in source mapping
    _mock_resource_request(mocker)
    data3 = create_source_sink_data(
        xnat_uri='https://xnat.example.com',
        project='governor_test',
        experiment='EXP_LABEL',
        source_mapping={
            "mask": "/scans/T1W*/resources/MASK/files/test_{latest_timestamp}.json",
        },
        sink_mapping={
            "t1_nii": "/scans/T1W*/resources/NIFTI/files/image{ext}",
            "flair_nii": "/scans/*FLAIR*/resources/NIFTI/files/image{ext}",
            "flair_coregistered": "/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}",

        },
        subject='SUBJECT_ID',
        label='FASTR_LABEL'
    )

    assert data3 == (
        {
            'mask': {
                'FASTR_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/MASK/files/test_2022-10-21T08:49:51.784578.json'
            }}
        , {
            'flair_coregistered': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            'flair_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            't1_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
        })


def test_run_network(mocker: MockerFixture,
                     networks_dir: pathlib.Path):

    SOURCE_DATA = {
        'flair': {
            'FASTR_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/DICOM'
        },
        't1': {
            'FASTR_LABEL': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/DICOM'
        }
    }

    SINK_DATA = {
        'flair_coregistered': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
        'flair_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
        't1_nii': 'xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
    }

    with tempfile.TemporaryDirectory() as scratch:
        scratch_path = pathlib.Path(scratch)
        sample_scratch = scratch_path / "fastr_dummy_network_SAMPLE_LABEL"
        with open(sample_scratch, 'w') as fout:
            fout.write('Blocking file!')

        mocked_popen = mocker.patch('studygovernor.callbacks.fastr.subprocess.Popen', return_value=MockedProcess(sample_scratch))

        result = run_network(
            network_id="dummy_network",
            source_data=SOURCE_DATA,
            sink_data=SINK_DATA,
            label="SAMPLE_LABEL",
            fastr_home="/home/user/.fastr",
            config={
                "STUDYGOV_PROJECT_NETWORKS": str(networks_dir),
                "STUDYGOV_PROJECT_SCRATCH": scratch,
            },
        )

        code_file_path = str(scratch_path / "fastr_dummy_network_SAMPLE_LABEL.py")
        assert result == {
            'network_id': 'dummy_network',
            'code_file': code_file_path,
            'scratch_dir': str(sample_scratch),
            'stdout_log': str(scratch_path / "fastr_dummy_network_SAMPLE_LABEL_stdout.txt"),
            'stderr_log': str(scratch_path / "fastr_dummy_network_SAMPLE_LABEL_stderr.txt"),
            'fastr_home': '/home/user/.fastr',
            'source_data': SOURCE_DATA,
            'sink_data': SINK_DATA,
            'cleanup_done': True,
            'return_value': 0,
        }

        # Check correct Popen call
        assert mocked_popen.call_count == 1
        mocked_popen.assert_called_with(['fastr_python_launcher', code_file_path], stdout=ANY, stderr=ANY)

        with open(f"{scratch}/fastr_dummy_network_SAMPLE_LABEL.py") as fh:
            code_file_data = fh.read()
            print(f"{code_file_data}")

        assert code_file_data == """#!/bin/env python
# Autogenerated file for network execution via module

import imp
import fastr

# Log to console and reinitialize to take redirected stdout
fastr.config.logtype = 'console'
fastr.config._update_logging()

source_data = {
    "flair": {
        "FASTR_LABEL": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/DICOM"
    },
    "t1": {
        "FASTR_LABEL": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/DICOM"
    }
}

sink_data = {
    "flair_coregistered": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData",
    "flair_nii": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData",
    "t1_nii": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData"
}

network_module = imp.load_source('dummy_network', '""" + str(scratch_path / "fastr_dummy_network_SAMPLE_LABEL_network.py") + """')
network = network_module.create_network()
run = network.execute(source_data, sink_data, tmpdir='vfs://tmp/fastr_dummy_network_SAMPLE_LABEL')
exit(0 if run.result else 1)
"""

        # Make a tempdir to check for that condition
        sample_scratch = f"{scratch}/fastr_serialised_network_SECOND_LABEL"
        os.mkdir(sample_scratch)

        mocked_popen = mocker.patch('studygovernor.callbacks.fastr.subprocess.Popen', return_value=MockedProcess())

        result = run_network(
            network_id="serialised_network",
            source_data=SOURCE_DATA,
            sink_data=SINK_DATA,
            label="SECOND_LABEL",
            fastr_home="/home/user/.fastr",
            config={
                "STUDYGOV_PROJECT_NETWORKS": str(networks_dir),
                "STUDYGOV_PROJECT_SCRATCH": scratch,
            },
        )

        code_file_path = str(scratch_path / "fastr_serialised_network_SECOND_LABEL.py")
        assert result == {
            'network_id': 'serialised_network',
            'code_file': code_file_path,
            'scratch_dir': str(scratch_path / "fastr_serialised_network_SECOND_LABEL"),
            'stdout_log': str(scratch_path / "fastr_serialised_network_SECOND_LABEL_stdout.txt"),
            'stderr_log': str(scratch_path / "fastr_serialised_network_SECOND_LABEL_stderr.txt"),
            'fastr_home': '/home/user/.fastr',
            'source_data': SOURCE_DATA,
            'sink_data': SINK_DATA,
            'cleanup_done': False,
            'return_value': 0,
        }

        # Check correct Popen call
        assert mocked_popen.call_count == 1
        mocked_popen.assert_called_with(['fastr_python_launcher', code_file_path], stdout=ANY, stderr=ANY)

        with open(f"{scratch}/fastr_serialised_network_SECOND_LABEL.py") as fh:
            code_file_data = fh.read()
            print(f"{code_file_data}")

        assert code_file_data == """#!/bin/env python
# Autogenerated file for network execution via module

import imp
import fastr

# Log to console and reinitialize to take redirected stdout
fastr.config.logtype = 'console'
fastr.config._update_logging()

source_data = {
    "flair": {
        "FASTR_LABEL": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/DICOM"
    },
    "t1": {
        "FASTR_LABEL": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/DICOM"
    }
}

sink_data = {
    "flair_coregistered": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData",
    "flair_nii": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData",
    "t1_nii": "xnat+https://xnat.example.com/data/archive/projects/governor_test/subjects/SUBJECT_ID/experiments/EXP_LABEL/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData"
}

network = fastr.Network.loadf('""" + str(scratch_path / "fastr_serialised_network_SECOND_LABEL_network.json") + """')
run = network.execute(source_data, sink_data, tmpdir='vfs://tmp/fastr_serialised_network_SECOND_LABEL')
exit(0 if run.result else 1)
"""

        with pytest.raises(ValueError):
            run_network(
                network_id="nonexisting_network",
                source_data=SOURCE_DATA,
                sink_data=SINK_DATA,
                label="NEW_LABEL",
                fastr_home="/home/user/.fastr",
                config={
                    "STUDYGOV_PROJECT_NETWORKS": str(networks_dir),
                    "STUDYGOV_PROJECT_SCRATCH": scratch,
                },
            )


def test_fastr_callback(mocker: MockerFixture,
                        networks_dir: pathlib.Path,
                        callback_execution_data: Dict):
    with tempfile.TemporaryDirectory() as scratch:
        scratch_path = pathlib.Path(scratch)
        mocked_popen = mocker.patch('studygovernor.callbacks.fastr.subprocess.Popen', return_value=MockedProcess())

        result = fastr(
            callback_execution_data=callback_execution_data,
            config={
                "STUDYGOV_PROJECT_NETWORKS": str(networks_dir),
                "STUDYGOV_PROJECT_SCRATCH": scratch,
                "STUDYGOV_XNAT_PROJECT": 'TEST_PROJECT'
            },
            network_id='dummy_network',
            source_mapping={
                "t1": "/scans/T1W*/resources/DICOM",
                "flair": "/scans/*FLAIR*/resources/DICOM"
            },
            sink_mapping={
                "t1_nii": "/scans/T1W*/resources/NIFTI/files/image{ext}",
                "flair_nii": "/scans/*FLAIR*/resources/NIFTI/files/image{ext}",
                "flair_coregistered": "/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}",
            }
        )

        code_file_path = str(scratch_path / "fastr_dummy_network_Experiment_1_1.py")
        stdout_path = str(scratch_path / "fastr_dummy_network_Experiment_1_1_stdout.txt")
        stderr_path = str(scratch_path / "fastr_dummy_network_Experiment_1_1_stderr.txt")
        assert result == {
            'network_id': 'dummy_network',
            'code_file': code_file_path,
            'scratch_dir': str(scratch_path / "fastr_dummy_network_Experiment_1_1"),
            'stdout_log': stdout_path,
            'stderr_log': stderr_path,
            'fastr_home': None,
            'source_data': {
                'flair': {
                    'Experiment_1_1': 'xnat+https://xnat.example.com/data/archive/projects/TEST_PROJECT/subjects/XNAT_SUB_ID/experiments/XNAT_EXP_ID/scans/*FLAIR*/resources/DICOM'
                },
                't1': {
                    'Experiment_1_1': 'xnat+https://xnat.example.com/data/archive/projects/TEST_PROJECT/subjects/XNAT_SUB_ID/experiments/XNAT_EXP_ID/scans/T1W*/resources/DICOM'
                }
            },
            'sink_data': {
                'flair_coregistered': 'xnat+https://xnat.example.com/data/archive/projects/TEST_PROJECT/subjects/XNAT_SUB_ID/experiments/XNAT_EXP_ID/scans/T1W*/resources/NIFTI/files/flair_to_t1{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
                'flair_nii': 'xnat+https://xnat.example.com/data/archive/projects/TEST_PROJECT/subjects/XNAT_SUB_ID/experiments/XNAT_EXP_ID/scans/*FLAIR*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
                't1_nii': 'xnat+https://xnat.example.com/data/archive/projects/TEST_PROJECT/subjects/XNAT_SUB_ID/experiments/XNAT_EXP_ID/scans/T1W*/resources/NIFTI/files/image{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData',
            },
            'cleanup_done': False,
            'return_value': 0,
        }

        # Check correct Popen call
        assert mocked_popen.call_count == 1
        mocked_popen.assert_called_with(['fastr_python_launcher', code_file_path], stdout=ANY, stderr=ANY)
