# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import deepdiff
import re

from studygovernor.callbacks.python_function import python_function


def function_with_args_and_kwargs(arg_1, arg_2, kwarg_1=None, kwarg_2=None):
    result = {
        'arg_1': arg_1,
        'arg_2': arg_2,
        'kwarg_1': kwarg_1,
        'kwarg_2': kwarg_2,
    }
    return result
    

def function_with_kwargs(kwarg_1=None, kwarg_2=None):
    result = {
        'kwarg_1': kwarg_1,
        'kwarg_2': kwarg_2,
    }
    return result


def function_with_args(arg_1, arg_2):
    result = {
        'arg_1': arg_1,
        'arg_2': arg_2,
    }
    return result


def function_without_args():
    return 42


def function_raising_exception():
    raise NotImplementedError


class RandomObject:
    def __init__(self, value):
        self.value = value


def function_with_invalid_return():
    return {"a": 42, "b": [RandomObject(42)]}


def function_with_callback_execution_data_and_kwargs(callback_execution_data, config, kwarg_1=None, kwarg_2=None):
    result = {
        'kwarg_1': kwarg_1,
        'kwarg_2': kwarg_2,
        'date_of_birth': callback_execution_data['subject']['date_of_birth']
    }
    return result


def function_with_callback_execution_data_and_args(callback_execution_data, config, arg_1, arg_2):
    result = {
        'arg_1': arg_1,
        'arg_2': arg_2,
        'xnat_id': callback_execution_data['experiment']['external_ids']['XNAT']
    }

    return result


def test_python_function_with_args_and_kwargs(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_with_args_and_kwargs',
        pass_callback_data=False,
        args=['arg_1', 'arg_2'],
        kwargs={
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        }
    )

    assert result == {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_with_args_and_kwargs',
        "args": ["arg_1", "arg_2"],
        "kwargs": {
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        },
        "return_value": {
            'arg_1': 'arg_1',
            'arg_2': 'arg_2',
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        },
        '__exception__': None,
        '__success__': True,
        '__traceback__': None,
    }
    

def test_python_function_with_kwargs(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_with_kwargs',
        pass_callback_data=False,
        args=None,
        kwargs={
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        }
    )

    assert result == {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_with_kwargs',
        "args": [],
        "kwargs": {
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        },
        "return_value": {
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        },
        '__exception__': None,
        '__success__': True,
        '__traceback__': None,
    }


def test_python_function_with_args(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_with_args',
        pass_callback_data=False,
        args=['arg_1', 'arg_2']
    )

    assert result == {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_with_args',
        "args": ["arg_1", "arg_2"],
        "kwargs": {},
        "return_value": {
            'arg_1': 'arg_1',
            'arg_2': 'arg_2',
        },
        '__exception__': None,
        '__success__': True,
        '__traceback__': None,
    }


def test_python_function_without_args(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_without_args',
        pass_callback_data=False,
    )

    assert result == {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_without_args',
        "args": [],
        "kwargs": {},
        "return_value": 42,
        '__exception__': None,
        '__success__': True,
        '__traceback__': None,
    }


def test_python_function_raising_exception(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_raising_exception',
        pass_callback_data=False,
        args=None,
    )

    difference = deepdiff.DeepDiff(result, {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_raising_exception',
        "args": [],
        "kwargs": {},
        "return_value": None,
        '__exception__': 'Exception occurred when executing function: ',
        '__success__': False,
    }, exclude_paths=[
        "root['__traceback__']"
    ])

    assert difference == {}

    # Check if traceback matches generally, in Python 3.11 traceback format changed so can't check to strict
    assert result['__traceback__'].endswith('in function_raising_exception\n    raise NotImplementedError\nNotImplementedError\n')


def test_python_function_invalid_return_value(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_with_invalid_return',
        pass_callback_data=False,
    )

    difference = deepdiff.DeepDiff(result, {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_with_invalid_return',
        "args": [],
        "kwargs": {},
        "return_value": None,
        '__exception__': None,
        '__success__': False,
        '__traceback__': None,
    }, exclude_paths=[
        "root['__exception__']"
    ])

    assert difference == {}

    assert result['__exception__'].startswith("Cannot serialise result to JSON, cannot store result! Result value:")


def test_python_function_with_callback_execution_data_args(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_with_callback_execution_data_and_args',
        pass_callback_data=True,
        args=['arg_1', 'arg_2'],
        kwargs={}
    )

    assert result == {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_with_callback_execution_data_and_args',
        "args": ["arg_1", "arg_2"],
        "kwargs": {},
        "return_value": {
            'arg_1': 'arg_1',
            'arg_2': 'arg_2',
            'xnat_id': 'XNAT_EXP_ID',
        },
        '__exception__': None,
        '__success__': True,
        '__traceback__': None,
    }


def test_python_function_with_callback_execution_data_kwargs(callback_execution_data):
    result = python_function(
        callback_execution_data,
        config={},
        package='studygovernor.callbacks.tests',
        module='test_python_function',
        function='function_with_callback_execution_data_and_kwargs',
        args=None,
        kwargs={
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        }
    )

    assert result == {
        "package": 'studygovernor.callbacks.tests',
        "module": 'test_python_function',
        "function": 'function_with_callback_execution_data_and_kwargs',
        "args": [],
        "kwargs": {
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
        },
        "return_value": {
            'kwarg_1': 'kwarg_1',
            'kwarg_2': 'kwarg_2',
            'date_of_birth': '2019-01-01',
        },
        '__exception__': None,
        '__success__': True,
        '__traceback__': None,
    }
