from typing import Dict
from pytest_mock import MockerFixture

from studygovernor.callbacks.send_mail import send_mail


class MockedMailExtension:
    def __init__(self):
        pass

    def send(self, message):
        pass


class MockedMessage:
    def __init__(self, subject, recipients, body, html):
        pass


class MockedCurrentApp:
    def __init__(self):
        self.extensions = {"mail": MockedMailExtension()}
        self.config = {
            "STUDYGOV_EMAIL_TO": "test_recipient",
            "STUDYGOV_EMAIL_FROM": "test_sender",
            "STUDYGOV_EMAIL_PREFIX": "test_prefix",
        }


def test_send_mail_callback(mocker: MockerFixture,
                            callback_execution_data: Dict):
    # To prevent the 'Working outside of app context' error when testing, some Flask elements
    # are mocked.
    mocker.patch("studygovernor.util.mail.has_app_context", return_value=True)
    mocker.patch("studygovernor.util.mail.current_app", MockedCurrentApp())
    mocker.patch("studygovernor.util.mail.render_template", return_value="html_body")
    mocker.patch("studygovernor.util.mail.Message", MockedMessage)

    subject_format_string = "subject_experiment:{experiment};" \
        + "experiment_url:{experiment_url};" \
        + "experiment_api_url:{experiment_api_url};" \
        + "action_url:{action_url};" \
        + "action_api_url:{action_api_url}"

    body_format_string = "body_experiment:{experiment};" \
        + "experiment_url:{experiment_url};" \
        + "experiment_api_url:{experiment_api_url};" \
        + "action_url:{action_url};" \
        + "action_api_url:{action_api_url}"

    result = send_mail(
        callback_execution_data=callback_execution_data,
        config={"test": "not_necessary"},
        subject=subject_format_string,
        body=body_format_string
    )

    sender_expected = "test_sender"
    recipient_expected = ["test_recipient"]
    subject_expected = "subject_experiment:Experiment_1_1;" \
        + "experiment_url:http://localhost/experiments/1;" \
        + "experiment_api_url:http://localhost/api/v1/experiments/1;" \
        + "action_url:http://localhost/actions/5;" \
        + "action_api_url:http://localhost/api/v1/actions/5"

    body_expected = "body_experiment:Experiment_1_1;" \
        + "experiment_url:http://localhost/experiments/1;" \
        + "experiment_api_url:http://localhost/api/v1/experiments/1;" \
        + "action_url:http://localhost/actions/5;" \
        + "action_api_url:http://localhost/api/v1/actions/5"

    result_expected = {
        "sender": sender_expected,
        "recipient": recipient_expected,
        "subject": subject_expected,
        "body": body_expected,
    }

    assert result == result_expected
