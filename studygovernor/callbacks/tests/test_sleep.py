# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pytest_mock import MockerFixture

from studygovernor.callbacks.sleep import sleep


def test_create_task(mocker: MockerFixture):
    # Mock requests module
    time_mock = mocker.patch('studygovernor.callbacks.sleep.time')

    result = sleep(
        callback_execution_data={},
        config={},
        seconds=3,
    )

    assert result == {
        'sleep_duration': 3,
        'extra_kwargs': {}
    }
    time_mock.sleep.assert_called_once_with(3)

    result = sleep(
        callback_execution_data={},
        config={},
        seconds=10,
        random=42,
    )

    assert result == {
        'sleep_duration': 10,
        'extra_kwargs': {
            'random': 42
        }
    }
    time_mock.sleep.assert_called_with(10)
