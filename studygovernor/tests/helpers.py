""" A collection of helpers for testing. """
from base64 import b64encode


def basic_auth_authorization_header(username, password):
    """ Generate the basic auth header from the username and password. """
    auth_string = b64encode(b":".join((username.encode('latin1'), password.encode('latin1')))).decode()
    return {"Authorization": "Basic {}".format(auth_string)}


admin_auth = basic_auth_authorization_header("admin", "admin")
superuser_auth = basic_auth_authorization_header("superuser", "superuser")
user_1_auth = basic_auth_authorization_header("user1", "user1")
user_2_auth = basic_auth_authorization_header("user2", "user2")
localworker_auth = basic_auth_authorization_header("localworker", "localworker")
