# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime

import datetime

import pytest
import yaml
from pytest_mock import MockerFixture

from studygovernor import exceptions
from studygovernor.models import db
from studygovernor.models import Experiment, Subject, Workflow, State, CallbackExecution, \
    CallbackExecutionStatus, CallbackExecutionResult
from studygovernor.control import get_variable, get_variables, set_variable, create_experiment, set_state,\
    check_condition, update_state, update_callback_execution_state


def test_create_experiment(workflow_test_data, subject_data):
    from datetime import datetime
    workflow = Workflow.query.first()
    test_subject = Subject.query.first()
    experiment_label = "NewExperiment2"
    experiment_scandate = datetime(2019, 1, 2)
    experiment = create_experiment(workflow, subject=test_subject, label=experiment_label, scandate=experiment_scandate)
    db.session.add(experiment)
    db.session.commit()

    assert experiment.state.workflow is workflow
    assert experiment.subject == test_subject
    assert experiment.label == experiment_label
    assert experiment.scandate == experiment_scandate
    assert experiment.state.label == 'step1'


def test_set_state_experiment(experiment_data):
    experiment = Experiment.query.first()
    wrong_state = State.query.first()
    destination_state = experiment.state.transition_sources[0].destination_state

    with pytest.raises(exceptions.NoValidTransitionError):
        set_state(experiment, wrong_state)

    assert experiment.state.id == 2
    assert experiment.state.label == 'step1'
    set_state(experiment, destination_state)
    assert experiment.state.id == 3
    assert experiment.state.label == 'step2a'


def test_set_state_with_callback(experiment_data_with_callback,
                                 mocker: MockerFixture):
    # Patch the dispatch_callback to avoid actual attempts at callbacks
    mocked_dispatch = mocker.patch('studygovernor.control.dispatch_callback')
    destination_state = State.query.filter(State.label == 'step2').one()

    # Set a variable to test against for all experiments
    experiments = Experiment.query.all()
    for number, experiment in enumerate(experiments, 40):
        set_variable(experiment, 'test', number)
    db.session.commit()

    # Trigger all state changes
    for experiment in experiments:
        print(f"Updating state for {experiment}")
        set_state(experiment, destination_state)

    # Check if the correct callbacks are fired
    expected_calls = [('Experiment_1_2', 'step2_callback2'),
                      ('Experiment_2_1', 'step2_callback1'),
                      ('Experiment_2_2', 'step2_callback2')]

    # Extract resulting calls
    resulting_calls = []
    for call in mocked_dispatch.mock_calls:
        # Extract the callback_execution argument for each call
        callback_execution = call[1][0]

        # Print some debug stuff for testing
        print(f"="*80)
        print(f"Callback: {callback_execution.callback}")
        print(f"Action: {callback_execution.action}")
        print(f"Experiment: {callback_execution.action.experiment}")

        # Save the call for comparison
        resulting_calls.append((callback_execution.action.experiment.label, callback_execution.callback.label))

    # Compare everything in one go for better detection of difference in case of errors
    assert expected_calls == resulting_calls


def test_update_state(experiment_data_with_callback,
                      mocker: MockerFixture):
    # Disable callbacks
    mocker.patch('studygovernor.control.dispatch_callback')
    destination_state = State.query.filter(State.label == 'step2').one()

    # Set all experiments ready to state2
    experiments = Experiment.query.all()
    for experiment in experiments:
        print(f"Stating state for {experiment} to {destination_state.label}")
        set_state(experiment, destination_state)

        # Make sure all callbacks are marked done (or update_state will not run)
        for action in experiment.actions:
            for execution in action.executions:
                execution.status = CallbackExecutionStatus.finished

    # Perform state updates
    for experiment in experiments:
        print(f"Updating state for {experiment}")
        update_state(experiment)

    expected_states = {
        'Experiment_1_1': 'step3b',
        'Experiment_1_2': 'step3a',
        'Experiment_2_1': 'step3b',
        'Experiment_2_2': 'step3a',
    }

    # Collect results and compare to expected results
    resulting_states = {}
    for experiment in experiments:
        resulting_states[experiment.label] = experiment.state.label

    assert expected_states == resulting_states


def test_get_set_variable(experiment_data):
    experiment = Experiment.query.first()

    # Make sure requesting non-existing variables give a KeyError
    with pytest.raises(KeyError):
        get_variable(experiment, 'test_var1')

    test_int = 12
    test_float = 42.0
    test_bool1 = True
    test_bool2 = False
    test_datetime = datetime.datetime.now()
    test_date = datetime.date.today()
    test_time = test_datetime.time()
    test_str = 'some string value'

    # Test setting variable
    set_variable(experiment, 'test_int', test_int)
    set_variable(experiment, 'test_float', test_float)
    set_variable(experiment, 'test_bool1', test_bool1)
    set_variable(experiment, 'test_bool2', test_bool2)
    set_variable(experiment, 'test_datetime', test_datetime)
    set_variable(experiment, 'test_date', test_date)
    set_variable(experiment, 'test_time', test_time)
    set_variable(experiment, 'test_str', test_str)

    db.session.commit()

    # Test retrieving variables
    assert get_variable(experiment, 'test_int') == test_int
    assert get_variable(experiment, 'test_float') == test_float
    assert get_variable(experiment, 'test_bool1') == test_bool1
    assert get_variable(experiment, 'test_bool2') == test_bool2
    assert get_variable(experiment, 'test_datetime') == test_datetime
    assert get_variable(experiment, 'test_date') == test_date
    assert get_variable(experiment, 'test_time') == test_time
    assert get_variable(experiment, 'test_str') == test_str

    # Make sure requesting non-existing variables give a KeyError
    with pytest.raises(KeyError):
        get_variable(experiment, 'test_var1')

    # Test requesting all variables
    assert get_variables(experiment) == {
        'test_int': test_int,
        'test_float': test_float,
        'test_bool1': test_bool1,
        'test_bool2': test_bool2,
        'test_datetime': test_datetime,
        'test_date': test_date,
        'test_time': test_time,
        'test_str': test_str,
    }


def test_check_condition(experiment_data):
    experiment = Experiment.query.first()
    set_variable(experiment, 'test_var', 42)
    set_variable(experiment, 'test_flag', True)
    db.session.commit()

    # Test non-conditions, they should be always true
    assert check_condition(None, experiment)
    assert check_condition('', experiment)
    assert check_condition('    ', experiment)

    # Test actual conditions
    assert check_condition("experiment.variables['test_flag']", experiment)
    assert check_condition("experiment.variables['test_var'] == 42", experiment)
    assert not check_condition("experiment.variables['test_var'] == 24", experiment)
    assert check_condition("40 < experiment.variables['test_var'] < 50", experiment)
    assert check_condition("(experiment.variables['test_var'] + 22) == (8 ** 2)", experiment)
    assert check_condition(f"experiment.label == '{experiment.label}'", experiment)
    assert check_condition(f"str(experiment.scandate) == '{experiment.scandate}'", experiment)
    assert check_condition(f"experiment.state == '{experiment.state.label}'", experiment)

    with pytest.raises(exceptions.ConditionFunctionCallFailedError):
        check_condition("some_non_existing_var == 2", experiment)

    with pytest.raises(exceptions.ConditionFunctionCallFailedError):
        check_condition("experiment.variables['test_bool']", experiment)


def test_update_callback_execution_state(experiment_data_with_callback,
                                         mocker: MockerFixture):
    # Patch the dispatch_callback to avoid actual attempts at callbacks
    mocked_dispatch = mocker.patch('studygovernor.control.dispatch_callback')
    destination_state = State.query.filter(State.label == 'step2').one()

    # Set a variable to test against for all experiments
    experiment = Experiment.query.filter_by(label="Experiment_1_2").one()
    set_variable(experiment, 'test', 1)
    db.session.commit()

    # Trigger state change
    print(f"Updating state for {experiment}")
    set_state(experiment, destination_state)

    # Extract resulting call
    assert(len(mocked_dispatch.mock_calls) == 1)
    call = mocked_dispatch.mock_calls[0]

    # Extract the callback_execution argument for each call
    callback_execution: CallbackExecution = call[1][0]

    assert callback_execution.status == CallbackExecutionStatus.created
    assert callback_execution.result == CallbackExecutionResult.none
    assert callback_execution.run_start is None
    assert callback_execution.wait_start is None
    assert callback_execution.finished is None

    update_callback_execution_state(callback_execution, "running")
    db.session.commit()
    assert callback_execution.status == CallbackExecutionStatus.running
    assert callback_execution.result == CallbackExecutionResult.none
    assert isinstance(callback_execution.run_start, datetime.datetime)
    assert callback_execution.wait_start is None
    assert callback_execution.finished is None
    assert get_variables(experiment) == {'test': 1}

    callback_execution.result_values = yaml.safe_dump({
        "task_uri": "https://www.example.com/path/to/task",
        "varname": 1337,
        "nested_value": {
            "key1": "something",
            "key2": [1, 2, 3, 42]
        }
    })
    callback_execution.result = CallbackExecutionResult.success
    update_callback_execution_state(callback_execution, "finished")

    db.session.commit()
    assert callback_execution.status == CallbackExecutionStatus.finished
    assert callback_execution.result == CallbackExecutionResult.success
    assert isinstance(callback_execution.run_start, datetime.datetime)
    assert callback_execution.wait_start is None
    assert isinstance(callback_execution.finished, datetime.datetime)
    assert get_variables(experiment) == {
        'test': 1,
        'inspection_task_uri': 'https://www.example.com/path/to/task',
        'some_important_variable': 1337,
        'the_answer': 42,
    }




