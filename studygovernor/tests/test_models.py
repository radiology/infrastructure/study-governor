# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime
import string

from studygovernor.models import db

from studygovernor.models import Cohort
from studygovernor.models import Subject
from studygovernor.models import ExternalSystem
from studygovernor.models import ExternalSubjectLinks
from studygovernor.models import Scantype
from studygovernor.models import State
from studygovernor.models import Callback
from studygovernor.models import CallbackExecution
from studygovernor.models import CallbackExecutionStatus
from studygovernor.models import CallbackExecutionResult
from studygovernor.models import Experiment
from studygovernor.models import Workflow

from studygovernor import exceptions

import pytest
from sqlalchemy import exc


def test_create_cohort(init_db):
    cohort_label = 'new_cohort'
    cohort_description = 'this is a test cohort'
    cohort = Cohort(label=cohort_label, description=cohort_description)

    db.session.add(cohort)
    db.session.commit()

    cohort_from_db = Cohort.query.filter_by(label=cohort_label).one()

    assert cohort_from_db.label == cohort_label
    assert cohort_from_db.description == cohort_description
    assert cohort_from_db.external_system_urls == []


def test_create_externalcohorturl(cohort_data, externalsystem):
    # Link External systems to Subject
    from studygovernor.models import ExternalSystem
    from studygovernor.models import Cohort
    from studygovernor.models import ExternalCohortUrls
    from studygovernor.models import db

    # Get data
    cohort = Cohort.query.first()
    externalsystem = ExternalSystem.query.first()

    # link subjects to external_data
    external_url = ExternalCohortUrls(
        url='http://www.example.com',
        cohort=cohort,
        external_system=externalsystem)
    db.session.add(external_url)
    db.session.commit()

    external_url_from_db = ExternalCohortUrls.query.first()
    assert external_url_from_db.url == 'http://www.example.com'
    assert external_url_from_db.cohort == cohort
    assert external_url_from_db.external_system == externalsystem

    assert cohort.external_system_urls == [external_url]
    assert externalsystem.external_cohort_urls == [external_url]


def test_create_subject(cohort_data):
    from datetime import date

    cohort = Cohort.query.first()

    # Test data
    subject_label = "NewSubject"
    subject_dob = date(2019, 1, 1)

    # Create subject
    subject = Subject(label=subject_label, date_of_birth=subject_dob, cohort=cohort)
    db.session.add(subject)
    db.session.commit()
    subject_from_db = Subject.query.filter_by(label=subject_label).one()

    assert subject_from_db.label == subject_label
    assert subject_from_db.date_of_birth == subject_dob
    assert f"{subject_from_db}" == f"<Subject label={subject_label}, id={subject_from_db.id}, dob={subject_dob}>"


def test_create_duplicate_subject(cohort_data):
    from datetime import date

    cohort = Cohort.query.first()

    # Test data
    subject_label = "NewSubject2"
    subject_dob = date(2019, 1, 1)

    # Create subject
    subject = Subject(label=subject_label, date_of_birth=subject_dob, cohort=cohort)
    db.session.add(subject)
    db.session.commit()

    # create duplicate
    duplicate_subject = Subject(label=subject_label, date_of_birth=subject_dob, cohort=cohort)
    db.session.add(duplicate_subject)

    with pytest.raises(exc.IntegrityError):
        db.session.commit()
    db.session.rollback()


def test_create_externalsystem(init_db):
    system_name = 'NewSystemName'
    system_url = 'http://system.url'

    external_system = ExternalSystem(system_name=system_name, url=system_url)
    db.session.add(external_system)
    db.session.commit()

    external_system_from_db = ExternalSystem.query.filter_by(system_name=system_name).one()

    assert external_system_from_db.system_name == system_name
    assert external_system_from_db.url == system_url
    assert f"{external_system_from_db}" == f"<ExternalSystem {system_name} (id={external_system_from_db.id})>"


def test_create_scantype(init_db):
    scan_modality = 'Mod'
    scan_protocol = 'Prot'

    scan_type = Scantype(modality=scan_modality, protocol=scan_protocol)
    db.session.add(scan_type)
    db.session.commit()

    scan_type_from_db = Scantype.query.filter_by(protocol=scan_protocol).one()
    result = f"<Scantype modality={scan_modality}, protocol={scan_protocol}, id={scan_type_from_db.id}>"
    assert scan_type_from_db.protocol == scan_protocol
    assert scan_type_from_db.modality == scan_modality
    assert f"{scan_type_from_db}" == result


def test_create_state(init_db):
    state_label = 'BeginState'
    state_freetext = 'freetext'

    state = State(label=state_label, freetext=state_freetext)
    db.session.add(state)
    db.session.commit()

    state_from_db = State.query.filter_by(id=state.id).one()

    assert state_from_db.label == state_label
    assert state_from_db.callbacks == []
    assert state_from_db.freetext == state_freetext
    assert f"{state_from_db}" == f"<State state={state_label}, id = {state_from_db.id}, workflow = none>"


def test_subject_data(subject_data):
    subjects = Subject.query.all()
    assert len(subjects) >= 2


def test_create_experiment(workflow_test_data, subject_data):
    from datetime import datetime
    test_subject = Subject.query.first()
    experiment_label = "NewExperiment"
    experiment_scandate = datetime(2019, 1, 2)

    workflow = Workflow.query.first()

    experiment = Experiment(subject=test_subject, label=experiment_label, scandate=experiment_scandate)
    db.session.add(experiment)
    db.session.commit()

    experiment_from_db = Experiment.query.filter_by(label=experiment_label).one()

    result = f"<Experiment label={experiment_label}, " \
             f"id={experiment.id}, subject={test_subject.id}, " \
             f"scandate={experiment_scandate}>"

    assert experiment_from_db.label == experiment_label
    assert experiment_from_db.scandate == experiment_scandate
    assert f"{experiment_from_db}" == result


def test_init_state_experiment(workflow_test_data, subject_data):
    from datetime import date
    workflow = Workflow.query.first()
    test_subject = Subject.query.first()
    experiment_label = "NewExperiment2"
    experiment_scandate = date(2019, 1, 2)
    experiment = Experiment(subject=test_subject, label=experiment_label, scandate=experiment_scandate)
    db.session.add(experiment)
    db.session.commit()

    assert experiment.state is None


def test_experiment_data(experiment_data):
    experiments = Experiment.query.all()
    assert len(experiments) == 4


def test_state_experiments_property(workflow_test_data, experiment_data):
    untracked_state = State.query.filter_by(label='untracked').one()
    step1_state = State.query.filter_by(label='step1').one()
    step2a_state = State.query.filter_by(label='step2a').one()

    assert untracked_state.experiments == []
    total_experiments = 0
    for state in State.query.all():
        total_experiments += len(state.experiments)
    assert total_experiments == 4


def test_create_externalsubjectlinks(subject_data, externalsystem):
    # Link External systems to Subject
    from studygovernor.models import ExternalSystem
    from studygovernor.models import Subject
    from studygovernor.models import ExternalSubjectLinks
    from studygovernor.models import db

    # Get data
    subjects = Subject.query.all()
    externalsystems = ExternalSystem.query.all()

    # link subjects to external_data
    external_link = ExternalSubjectLinks(
        f'External_SubjectID_1',
        subjects[0],
        external_system=externalsystems[0])
    db.session.add(external_link)
    db.session.commit()

    external_link_from_db = ExternalSubjectLinks.query.first()
    assert external_link_from_db.external_id == 'External_SubjectID_1'
    assert external_link_from_db.subject_id == 1
    assert external_link_from_db.external_system_id == 1


def test_external_ids_subject(subject_links):
    subjects = Subject.query.all()
    subject_0_external_ids = subjects[0].external_ids
    subject_1_external_ids = subjects[1].external_ids

    assert subject_0_external_ids == {
        'NewSystemName_1': 'External_SubjectID_1'
    }
    assert subject_1_external_ids == {
        'NewSystemName_2': 'External_SubjectID_2',
        'NewSystemName_3': 'External_SubjectID_2'
    }


def test_callback_create(workflow_test_data):
    state = State.query.first()

    callback_args = '{"argument": 42}'
    run_timeout = 60
    wait_timeout = 120
    initial_delay = 0
    description = 'test callback'

    callback = Callback(
        state=state,
        function='test_function',
        callback_arguments=callback_args,
        run_timeout=run_timeout,
        wait_timeout=wait_timeout,
        initial_delay=initial_delay,
        description=description,
    )
    db.session.add(callback)
    db.session.commit()

    callback_from_db = Callback.query.filter_by(function='test_function').one()

    assert callback_from_db.state is state
    assert callback_from_db.callback_arguments == callback_args
    assert callback_from_db.run_timeout == run_timeout
    assert callback_from_db.wait_timeout == wait_timeout
    assert callback_from_db.initial_delay == initial_delay
    assert callback_from_db.description == description
    assert callback_from_db.condition is None


def test_callbackexecution_create(workflow_test_data):
    state = State.query.first()

    callback = Callback(
        state=state,
        function='test_callback',
        callback_arguments='{"argument": 42}',
        run_timeout=15,
        wait_timeout=60,
        initial_delay=5,
        description='just a callback for testing',
    )

    db.session.add(callback)
    db.session.commit()

    status = CallbackExecutionStatus.finished
    result = CallbackExecutionResult.success

    run_log = 'running...'
    secret = 'superdupersecretkey'

    callback_execution = CallbackExecution(
        callback=callback,
        action=None,
        status=status,
        result=result,
        run_log=run_log,
        secret_key=secret,
    )

    run_start = datetime.datetime.now()

    callback_execution.run_start = run_start

    db.session.add(callback_execution)
    db.session.commit()

    callback_execution_from_db = CallbackExecution.query.first()

    assert callback_execution_from_db.callback is callback
    assert callback_execution_from_db.status is status
    assert callback_execution_from_db.result is result
    assert callback_execution_from_db.run_log == run_log
    assert callback_execution_from_db.result_log is None
    assert callback_execution_from_db.secret_key == secret
    assert isinstance(callback_execution_from_db.created, datetime.datetime)

    # Compare without checking microseconds and timezone information
    db_run_start = callback_execution_from_db.run_start
    db_run_start = db_run_start.replace(tzinfo=None)
    run_start = run_start.replace(tzinfo=None)

    # Rounding in mysql could make a 1 second shift happen
    assert abs((db_run_start - run_start).total_seconds()) <= 1.0


def test_callbackexecution_create_defaults(workflow_test_data):
    state = State.query.first()

    callback = Callback(
        state=state,
        function='test_callback',
        callback_arguments='{"argument": 42}',
        run_timeout=15,
        wait_timeout=60,
        initial_delay=5,
        description='just a callback for testing',
    )

    db.session.add(callback)
    db.session.commit()

    # Check for defaults
    run_start = datetime.datetime.now()
    callback_execution = CallbackExecution(
        callback=callback,
        action=None,
        run_start=run_start,
    )

    db.session.add(callback_execution)
    db.session.commit()

    callback_execution_from_db = CallbackExecution.query.first()
    assert callback_execution_from_db.callback is callback

    # Test defaults
    assert callback_execution_from_db.status == CallbackExecutionStatus.created
    assert callback_execution_from_db.result == CallbackExecutionResult.none
    assert callback_execution_from_db.run_log is None
    assert callback_execution_from_db.result_log is None
    assert isinstance(callback_execution_from_db.created, datetime.datetime)

    # Compare without checking microseconds and timezone information
    db_run_start = callback_execution_from_db.run_start
    db_run_start = db_run_start.replace(tzinfo=None)
    run_start = run_start.replace(tzinfo=None)

    # Rounding in mysql could make a 1 second shift happen
    assert abs((db_run_start - run_start).total_seconds()) <= 1.0

    # Test if secret is a correctly generated 256 char hex string
    assert isinstance(callback_execution_from_db.secret_key, str)
    assert len(callback_execution_from_db.secret_key) == 256
    assert all(x in string.hexdigits for x in callback_execution_from_db.secret_key)
