import pathlib
from studygovernor.util.helpers import initialize_workflow
from studygovernor.models import Workflow


def test_init_workflow(app, app_config):
    # Load test workflow
    workflow_file = pathlib.Path(__file__).parent.parent.parent / 'tests' / 'test_workflow_callbacks.yaml'
    initialize_workflow(workflow_file, app=app, verbose=True)
    workflow = Workflow.query.one_or_none()
    assert workflow is not None

