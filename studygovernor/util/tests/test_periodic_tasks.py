# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime
import time
import re

from pytest_mock import MockerFixture

from studygovernor.control import set_state, set_variable
from studygovernor.models import db, Experiment, State, CallbackExecution, Callback, \
    CallbackExecutionStatus, CallbackExecutionResult
from studygovernor.util import periodic_tasks


def test_check_timeout(client,
                       experiment_data_with_callback,
                       mocker: MockerFixture):
    # Disable callbacks
    mocker.patch('studygovernor.control.dispatch_callback')
    periodic_tasks.FACTOR = 1  # Use seconds instead of minutes for quick testing
    destination_state = State.query.filter(State.label == 'step2').one()

    # Set all experiments ready to state2
    experiment = Experiment.query.first()
    set_variable(experiment, 'test', 42)
    set_state(experiment, destination_state)

    # Set timeouts more clear
    callback1 = Callback.query.filter_by(label='step2_callback1').one()
    callback1.run_timeout = 5
    callback1.wait_timeout = 3
    print(f"Callback1: {callback1}")
    callback2 = Callback.query.filter_by(label='step2_callback2').one()
    callback2.run_timeout = 10
    print(f"Callback2: {callback1}")

    # Get correct secret keys from DB
    start_time = datetime.datetime.now()
    callback_execution1: CallbackExecution = CallbackExecution.query.filter_by(id=1).one()
    callback_execution1.status = CallbackExecutionStatus.running
    callback_execution1.run_start = start_time
    callback_execution2: CallbackExecution = CallbackExecution.query.filter_by(id=2).one()
    callback_execution2.status = CallbackExecutionStatus.running
    callback_execution2.run_start = start_time

    print(f"Execution1: {callback_execution1}, callback: {callback_execution1.callback}, run_timeout: {callback_execution1.callback.run_timeout}")

    db.session.commit()

    assert callback_execution1.status == CallbackExecutionStatus.running
    assert callback_execution2.status == CallbackExecutionStatus.running

    periodic_tasks.check_timeouts()

    assert callback_execution1.status == CallbackExecutionStatus.running
    assert callback_execution2.status == CallbackExecutionStatus.running

    time.sleep(6)
    periodic_tasks.check_timeouts()

    assert callback_execution1.status == CallbackExecutionStatus.finished
    assert callback_execution1.result == CallbackExecutionResult.timeout
    assert re.match(r"ERROR: CallbackExecution run timeout at [\.\- :0-9]+ after [5-6].\d+ minutes \(started at [\.\- :0-9]+\)\.",
                    callback_execution1.run_log)
    assert callback_execution2.status == CallbackExecutionStatus.running

    time.sleep(6)
    periodic_tasks.check_timeouts()

    assert callback_execution1.status == CallbackExecutionStatus.finished
    assert callback_execution2.status == CallbackExecutionStatus.finished
    assert callback_execution2.result == CallbackExecutionResult.timeout
    assert re.match(r"ERROR: CallbackExecution run timeout at [\.\- :0-9]+ after 1[0-2].\d+ minutes \(started at [\.\- :0-9]+\)\.",
                    callback_execution2.run_log)

    start_time = datetime.datetime.now()
    callback_execution1.status = CallbackExecutionStatus.waiting
    callback_execution1.wait_start = start_time

    periodic_tasks.check_timeouts()

    assert callback_execution1.status == CallbackExecutionStatus.waiting
    assert callback_execution1.result_log is None

    time.sleep(4)
    periodic_tasks.check_timeouts()

    assert callback_execution1.status == CallbackExecutionStatus.finished
    assert callback_execution1.result == CallbackExecutionResult.timeout
    assert re.match(r"ERROR: CallbackExecution wait timeout at [\.\- :0-9]+ after [3-4].\d+ minutes \(started at [\.\- :0-9]+\)\.",
                    callback_execution1.result_log)
